#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <time.h>
#include <string.h>

#define BUF_SIZE 1000

char *currTime(const char *format)
{
	static char buf[BUF_SIZE];
	time_t t;
	size_t s;
	struct tm *tm;

	t = time(NULL);
	tm = localtime(&t);
	if (tm == NULL)
		return NULL;

	s = strftime(buf,BUF_SIZE,(format != NULL) ? format : "%c",tm);
	
	return (s == 0) ? NULL : buf;
}

int main(int argc,char **argv)
{
	int pfd[2];
	int j,dummy;

	if (argc < 2 || strcmp(argv[1],"--help") == 0)
		printf("%s sleep-time...\n",argv[0]);

	setbuf(stdout,NULL);

	printf("%s Parent started\n",currTime("%T"));

	if (pipe(pfd) == -1)
		perror("pipe");
	
	for (j=1 ; j < argc; j++)
	{
		switch(fork())
		{
			case -1:
				printf("fork %d",j);
				perror("fork");
			case 0:
				if (close(pfd[0]) == -1)
					perror("close");
				
				sleep(atoi(argv[j]));

				printf("%s Child %d (PID=%ld) closing pipe\n",currTime("%T"),j,(long)getpid());
				if (close(pfd[1]) == -1)
					perror("close");

				_exit(EXIT_SUCCESS);
			default:
				break;
				
		}
	}
	
	if (close(pfd[1]) == -1)
		perror("close");

	if (read(pfd[0],&dummy,1) != 0)
		perror("parent didn't get EOF");
	printf("%s Parent ready to go\n",currTime("%T"));

	exit(EXIT_SUCCESS);
}
