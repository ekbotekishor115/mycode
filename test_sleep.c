#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <unistd.h>

void sig_handler(int signum)
{
	printf("Signal catched is %d\n",signum);
}

int main()
{
	signal(SIGINT,sig_handler);
	unsigned int ret = sleep(5);
	printf("ret is %d\n",ret);
}
