#include <stdio.h>
#include <string.h>
#include <stdlib.h>
void fun_one(void);
int fun_two(int a);
int* fun_three(int *b);
int fun_four(int i,char*h,int n);

int main()
{
	int d;
	char buf[BUFSIZ];
	fun_one();
	int *c = fun_three(&d);
	int k = fun_four(10,buf,d); 
	printf("Main function %d:%s\n",*(&d),buf);
}

void fun_one(void)
{
	printf("%s:%d:%s\n",__FUNCTION__,__LINE__,__FILE__);
}

int fun_two(int a)
{
	printf("%s\n",__FUNCTION__);

}

int* fun_three(int *b)
{
//	*b = 10;
	printf("%s: %d\n",__FUNCTION__,*b);
	*b = 10;
	return NULL;
}

int fun_four(int i, char* h,int n)
{
	printf("i=%d, h=%s, n=%d\n",i,h,n);
	strcpy(h,"Hello");
	
}
