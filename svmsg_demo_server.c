#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/msg.h>
#include <sys/stat.h>
#include <errno.h>

#define KEY_FILE "/tmp/key"

int main(int argc,char **argv)
{
	int msqid;
	key_t key;
	const int MQ_PERMS = S_IRUSR | S_IWUSR | S_IWGRP;

	key = ftok(KEY_FILE,1);
	if (key == -1)
	{
		perror("ftok");
		return EXIT_FAILURE;
	}

	while ((msqid = msgget(key,IPC_CREAT | IPC_EXCL | MQ_PERMS)) == -1)
	{
		if (errno == EEXIST)
		{
			msqid = msgget(key,0);
			if (msqid == -1)
				printf("msgget() failed to retrieve old queue ID\n");
			if (msgctl(msqid,IPC_RMID,NULL) == -1)
				printf("msgget() failed to delete old queue\n");
			printf("Remove old message queue (id=%d)\n",msqid);
		}
		else 
		{
			printf("msgget() failed\n");
		}
	}
	
	exit(EXIT_SUCCESS);
}
