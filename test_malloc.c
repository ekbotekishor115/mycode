#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main()
{
	int n;
	printf("Enter number to allocate int array\n");
	scanf("%d",&n);
	printf("n=%d\n",n);
	int *ptr = (int*) malloc(n * sizeof(int));
//	int *ptr_new = (int *) realloc(ptr, sizeof(int));
	for (int i = 0; i <= 16;i++)
	{
		ptr[i] = rand();
	}
	for (int i = 0;i <= 16; i++)
	{	
		printf("ptr[%d]=%d\n",i,ptr[i]);
	}

	memset(ptr,0,n * sizeof(int));
//	memset(ptr_new,0,n * sizeof(int));
//	if (ptr_new == ptr)
//	{
//		printf("Same Pointer\n");
//	}
	free(ptr);
	ptr = NULL;
//	ptr_new = NULL;
	return 0;
}
