#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include "svshm_xfr.h"

int main(int argc,char **argv)
{
	int semid,shmid,xfrs,bytes;
	struct shmseg *shmp;
	
	semid = semget(SEM_KEY,0,0);
	if (semid == -1)
	{
		perror("semget");
		exit(EXIT_FAILURE);
	}

	shmid = shmget(SHM_KEY,0,0);
	if (shmid == -1)
	{
		perror("shmget");
		exit(EXIT_FAILURE);
	}
	
	shmp = shmat(shmid,NULL,SHM_RDONLY);
	if (shmp == (void *) -1)
	{
		perror("shmat");
		exit(EXIT_FAILURE);
	}
	
	for (xfrs = 0, bytes = 0;;xfrs++)
	{
		if (reverseSem(semid,READ_SEM) == -1)
		{
			perror("reverseSem");
			exit(EXIT_FAILURE);
		}

		if (shmp->cnt == 0)
			break;
		bytes += shmp->cnt;

		if (write(STDOUT_FILENO,shmp->buf,shmp->cnt) != shmp->cnt)
		{
			printf("partial/failed write");
			exit(EXIT_FAILURE);
		}

		if (releaseSem(semid,WRITE_SEM) == -1)
		{
			perror("releaseSem");
			exit(EXIT_FAILURE);
		}
	}

	if (shmdt(shmp) == -1)
	{
		perror("shmdt");
		exit(EXIT_FAILURE);
	}

	if (releaseSem(semid,WRITE_SEM) == -1)
	{
		perror("releaseSem");
		exit(EXIT_FAILURE);
	}

	fprintf(stderr,"Received %d bytes (%d xfrs)\n",bytes,xfrs);
	exit(EXIT_SUCCESS);
}
