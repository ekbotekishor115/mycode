//C program to implement one side of FIFO
//This side writes first,then reads

#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>
#include <string.h>

int main()
{
	int fd;

	char * myfifo = "/tmp/myfifo";

	mkfifo(myfifo,0666);

	char arr1[80],arr2[80];

	while (1)
	{
		fd = open(myfifo,O_WRONLY);
		
		fgets(arr2,80,stdin);
//		scanf("%s",&arr2[]);
		

		write(fd,arr2,strlen(arr2)+1);
//		for (int i = 0; i < strlen(arr2)+1; i++)
//		{
//			printf("%s",arr2[i]);
//		}
//		printf("%ld\n",strlen(arr2)+1);
		close(fd);

		fd = open(myfifo,O_RDONLY);
		
		read(fd,arr1,sizeof(arr1));

		printf("User2: %s\n",arr1);
		close(fd);
	}

	return 0;
}
