#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <unistd.h>
#include <string.h>

void printSigset(FILE *of, const char *prefix, const sigset_t *sigset)
{
	int sig, cnt;

	cnt = 0;
	for(sig = 0 ; sig < NSIG ; sig++)
		if(sigismember(sigset, sig))
		{
			cnt++;
			fprintf(of, "%s%d (%s)\n", prefix, sig, strsignal(sig));
		}
}

int printPendingSigs(FILE *of, const char *msg)
{
	sigset_t pendingSigs;

	if(msg != NULL)
		fprintf(of, "%s", msg);
	
	if(sigpending(&pendingSigs) == -1)
		return -1;

	printSigset(of, "\t\t", &pendingSigs);
	return 0;
}

void sig_handler(int sig)
{
	printf("Signal\n");
}

int main()
{
	sigset_t blockSet,prevMask;
	sigemptyset(&blockSet);
	sigaddset(&blockSet, SIGINT);
	sigaddset(&blockSet, SIGTRAP);
	sigaddset(&blockSet, SIGABRT);
	if(sigprocmask(SIG_BLOCK, &blockSet, &prevMask) == -1)
		perror("sigprocmask1");
	signal(SIGINT, sig_handler);
	while(1)
	{
		usleep(500);
		printPendingSigs(stdout,"Rahile rao Signal");
	}

	if(sigprocmask(SIG_SETMASK, &prevMask, NULL)== -1)
		perror("sigprocmask2");

//	printPendingSigs(stdout,"Rahile rao Signal");
}
	
