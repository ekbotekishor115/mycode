#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include "svmsg_file.h"

static int clientId;

static void removeQueue()
{
	if (msgctl(clientId,IPC_RMID,NULL) == -1)
	{
		perror("msgctl");
		exit(EXIT_FAILURE);
	}
}

int main(int argc,char **argv)
{
	struct requestMsg req;
	struct responseMsg resp;
	int serverId,numMsgs;
	ssize_t msgLen,totBytes;

	if (argc != 2 || strcmp(argv[1],"--help") == 0)
	{
		printf("%s pathname\n",argv[0]);
		exit(EXIT_FAILURE);
	}

	if (strlen(argv[1]) > sizeof(req.pathname) - 1)
	{
		printf("pathname too long (max: %ld bytes)\n",(long) sizeof(req.pathname) - 1);
	}

	serverId = msgget(SERVER_KEY,S_IWUSR);
	if (serverId == -1)
	{
		perror("msgget - server message queue");
		return EXIT_FAILURE;
	}

	clientId = msgget(IPC_PRIVATE,S_IRUSR | S_IWUSR | S_IWGRP);
	if (clientId == -1)
	{
		perror("msgget - client message queue");
		return EXIT_FAILURE;
	}

	if (atexit(removeQueue) != 0)
	{
		perror("atexit");
		return EXIT_FAILURE;
	}

	req.mtype = 1;
	req.clientId = clientId;
	strncpy(req.pathname,argv[1],sizeof(req.pathname) - 1);
	req.pathname[sizeof(req.pathname) - 1] = '\0';

	if (msgsnd(serverId,&req,REQ_MSG_SIZE,0) == -1)
	{
		perror("msgsnd");
		return EXIT_FAILURE;
	}

	msgLen = msgrcv(clientId,&resp,RESP_MSG_SIZE,0,0);
	if (msgLen == -1)
	{
		perror("msgrcv");
		return EXIT_FAILURE;
	}

	if (resp.mtype == RESP_MT_FAILURE)
	{
		printf("%s\n",resp.data);
		if (msgctl(clientId,IPC_RMID,NULL) == -1)
		{
			perror("msgctl");
		
		}
		exit(EXIT_FAILURE);
	}

	totBytes = msgLen;
	for (numMsgs = 1;resp.mtype == RESP_MT_DATA;numMsgs++)
	{
		msgLen = msgrcv(clientId,&resp,RESP_MSG_SIZE,0,0);
		if (msgLen == -1)
		{
			perror("msgrcv");
			return EXIT_FAILURE;
		}
		totBytes += msgLen;
	}

	printf("Received %ld bytes (%d messages)\n",(long) totBytes,numMsgs);
	
	exit(EXIT_SUCCESS);
}
