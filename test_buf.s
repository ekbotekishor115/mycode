	.file	"test_buf1.c"
	.section	.rodata
.LC0:
	.string	"setvbuf"
.LC1:
	.string	"Hello_World!"
	.text
	.globl	main
	.type	main, @function
main:
.LFB0:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	movq	stdout(%rip), %rax
	movl	$1024, %ecx
	movl	$0, %edx
	movl	$buf.2285, %esi
	movq	%rax, %rdi
	call	setvbuf
	testl	%eax, %eax
	je	.L2
	movl	$.LC0, %edi
	call	perror
.L2:
	movl	$.LC1, %edi
	call	puts
	movl	$0, %eax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE0:
	.size	main, .-main
	.local	buf.2285
	.comm	buf.2285,1024,32
	.ident	"GCC: (Ubuntu 5.4.0-6ubuntu1~16.04.12) 5.4.0 20160609"
	.section	.note.GNU-stack,"",@progbits
