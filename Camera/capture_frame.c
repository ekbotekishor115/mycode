#include <stdio.h>
#include <stdlib.h>
#include <linux/ioctl.h>
#include <linux/types.h>
#include <linux/v4l2-common.h>
#include <linux/v4l2-controls.h>
#include <linux/videodev2.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/ioctl.h>
#include <sys/mman.h>
#include <string.h>


//using namespace std;

int main()
{
	//1. open the device
	int fd;
	fd = open("/dev/video0",O_RDWR);
	if (fd < 0)
	{
		perror("Failed to open device, OPEN");
		return 1;
	}

	//2. Ask the device if it can capture frames
	struct v4l2_capability capability;
	if (ioctl(fd, VIDIOC_QUERYCAP, &capability) < 0)
	{
		perror("Failed to get device capabilities, VIDIOC_QUERYCAP");
		return 1;
	}

	//3. Set image format
	struct v4l2_format imageFormat;
	imageFormat.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
	imageFormat.fmt.pix.width = 1024;
	imageFormat.fmt.pix.height = 1024;
	imageFormat.fmt.pix.pixelformat = V4L2_PIX_FMT_MJPEG;
	imageFormat.fmt.pix.field = V4L2_FIELD_NONE;
	//tell the device you are using this format
	if (ioctl(fd,VIDIOC_S_FMT, &imageFormat) < 0)
	{
		perror("Device could not set format, VIDIOC_S_FMT");
		return 1;
	}



	//4. Request Buffers from the device
	struct v4l2_requestbuffers requestBuffer = {0};
	requestBuffer.count = 1; // one request buffer
	requestBuffer.type = V4L2_BUF_TYPE_VIDEO_CAPTURE; // request a buffer which we can use for capturing frames
	requestBuffer.memory = V4L2_MEMORY_MMAP;

	if (ioctl(fd, VIDIOC_REQBUFS, &requestBuffer) < 0)
	{
		perror("Could not request buffer from device, VIDIOC_REQBUFS");
		return 1;
	}

	//5. Quety the buffer to get raw data ie. ask for the you requested buffer
	//and allocate memory for it
	struct v4l2_buffer queryBuffer = {0};
	queryBuffer.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
	queryBuffer.memory = V4L2_MEMORY_MMAP;
	queryBuffer.index = 0;
	if (ioctl(fd, VIDIOC_QUERYBUF, &queryBuffer) < 0)
	{
		perror("Device did not return the buffer information, VIDIOC_QUERYBUF");
		return 1;
	}


	// use a pointer to point the newly created buffer
	// mmap() will map the memory address of the device to 
	// an address in memory
	char *buffer = (char *) mmap(NULL, queryBuffer.length, PROT_READ | PROT_WRITE, MAP_SHARED, fd, queryBuffer.m.offset);
	memset(buffer, 0, queryBuffer.length);

	//6. Get a frame
	// Create a new buffer type so the device knows whichbuffer we are talking about
	struct v4l2_buffer bufferinfo;
	memset(&bufferinfo, 0, sizeof(bufferinfo));
	bufferinfo.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
	bufferinfo.memory = V4L2_MEMORY_MMAP;
	bufferinfo.index = 0;

	// Active streming
	int type = bufferinfo.type;
	if (ioctl(fd, VIDIOC_STREAMON, &type) < 0)
	{
		perror("Could not start streaming, VIDIOC_STREAMON");
		return 1;
	}

/************************************ Begin looping here ****************************/

	// Queue the buffer
	if (ioctl(fd, VIDIOC_QBUF, &bufferinfo) < 0)
	{
		perror("Could not queue buffer, VIDIOC_QBUF");
		return 1;
	}


	// Dequque the buffer
	if (ioctl(fd, VIDIOC_DQBUF, &bufferinfo) < 0)
	{
		perror("Could not dequeue the buffer, VIDIOC_DQBUF");
		return 1;
	}

	// Frames get written after dequeing the buffer
	//can use normal printf

	// write the date out to the file
	int cam_fd = open("webcam_output.jpeg",O_WRONLY | O_CREAT);
	if (cam_fd < 0)
	{
		perror("webcam file open failed");
		return EXIT_FAILURE;
	}

	int bufPos = 0, outFileMemBlockSize = 0; // the position in the buffer and the amount to copy from the buffer
	int remainingBufferSize = bufferinfo.bytesused; //the remaining buffer size is decreamented by memBlockSize amount on each loop so we do not overwrite the buffer
	char *outFileMemBlock = NULL; // a pointer to a new memory block
	int itr = 0; //counts the number of iterations
	
	while (remainingBufferSize > 0)	
	{
		bufPos += outFileMemBlockSize;
		//increament the buffer pointer on each loop 
		//initialize bufPos before outFileMemBlockSize so we can start at the beginning of the buffer
		outFileMemBlockSize = 1024; //set the output block size to apreferable size. 1024
//		outFileMemBlock = (char *) malloc(sizeof(outFileMemBlockSize));
		outFileMemBlock = (char *) malloc(outFileMemBlockSize);
		if (outFileMemBlock == NULL)
		{
			perror("Unable to malloc");
			return EXIT_FAILURE;
		}
		// copy the 1024 bytes of data starting from buffer + bufPos
		memcpy(outFileMemBlock,buffer+bufPos,outFileMemBlockSize);
		ssize_t cam_wr = write(cam_fd,outFileMemBlock,outFileMemBlockSize);
		if (cam_wr < 0)
		{
			perror("Write failed");
			return EXIT_FAILURE;
		}
		
		// calculate the amount of memory left to read
		// if the memory block size is greater than the remaining
		// amount of data we have to copy
		if (outFileMemBlockSize > remainingBufferSize)
		{
			outFileMemBlockSize = remainingBufferSize;
		}
		// subtract the amount of data we have to copy
		// from the remaining buffer size
		remainingBufferSize -= outFileMemBlockSize;

		// display the remaining buffer size
		printf("Remaining bytes: %d\n",remainingBufferSize);

		free(outFileMemBlock);
	}
	
	//close the file
	int cam_cl = close(cam_fd);
	if (cam_cl < 0)
	{
		perror("close failed");
		return EXIT_FAILURE;
	}

/***************************************** end looping here *****************************/

	// end streaming 
	if (ioctl(fd, VIDIOC_STREAMOFF, &type) < 0)
	{
		perror("Could not end streaming, VIDIOC_STREAMOFF");
		return 1;
	}
	
	close(fd);
	return 0;
}

