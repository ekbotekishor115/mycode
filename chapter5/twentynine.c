//Size of array of function pointer
#include <stdio.h>
#include <stdlib.h>

int main()
{
	int (*arr[4]) (int, int);
	printf("Memory allocated to arr is %d bytes\n",sizeof(arr));
	exit(0);
}
