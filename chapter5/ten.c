//Attempt to return more than one value
#include <stdio.h>
#include <stdlib.h>

int sum_diff(int, int);

void main()
{
	int a=10,b=2;
	printf("Sum is %d\n",sum_diff(a,b));
	printf("Difference is %d\n",sum_diff(a,b));
	exit(0);
}

int sum_diff(int a,int b)
{
	int sum = a+b;
	int diff = a-b;
	return sum,diff;
}
