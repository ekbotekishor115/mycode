// Array of function pointers
#include <stdio.h>
#include <stdlib.h>
// Function declaration
int add(int,int);
int sub(int,int);
int mult(int,int);
int division(int,int);

// Function definations

int main()
{
	// Array of function pointers initialized with initialization list
	int (*arr[4])(int,int) = {add,sub,mult,division};
	int lc;
	printf("Calling functions using iteration:\n");

	// Functions called in a generalized way by using loop
	for (lc=0 ;lc<4 ;lc++)
	{
		arr[lc](6,3);
	}

	exit(0);
}

int add(int a, int b)
{
	printf("Result of addition of %d and %d is %d\n",a,b,a+b);
}

int sub(int a, int b)
{
	printf("Result of subtraction of %d and %d is %d\n",a,b,a-b);
}

int mult(int a, int b)
{
	printf("Result of multiplication of %d and %d is %d\n",a,b,a*b);
}

int division(int a, int b)
{
	printf("Result of division of %d and %d is %d\n",a,b,a/b);
}
