//Non-tail recursive factorial function
#include <stdio.h>
#include <stdlib.h>

int fact_norm(int);

void main()
{
	int no, factorial;
	printf("Enter the number:\t");
	scanf("%d",&no);
	factorial= fact_norm(no);
	printf("Factorial of %d is %d\n",no,factorial);
	exit(0);
}

int fact_norm(int no)
{
	if(no == 1)
		return 1;
	else
		return no*fact_norm(no-1);
}

