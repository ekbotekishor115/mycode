//Function generalization of printsum
#include <stdio.h>
#include <stdlib.h>

void printsum(int ,int ,char);

void main()
{
	int a,b;
	char base;
	printf("Enter base of output(D,O or H)\n");
	scanf("%c",&base);
	printf("Enter thye values of a & b\t");
	scanf("%d %d",&a,&b);
	printf("%d %d\n",a,b);
//	printf("Enter base of output(D,O or H)\n");
//	flushall();
//	fflush(stdin);
//	scanf("%c",&base);
	printf("%d\n",base);
	printsum(a,b,base);
	exit(0);
}

void printsum(int x,int y,char base)
{
	if(base== 'd' || base == 'D')
		printf("Sum of %d and %d in Decimal is %d\n",x,y,x+y);
	else if(base == 'o' || base == 'O')
		printf("Sum of %d and %d in Octal is %d\n",x,y,x+y);
	else if(base == 'h' || base =='H')
		printf("Sum of %d and %d in Hexadecimal is%d\n",x,y,x+y);
}
