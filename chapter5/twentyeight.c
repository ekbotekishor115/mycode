//calling functions using function pointers
#include <stdio.h>
#include <stdlib.h>

int add(int  a, int b);

int main()
{
	// Assigning address by using function designator only
	int (*ptr1)(int, int) = add;

	// Assigning address by using address-of operator
	int (*ptr2)(int, int) = &add;
	printf("Calling functions using function pointers:\n");
	
	// Calling functions by derefferencing function pointers
	(*ptr1)(10,12);

	// Calling functions by using function pointer name
	ptr2(2,3);
	exit(0);
}

int add(int a, int b)
{
	printf("The result of addition is %d\n",a+b);
}
