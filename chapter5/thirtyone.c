// Passing function to afunction as an argument
#include <stdio.h>
#include <stdlib.h>

// Function declaration 
int add(int,int);
int sub(int,int);

//Declaration of function whose third paameter is function ptr
void f_calling_fs(int,int,int (*) (int,int));

//Function defination

void main()
{
	printf("Passing functions to a function:\n");
	// Third argument in the following function  calls is a function designator
	f_calling_fs(10,20,add);
	f_calling_fs(10,20,sub);
	exit(0);
}

void f_calling_fs(int a,int b,int (*fun) (int,int))
{
	fun(a,b);
}

int add(int a, int b)
{
	printf("Result of addition of %d and %d is %d\n",a,b,a+b);
	return 0;
}

int sub(int a, int b)
{
	printf("Result of subtraction of %d and %d is %d\n",a,b,a-b);
	return 0;
}

