//Function with Inputs and No Outputs
#include <stdio.h>
#include <stdlib.h>

void printsum(int,int);

void main()
{
	int a, b;
	printf("Enter values of a & b\t");
	scanf("%d %d",&a,&b);
	printsum(a,b);
	printf("Enter values of a & b again\t");
	scanf("%d %d",&a,&b);
	printsum(a,b);
	exit(0);
}

void printsum(int x, int y)
{
	printf("Sum of the %d and %d is %d\n",x,y,x+y);
}
