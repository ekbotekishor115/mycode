	.file	"twentyfive1.c"
	.section	.rodata
.LC0:
	.string	"Enter term no.:\t"
.LC1:
	.string	"%d"
.LC2:
	.string	"Fibonacci term is %d\n"
	.text
	.globl	main
	.type	main, @function
main:
.LFB2:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movl	$.LC0, %edi
	movl	$0, %eax
	call	printf
	leaq	-8(%rbp), %rax
	movq	%rax, %rsi
	movl	$.LC1, %edi
	movl	$0, %eax
	call	__isoc99_scanf
	movl	-8(%rbp), %eax
	movl	$0, %edx
	movl	$1, %esi
	movl	%eax, %edi
	call	fib_tail
	movl	%eax, -4(%rbp)
	movl	-4(%rbp), %eax
	movl	%eax, %esi
	movl	$.LC2, %edi
	movl	$0, %eax
	call	printf
	movl	$0, %edi
	call	exit
	.cfi_endproc
.LFE2:
	.size	main, .-main
	.globl	fib_tail
	.type	fib_tail, @function
fib_tail:
.LFB3:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movl	%edi, -4(%rbp)
	movl	%esi, -8(%rbp)
	movl	%edx, -12(%rbp)
	cmpl	$1, -4(%rbp)
	jne	.L3
	movl	-12(%rbp), %eax
	jmp	.L4
.L3:
	movl	-8(%rbp), %edx
	movl	-12(%rbp), %eax
	leal	(%rdx,%rax), %esi
	movl	-4(%rbp), %eax
	leal	-1(%rax), %ecx
	movl	-8(%rbp), %eax
	movl	%eax, %edx
	movl	%ecx, %edi
	call	fib_tail
.L4:
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3:
	.size	fib_tail, .-fib_tail
	.ident	"GCC: (Ubuntu 5.4.0-6ubuntu1~16.04.12) 5.4.0 20160609"
	.section	.note.GNU-stack,"",@progbits
