#include <stdio.h>
#include <stdlib.h>

void swap(int ,int);

void main()
{
	int a=10, b=20;
	printf("Before swap values are : %d ,%d\n",a,b);
	swap(a,b);
	printf("After swap values are : %d, %d\n",a,b);
	exit(0);
}

void swap(int a, int b)
{
	a=a+b;
	b=a-b;
	a=a-b;
	printf("In swap function values are %d and %d\n",a,b);
}
