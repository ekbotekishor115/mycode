// Function to check whether a given number is prime or not
#include <stdio.h>
#include <stdlib.h>

int prime(int no);

void main()
{
	int num;
	printf("Enter the number to be checked:\t");
	scanf("%d",&num);
	if (prime(num) == 0)
		printf("Number is not prime\n");
	else
		printf("Number is prime\n");
	exit(0);
}

int prime(int no)
{
	int i;
	for (i=2; i<no; i++)
	{
		if (no%i == 0)
		{
			return 0;
		}
	}
	return 1;
}
