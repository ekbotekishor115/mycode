//Recursion to find factorial
#include <stdio.h>
#include <stdlib.h>

int fact(int);

void main()
{
	int no, factorial;
	printf("Enter the number:\t");
	scanf("%d",&no);
	factorial = fact(no);
	printf("Factorial of % is %d\n",no,factorial);
	exit(0);
}

int fact(int no)
{
	if(no == 1)
		return 1;
	else
		return no*fact(no-1);
}
