//Return inside void function
#include <stdio.h>
#include <stdlib.h>

void printsum(void);

void main()
{
	printsum();
	exit(0);
}

void printsum()
{
	printf("This is void function\n");
	printf("Ths is a statement before return!...\n");
	return;
	printf("This is a statement after return!...\n");
	printf("Unreachable code!\n");
}
