//Indirectly returning more than one value
#include <stdio.h>
#include <stdlib.h>

void sum_diff(int ,int ,int *,int *);

void main()
{
	int a=10, b=2;
	int sum, diff;
	sum_diff(a, b, &sum, &diff);
	printf("Sum is %d\n",sum);
	printf("Difference is %d\n",diff);
	exit(0);
}

void sum_diff(int a,int b,int *sum,int *diff)
{
	*sum = a+b;
	*diff = a-b;
}
