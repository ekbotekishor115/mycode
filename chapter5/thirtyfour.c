// Functions that sums all the elements of an array
#include <stdio.h>
#include <stdlib.h>

int sumall(int array[],int num);

void main()
{
	int num, i, result, elements[20];
	printf("Enter the number of elements int the array (max. 20):\t");
	scanf("%d",&num);
	printf("Enter the elements:\n");
	for (i=0 ; i < num; i++)
	{
		scanf("%d",&elements[i]);
	}
	result = sumall(elements,num);
	printf("The sum of all the elements of the array is %d\n",result);
	
	exit(0);
}

int sumall(int array[], int num)
{
	int i, sum = 0;
	for (i=0 ; i < num; i++)
	{
		sum = sum + array[i];
	}
	return sum;
}
