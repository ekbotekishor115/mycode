// Matrix multiplication with the help of functions
#include <stdio.h>
#include <stdlib.h>

int mat_multiply(int mx1[][10],int m1,int n1,int mx2[][10],int m2,int n2,int mx3[][10]);

void main()
{
	int mx1[10][10], mx2[10][10], mx3[10][10] = {0};
	int m1, n1,m2, n2, i, j, indicator;
	printf("Enter the order of matrix-1 (max. 10 by 10)\t");
	scanf("%d %d",&m1, &n1);
	printf("Enter the elements of the matrix-1:\n");

	for(i=0; i<m1; i++)
	{
		for (j=0; j<n1; j++)
		{
			scanf("%d",&mx1[i][j]);
		}
	}

	printf("nter the order of matrix-2 (max. 10 by 10)\t");
	scanf("%d %d",&m2,&n2);
	printf("Enter the elements of matrix-2:\n");

	for (i=0; i<m2; i++)
	{
		for (j=0; j<n2; j++)
		{
			scanf("%d",&mx2[i][j]);
		}
	}

	indicator = mat_multiply(mx1,m1,n1,mx2,m2,n2,mx3);

	if (indicator == 0)
	{
		printf("Matrices are not compatible for multiplication\n");
	}
	else
	{
		printf("The result of matrix multiplication is :\n");
		for (i=0; i<m1; i++)
		{
			for (j=0; j<n2; j++)
			{
				printf("%d",mx3[i][j]);
			}
			printf("\n");
		}
	}
	exit(0);
}

int mat_multiply(int mx1[][10], int m1, int n1, int mx2[][10], int m2, int n2, int mx3[][10])
{
	int i,j,k;
	if (n1 != m2)
	{
		return 0;
	}
	else
	{
		for (i=0; i<m1; i++)
		{
			for (j=0; j<n2; j++)
			{
				for (k=0; k<n1; k++)
				{
					mx3[i][j] = mx3[i][j] + mx1[i][k] * mx2[k][j];
				}
			}
		}
		return 1;
	}
}
