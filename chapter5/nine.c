//Area of Triangle
#include <stdio.h>
#include <stdlib.h>

float circle_area(int);

void main()
{
	int radious;
	float area;
	printf("Enter the radious of circle\t");
	scanf("%d",&radious);
	area = circle_area(radious);
	printf("Area of circle is %f\n",area);
	exit(0);
}

float circle_area(int radious)
{
	return 3.1428 *radious * radious;
}

//SEGMENTATION FAULT with tool-chain
