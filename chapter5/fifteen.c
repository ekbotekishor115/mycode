//Individual elements of array passed by referance
#include <stdio.h>
#include <stdlib.h>

int sum_array(int*,int);

int main()
{
	int arr[10],nele,lc,sum=0;
	printf("Enter the no. of elements\t");
	scanf("%d",&nele);
	printf("Enter elements of array\n");
	for(lc=0;lc<nele;lc++)
		scanf("%d",&arr[lc]);
	for(lc=0;lc<nele;lc++)
	{
		sum=sum_array(&arr[lc],sum);
	}
	printf("Sum is %d\n",sum);
	exit(0);
}

int sum_array(int *elements, int sum)
{
	return sum+*elements;
}
