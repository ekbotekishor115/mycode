//Default arguments for a subdet of parameters
#include <stdio.h>
#include <stdlib.h>

int add(int a, int b=12, int c=8);
	
void main()
{
	add(10,12);
	exit(0);
}

int add(int a, int b, int c)
{
	printf("Result after the addition is %d\n",a+b+c);
}
