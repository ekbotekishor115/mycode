//Tail recursive factorial function
#include <stdio.h>
#include <stdlib.h>

int fact_tail (int, int);

void main()
{
	int no, factorial;
	printf("Enter the number\t");
	scanf("%d",&no);
	factorial = fact_tail(no,1);
	printf("Resultant factorial is %d\n",factorial);
	exit(0);
}

int fact_tail (int no,int result)
{
	if(no == 1)
		return result;
	else
		return fact_tail(no-1,no*result);
}
