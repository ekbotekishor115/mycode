//Default arguments
#include <stdio.h>
#include <stdlib.h>

void printsum(int, int , char base='D');

void main()
{
	printf("Use of default arguments:\n");
	printf("General conditions:\n");
	printsum(5,6);
	printsum(3,4);
	printf("Rare conditions:\n");
	printsum(6,9,'H');
	printsum(6,9,'O');
	exit(0);
}

void printsum(int x, int y,char base)
{
	if(base == 'd' || base == 'D')
		printf("Sum of %d and %d in Decimal is %d\n",x,y,x+y);
	else if(base == 'h' || base == 'H')
		printf("Sum of %d and %d in Hexadecimal is %d\n",x,y,x+y);
	else if(base == 'o' || base == 'O')
		printf("Sum of %d and %d in Octal is %d\n",x,y,x+y);
}
