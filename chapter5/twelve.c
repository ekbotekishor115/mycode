//Use of referance in swap function
#include <stdio.h>
#include <stdlib.h>

int swap(int *,int *);

void main()
{
	int a=10,b=20;
	printf("Before swap values are %d %d\n",a,b);
	swap(&a,&b);
	printf("After swap values are %d %d\n",a,b);
	exit(0);
}

int swap(int *x,int *y)
{
	*x=*x+*y;
	*y=*x-*y;
	*x=*x-*y;
	printf("In swap function values are %d %d\n",*x,*y);
}
