#include <stdio.h>
#include <stdlib.h>

void permute(int array[],int parray[],int L,int N)
{
	int i,j;
	
	if (L > N)
	{
		for (i=1 ; i <= N ; i++)
			printf("%d ",parray[i]);
		printf("\n");
	}
	else
	{
		for (i=1 ; i <= N; i++)
		{
			if (array[i] == 0)
			{
				parray[L] = i;
				array[i] = 1;
				permute(array,parray,L+1,N);
				array[i] = 0;
			}
		}
	}
}

void main()
{
	int array[10] = {0}, parray[10],n;
	printf("Generating permutations of 1 to n\n");
	printf("Enter the value of n(<10)\t");
	scanf("%d",&n);
	permute(array,parray,1,n);
}


