//Individual elements of an array passed by value
#include <stdio.h>
#include <stdlib.h>

int sum_array(int , int);

void main()
{
	int arr[10],nele,lc, sum=0;
	printf("Enter the number of elements\t");
	scanf("%d\n",&nele);
	printf("Enter the elements of array\n");
	for(lc=0;lc<nele;lc++)
		scanf("%d",&arr[lc]);
	for(lc=0;lc<nele;lc++)
	{
		sum= sum_array(arr[lc],sum);
	}
	printf("Sum is %d\n",sum);
	exit(0);
}

int sum_array(int elements,int sum)
{
	return sum+elements;
}
