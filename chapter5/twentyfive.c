//Tail Recursive Fibonacci Function
#include <stdio.h>
#include <stdlib.h>

int fib_tail(int, int, int);

void main()
{
	int n,term;
	printf("Enter term no.:\t");
	scanf("%d",&n);
	term = fib_tail(n,1,0);
	printf("Fibonacci term is %d\n",term);
	exit(0);
}

int fib_tail(int n, int next, int result)
{
	if(n == 1)
		return result;
	return fib_tail(n-1, next+result,next);
}
