// Variable argument function
// file stdarg.h is to be included for using va_list, va_start, etc.

#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>

// Ellipses (i.e. three dots) are used to declare variable argument function
int sum(int no_of_arguments,...);

// Function defination
void main()
{
	int result;
	// function sum invoked with four arguments
	result = sum(3,12,14,13);
	printf("The result of addition of 3 numbers is %d\n",result);
	//Function sum invoked with 6 arguments
	result = sum(5,10,20,30,40,50);
	printf("The result of addition of 5 numbers is %d\n",result);
	exit(0);
}

// Defination of variable argument function
int sum(int no_of_arguments,...)
{
	int arg, i=0, total=0;
	va_list ptr;
	va_start(ptr,no_of_arguments);
	arg = va_arg(ptr,int);
	while(i++ < no_of_arguments)
	{
		total += arg;
		arg = va_arg(ptr,int);
	}

	va_end(ptr);
	return total;
}
