//Passing 1-D array
#include <stdio.h>
#include <stdlib.h>

void find_max_min(int *,int);

void main()
{
	int arr[10],nele,lc,sum=0;
	printf("Enyer the no. of elements\t");
	scanf("%d",&nele);
	printf("Enter elements of array\n");
	for(lc=0;lc<nele;lc++)
		scanf("%d",&arr[lc]);
	find_max_min(arr,nele);
	printf("Max is %d\n",arr[0]);
	printf("Min is %d\n",arr[1]);
	exit(0);
}


void find_max_min(int*arr,int nele)
{
	int lc,max=arr[0],min=arr[0];
	for(lc=1;lc<nele;lc++)
		if(arr[lc]>max)
			max= arr[lc];
		else if(arr[lc]<min)
			min = arr[lc];
	arr[0]=max;
	arr[1]=min;
}
