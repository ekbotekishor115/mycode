	.file	"thirtyfive1.c"
	.section	.rodata
	.align 8
.LC0:
	.string	"Enter the order of matrix-1 (max. 10 by 10)\t"
.LC1:
	.string	"%d %d"
	.align 8
.LC2:
	.string	"Enter the elements of the matrix-1:"
.LC3:
	.string	"%d"
	.align 8
.LC4:
	.string	"nter the order of matrix-2 (max. 10 by 10)\t"
	.align 8
.LC5:
	.string	"Enter the elements of matrix-2:"
	.align 8
.LC6:
	.string	"Matrices are not compatible for multiplication"
	.align 8
.LC7:
	.string	"The result of matrix multiplication is :"
	.text
	.globl	main
	.type	main, @function
main:
.LFB2:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$1232, %rsp
	leaq	-1216(%rbp), %rdx
	movl	$0, %eax
	movl	$50, %ecx
	movq	%rdx, %rdi
	rep stosq
	movl	$.LC0, %edi
	movl	$0, %eax
	call	printf
	leaq	-1224(%rbp), %rdx
	leaq	-1220(%rbp), %rax
	movq	%rax, %rsi
	movl	$.LC1, %edi
	movl	$0, %eax
	call	__isoc99_scanf
	movl	$.LC2, %edi
	call	puts
	movl	$0, -4(%rbp)
	jmp	.L2
.L5:
	movl	$0, -8(%rbp)
	jmp	.L3
.L4:
	leaq	-416(%rbp), %rcx
	movl	-8(%rbp), %eax
	movslq	%eax, %rsi
	movl	-4(%rbp), %eax
	movslq	%eax, %rdx
	movq	%rdx, %rax
	salq	$2, %rax
	addq	%rdx, %rax
	addq	%rax, %rax
	addq	%rsi, %rax
	salq	$2, %rax
	addq	%rcx, %rax
	movq	%rax, %rsi
	movl	$.LC3, %edi
	movl	$0, %eax
	call	__isoc99_scanf
	addl	$1, -8(%rbp)
.L3:
	movl	-1224(%rbp), %eax
	cmpl	%eax, -8(%rbp)
	jl	.L4
	addl	$1, -4(%rbp)
.L2:
	movl	-1220(%rbp), %eax
	cmpl	%eax, -4(%rbp)
	jl	.L5
	movl	$.LC4, %edi
	movl	$0, %eax
	call	printf
	leaq	-1232(%rbp), %rdx
	leaq	-1228(%rbp), %rax
	movq	%rax, %rsi
	movl	$.LC1, %edi
	movl	$0, %eax
	call	__isoc99_scanf
	movl	$.LC5, %edi
	call	puts
	movl	$0, -4(%rbp)
	jmp	.L6
.L9:
	movl	$0, -8(%rbp)
	jmp	.L7
.L8:
	leaq	-816(%rbp), %rcx
	movl	-8(%rbp), %eax
	movslq	%eax, %rsi
	movl	-4(%rbp), %eax
	movslq	%eax, %rdx
	movq	%rdx, %rax
	salq	$2, %rax
	addq	%rdx, %rax
	addq	%rax, %rax
	addq	%rsi, %rax
	salq	$2, %rax
	addq	%rcx, %rax
	movq	%rax, %rsi
	movl	$.LC3, %edi
	movl	$0, %eax
	call	__isoc99_scanf
	addl	$1, -8(%rbp)
.L7:
	movl	-1232(%rbp), %eax
	cmpl	%eax, -8(%rbp)
	jl	.L8
	addl	$1, -4(%rbp)
.L6:
	movl	-1228(%rbp), %eax
	cmpl	%eax, -4(%rbp)
	jl	.L9
	movl	-1232(%rbp), %r9d
	movl	-1228(%rbp), %r8d
	movl	-1224(%rbp), %edx
	movl	-1220(%rbp), %esi
	leaq	-816(%rbp), %rcx
	leaq	-416(%rbp), %rax
	subq	$8, %rsp
	leaq	-1216(%rbp), %rdi
	pushq	%rdi
	movq	%rax, %rdi
	call	mat_multiply
	addq	$16, %rsp
	movl	%eax, -12(%rbp)
	cmpl	$0, -12(%rbp)
	jne	.L10
	movl	$.LC6, %edi
	call	puts
	jmp	.L11
.L10:
	movl	$.LC7, %edi
	call	puts
	movl	$0, -4(%rbp)
	jmp	.L12
.L15:
	movl	$0, -8(%rbp)
	jmp	.L13
.L14:
	movl	-8(%rbp), %eax
	movslq	%eax, %rcx
	movl	-4(%rbp), %eax
	movslq	%eax, %rdx
	movq	%rdx, %rax
	salq	$2, %rax
	addq	%rdx, %rax
	addq	%rax, %rax
	addq	%rcx, %rax
	movl	-1216(%rbp,%rax,4), %eax
	movl	%eax, %esi
	movl	$.LC3, %edi
	movl	$0, %eax
	call	printf
	addl	$1, -8(%rbp)
.L13:
	movl	-1232(%rbp), %eax
	cmpl	%eax, -8(%rbp)
	jl	.L14
	movl	$10, %edi
	call	putchar
	addl	$1, -4(%rbp)
.L12:
	movl	-1220(%rbp), %eax
	cmpl	%eax, -4(%rbp)
	jl	.L15
.L11:
	movl	$0, %edi
	call	exit
	.cfi_endproc
.LFE2:
	.size	main, .-main
	.globl	mat_multiply
	.type	mat_multiply, @function
mat_multiply:
.LFB3:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	movq	%rdi, -24(%rbp)
	movl	%esi, -28(%rbp)
	movl	%edx, -32(%rbp)
	movq	%rcx, -40(%rbp)
	movl	%r8d, -44(%rbp)
	movl	%r9d, -48(%rbp)
	movl	-32(%rbp), %eax
	cmpl	-44(%rbp), %eax
	je	.L17
	movl	$0, %eax
	jmp	.L18
.L17:
	movl	$0, -4(%rbp)
	jmp	.L19
.L24:
	movl	$0, -8(%rbp)
	jmp	.L20
.L23:
	movl	$0, -12(%rbp)
	jmp	.L21
.L22:
	movl	-4(%rbp), %eax
	movslq	%eax, %rdx
	movq	%rdx, %rax
	salq	$2, %rax
	addq	%rdx, %rax
	salq	$3, %rax
	movq	%rax, %rdx
	movq	16(%rbp), %rax
	leaq	(%rdx,%rax), %rcx
	movl	-4(%rbp), %eax
	movslq	%eax, %rdx
	movq	%rdx, %rax
	salq	$2, %rax
	addq	%rdx, %rax
	salq	$3, %rax
	movq	%rax, %rdx
	movq	16(%rbp), %rax
	addq	%rax, %rdx
	movl	-8(%rbp), %eax
	cltq
	movl	(%rdx,%rax,4), %esi
	movl	-4(%rbp), %eax
	movslq	%eax, %rdx
	movq	%rdx, %rax
	salq	$2, %rax
	addq	%rdx, %rax
	salq	$3, %rax
	movq	%rax, %rdx
	movq	-24(%rbp), %rax
	addq	%rax, %rdx
	movl	-12(%rbp), %eax
	cltq
	movl	(%rdx,%rax,4), %edi
	movl	-12(%rbp), %eax
	movslq	%eax, %rdx
	movq	%rdx, %rax
	salq	$2, %rax
	addq	%rdx, %rax
	salq	$3, %rax
	movq	%rax, %rdx
	movq	-40(%rbp), %rax
	addq	%rax, %rdx
	movl	-8(%rbp), %eax
	cltq
	movl	(%rdx,%rax,4), %eax
	imull	%edi, %eax
	leal	(%rsi,%rax), %edx
	movl	-8(%rbp), %eax
	cltq
	movl	%edx, (%rcx,%rax,4)
	addl	$1, -12(%rbp)
.L21:
	movl	-12(%rbp), %eax
	cmpl	-32(%rbp), %eax
	jl	.L22
	addl	$1, -8(%rbp)
.L20:
	movl	-8(%rbp), %eax
	cmpl	-48(%rbp), %eax
	jl	.L23
	addl	$1, -4(%rbp)
.L19:
	movl	-4(%rbp), %eax
	cmpl	-28(%rbp), %eax
	jl	.L24
	movl	$1, %eax
.L18:
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3:
	.size	mat_multiply, .-mat_multiply
	.ident	"GCC: (Ubuntu 5.4.0-6ubuntu1~16.04.12) 5.4.0 20160609"
	.section	.note.GNU-stack,"",@progbits
