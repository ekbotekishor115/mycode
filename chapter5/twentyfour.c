//Non-tail recursive Fibonacci functions
#include <stdio.h>
#include <stdlib.h>

int fib_norm(int);

void main()
{
	int n,term;
	printf("Enter term no\t");
	scanf("%d",&n);
	term = fib_norm(n);
	printf("Fibonacci term is %d \n",term);
	exit(0);
}

int fib_norm(int n)
{
	if(n == 1)
		return 0;
	if(n == 2)
		return 1;
	return fib_norm(n-1) + fib_norm(n-2);
}
