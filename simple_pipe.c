#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/wait.h>
#include <string.h>

#define BUF_SIZE 10

int main(int argc,char **argv)
{
	int pfd[2];
	char buf[BUF_SIZE];
	ssize_t numRead;

	if (argc != 2 || strcmp(argv[1],"--help") == 0)
		printf("%s string\n",argv[0]);

	if (pipe(pfd) == -1)
		perror("pipe");

	switch(fork())
	{
		case -1:
			perror("fork");
		case 0:
			if (close(pfd[1]) == -1)
				perror("close - child");
		
			for (;;)
			{
				numRead = read(pfd[0],buf,BUF_SIZE);
				if (numRead == -1)
					perror("read");
				if (numRead == 0)
					break;
				if (write(STDOUT_FILENO,buf,numRead) != numRead)
					perror("child - partial/failed write");
					exit(EXIT_FAILURE);
			}


			write(STDOUT_FILENO,"\n",1);
			if (close(pfd[0]) == -1)
				perror("close");
			_exit(EXIT_SUCCESS);
		
		default:
			if (close(pfd[0]) == -1)
				perror("close - parent");
			
			if (write(pfd[1],argv[1],strlen(argv[1])) != strlen(argv[1]))
				perror("parent - partial/failed write");
				exit(EXIT_FAILURE);	
		

			if (close(pfd[1]) == -1)
				perror("close");
			wait(NULL);
			exit(EXIT_SUCCESS);
	}
}
