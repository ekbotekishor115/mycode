#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include "semun.h"
#include "svshm_xfr.h"

int main(int argc,char **argv)
{
	int semid,shmid,bytes,xfrs;
	struct shmseg *shmp;
	union semun dummy;

	semid = semget(SEM_KEY,2,IPC_CREAT | OBJ_PERMS);
	if (semid == -1)
	{
		perror("semget");
		exit(EXIT_FAILURE);
	}
	if (initSemAvailable(semid,WRITE_SEM) == -1)
	{
		perror("initSemAvailable");
		exit(EXIT_FAILURE);
	}
	if (initSemInUse(semid,READ_SEM) == -1)
	{
		perror("initSemInUse");
		exit(EXIT_FAILURE);
	}

	shmid = shmget(SHM_KEY,sizeof(struct shmseg),IPC_CREAT | OBJ_PERMS);
	if (shmid == -1)
	{
		perror("shmget");
		exit(EXIT_FAILURE);
	}

	shmp = shmat(shmid,NULL,0);
	if (shmp == (void *) -1)
	{
		perror("shmat");
		exit(EXIT_FAILURE);
	}

	for (xfrs = 0,bytes = 0;;xfrs++,bytes += shmp->cnt)
	{
		if (reverseSem(semid,WRITE_SEM) == -1)
		{
			perror("reverseSem");
			exit(EXIT_FAILURE);
		}
		
		shmp->cnt = read(STDIN_FILENO,shmp->buf,BUF_SIZE);
		if (shmp->cnt == -1)
		{
			perror("read");
			exit(EXIT_FAILURE);
		}
	
		if (releaseSem(semid,READ_SEM) == -1)
		{
			perror("releaseSem");
			exit(EXIT_FAILURE);
		}
		
		if (shmp->cnt == 0)
			break;
	}

	if (reverseSem(semid,WRITE_SEM) == -1)
	{
		perror("reverseSem");
		exit(EXIT_FAILURE);
	}

	if (semctl(semid,0,IPC_RMID,dummy) == -1)
	{
		perror("semctl");
		exit(EXIT_FAILURE);
	}
	if (shmdt(shmp) == -1)
	{
		perror("shmdt");
		exit(EXIT_FAILURE);
	}
	if (shmctl(shmid,IPC_RMID,0) == -1)
	{
		perror("shmctl");
		exit(EXIT_FAILURE);
	}

	fprintf(stderr,"Sent %d bytes (%d xfrs)\n",bytes,xfrs);
	exit(EXIT_SUCCESS);
	
}
