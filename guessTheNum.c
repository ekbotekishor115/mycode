#include <stdio.h>
#include <stdlib.h>
#include <linux/random.h>
#include <unistd.h>

int randnum(int maxV)
{
	int randV;
	int val = getrandom(&randV, sizeof(int), GRND_NONBLOCK);
	if(val < 0)
	{
		return(-1 * randV % maxV + 1);
	}
	else
	{
		return(randV % maxV + 1);
	}
}

int main()
{
	int number;
	int guess;
	
	number = randnum(100);
	printf("Guess a number between 1 and 100\n");

	do
	{
		scanf("%d", &guess);
		
		if(guess < number)
		{
			printf("Too Low\n");
		}
		else if(guess > number)
		{
			printf("Too High\n");
		}
	}
	while(guess != number);
	
	printf("Thats Right!!\n");

	return 0;
}
