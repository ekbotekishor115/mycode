#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <unistd.h>
#include <errno.h>

static void tstpHandler(int sig)
{
	sigset_t tstpMask,prevMask;
	int savedError;
	struct sigaction sa;

	savedError = errno;

	printf("Caught SIGTSTP\n");

	if (signal(SIGTSTP,SIG_DFL) == SIG_ERR)
		perror("signal");
	
	raise(SIGTSTP);

	sigemptyset(&tstpMask);
	sigaddset(&tstpMask,SIGTSTP);
	if (sigprocmask(SIG_UNBLOCK,&tstpMask,&prevMask) == -1)
		perror("sigprocmask");

	if (sigprocmask(SIG_SETMASK,&prevMask,NULL) == -1)
		perror("sigprocmask");

	sigemptyset(&sa.sa_mask);
	sa.sa_flags = SA_RESTART;
	sa.sa_handler = tstpHandler;
	if (sigaction(SIGTSTP,&sa,NULL) == -1)
		perror("sigaction");

	printf("Exiting SIGTSTP handler\n");
	errno = savedError;
}

int main(int argc,char **argv)
{
	struct sigaction sa;

	if (sigaction(SIGTSTP,NULL,&sa) == -1)
		perror("sigaction");

	if (sa.sa_handler != SIG_IGN)
	{
		sigemptyset(&sa.sa_mask);
		sa.sa_flags = SA_RESTART;
		sa.sa_handler = tstpHandler;
		if (sigaction(SIGTSTP,&sa,NULL) == -1)
			perror("sigaction");
	}

	for (;;)
	{
		pause();
		printf("Main\n");
	}
}
