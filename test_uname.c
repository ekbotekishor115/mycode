#include <stdio.h>
#include <stdlib.h>
#include <sys/utsname.h>

int main()
{
	struct utsname uts;
	int ret = uname(&uts);

	if(ret < 0)
	{
		perror("uname");
		exit(EXIT_FAILURE);
	}
	
	printf("Node name: %s\n",uts.nodename);
	printf("System name: %s\n",uts.sysname);
	printf("Release: %s\n",uts.release);
	printf("Version: %s\n",uts.version);
	printf("Machine: %s\n",uts.machine);
}
