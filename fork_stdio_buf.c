#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

int main(int argc, char **argv)
{
	printf("Hello World!\n");
	write(STDOUT_FILENO, "Kishor\n",7);
	
	if(fork() == -1)
		perror("fork");

	exit(EXIT_SUCCESS);
}
