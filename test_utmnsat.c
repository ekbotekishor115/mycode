#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <sys/stat.h>

int main()
{
	struct timespec time_s[2];
	time_s[0].tv_sec = 0;
	time_s[0].tv_nsec = UTIME_NOW;
	time_s[1].tv_sec = 0;
	time_s[1].tv_nsec = UTIME_OMIT;
	int ret = utimensat(AT_FDCWD,"./login.c",time_s,0);
	if(ret < 0)
	{
		perror("utimensat");
		exit(EXIT_FAILURE);
	}
	return 0;
}
