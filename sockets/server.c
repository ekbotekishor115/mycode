#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <unistd.h>
#include <stdbool.h>
#include <pthread.h>
#include <signal.h>
#include <mqueue.h>
#include <fcntl.h>
#include <limits.h>
#include <sys/stat.h>
#include <sys/time.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <netinet/in.h>

struct thread_data 
{
	int socket;
	pthread_t ptid;
	bool is_run;
	struct thread_data *th_data_next;
};

struct dummy_node
{
	int num;
	struct thread_data *th_data_head;
};

bool init_sig();
void sig_int_handler(int num);
void *serv_client(void *arg);
struct thread_data *get_empty_thread_data_struct(struct dummy_node *head);
void cleanup(struct dummy_node *head);
void *serve_request(void *arg);
void remove_from_list(struct dummy_node *head, pthread_t pid);

static volatile bool is_interrupted = false;

int main(int argc, char **argv)
{
	if (argc != 2)
	{
		printf("Usage Error,%s <listening port number>\n",argv[0]);
		return EXIT_FAILURE;
	}

	uint32_t temp = atoi(argv[1]);
	uint16_t port = (uint32_t) temp;

	//TODO port error check
	//Create master socket

	int master_socket = 0, new_socket = 0, addrlen = 0;
	int opt = true;
	struct sockaddr_in address;
	fd_set readfds;
	int max_sd, sd;
	int activity, th_num = 0;
	
	if (init_sig() == false)
	{
		printf("Signal init failed\n");
		return EXIT_FAILURE;
	}
	if ((master_socket = socket(AF_INET, SOCK_STREAM, 0)) == 0)
	{
		perror("Socket failed");
		exit(EXIT_FAILURE);
	}

	//set master socket to allow multiple connections
	if (setsockopt(master_socket,SOL_SOCKET, SO_REUSEADDR, (char *)&opt, sizeof(opt)) < 0)
	{
		perror("setsockopt");
		exit(EXIT_FAILURE);
	}

	//type of socket created
	address.sin_family = AF_INET;
	address.sin_addr.s_addr = INADDR_ANY;
	address.sin_port = htons(port);

	if (bind(master_socket, (struct sockaddr *)&address, sizeof(address)) < 0)
	{
		perror("bind");
		exit(EXIT_FAILURE);
	}

	//try to specify max. of 3 pending connections the master socket
	if (listen(master_socket,3) < 0)
	{
		perror("listen");
		exit(EXIT_FAILURE);
	}

	addrlen = sizeof(address);
	struct dummy_node *dummy_head = malloc(sizeof(struct dummy_node));
	dummy_head->num = 0;
	dummy_head->th_data_head = NULL;

	while (is_interrupted == false)
	{
		//clear the socket
		new_socket = 0;
		FD_ZERO(&readfds);
	
		//add master socket to set 
		FD_SET(master_socket, &readfds);
		max_sd = master_socket;

		//wait for an activity on one of the socket, timeout is NULL
		//so wait indefinitely
		activity = select(max_sd + 1, &readfds, NULL, NULL, NULL);
		
		if ((activity < 0) && (errno != EINTR))
		{
			printf("select error");
		}
		
		//if something happened on master socket,
		//then its an incoming connection

		if ((FD_ISSET(master_socket, &readfds)) && is_interrupted == false)
		{
			if ((new_socket = accept(master_socket,(struct sockaddr*) &address,(socklen_t*)&addrlen)) < 0)
			{
				perror("accept");
				//go to cleanup
			}
			//inform user of socket number ~used in send and receive
			printf("New connection socket fd %d, ip is : %s , port : %hd\n",new_socket, inet_ntoa(address.sin_addr), ntohs(address.sin_port));
			struct thread_data *data = get_empty_thread_data_struct(dummy_head);
			data->socket = new_socket;
			data->is_run = true;
			data->th_data_next = NULL;
			pthread_create(&data->ptid, NULL, &serv_client, data);

		}
	}

	struct thread_data *th_temp = dummy_head->th_data_head;
	for (int num_of_thread = 0; num_of_thread != dummy_head->num; num_of_thread++)
	{
		th_temp->is_run = false;
		pthread_join(th_temp->ptid, NULL);
		th_temp = th_temp->th_data_next;
	}
	close(master_socket);
	cleanup(dummy_head);
	return 0;
}

void sig_int_handler(int num)
{
	printf("Interrupt received\r\n");
	is_interrupted = true;
}

struct thread_data *get_empty_thread_data_struct(struct dummy_node *head)
{
	struct thread_data *new_node = malloc(sizeof(struct thread_data));
	struct thread_data *temp = head->th_data_head;
	if (temp == NULL)
	{
		head->num +=1;
		head->th_data_head = new_node;
		return new_node;
	}
	else
	{
		while (temp->th_data_next != NULL)
		{
			temp = temp->th_data_next;
		}
		head->num +=1;
		temp->th_data_next = new_node;
		return new_node;
	}
}

void cleanup(struct dummy_node *head)
{
	struct thread_data *th_data = head->th_data_head;
	if (th_data == NULL)
	{
		free(head);
	}
	else
	{
		while (true)
		{
			struct thread_data *temp = th_data->th_data_next;
			free(th_data);
			if (temp == NULL)
			{
				break;
			}
			th_data = temp;
		}
		free(head);
	}
}

bool init_sig()
{
	struct sigaction sigaction_new, sigaction_old;
	sigset_t sig_set;
	sigfillset(&sig_set);
	int ret = pthread_sigmask(SIG_BLOCK, &sig_set, NULL);
	if (ret != 0)
	{
		perror("pthread_sigmask");
		return false;
	}

	//install signal handler for SIGINT
	//Block all signals
	sigaction_new.sa_handler = &sig_int_handler;
	sigaction_new.sa_flags = 0;
	sigfillset(&sigaction_new.sa_mask);
	
	ret = sigaction(SIGINT, &sigaction_new, &sigaction_old);
	if (ret != 0)
	{
		perror("sigaction");
		return false;
	}
	sigemptyset(&sig_set);
	sigaddset(&sig_set, SIGINT);

	ret = pthread_sigmask(SIG_UNBLOCK, &sig_set, NULL);
	if (ret != 0)
	{
		perror("pthread_sigmask2");
		return false;
	}
	return true;
}

void *serv_client(void *arg)
{
	struct thread_data *th_data = (struct thread_data *)arg;
	fd_set readfds;
	FD_SET(th_data->socket, &readfds);
	int activity, valread = -1;
	char buffer[1024] = {0};
	struct sockaddr_in address = {0};
	int addrlen = sizeof(address);
	while (th_data->is_run)
	{
		activity = select(th_data->socket + 1, &readfds, NULL, NULL,NULL);
		if ((activity < 0) && (errno != EINTR))
		{
			printf("select error\n");
		}
		if (FD_ISSET(th_data->socket, &readfds))
		{
			//check if it was for closing, and also read the incoming msg
			if ((valread = read(th_data->socket, buffer, 1024)) == 0)
			{
				//somebody disconnected ,get his details and print
				getpeername(th_data->socket,(struct sockaddr *)&address, (socklen_t *)&addrlen); 
				printf("Host disconnected, ip %s, port %d\n",inet_ntoa(address.sin_addr), ntohs(address.sin_port));
				th_data->is_run = false;
			}
			buffer[valread] = '\0';
			printf("message from server tid:%ld | msg: %s\n",th_data->ptid, buffer);
			if ((send(th_data->socket,buffer, strlen(buffer), 0) < 0))
			{
				perror("server disconnected error");
				getpeername(th_data->socket, (struct sockaddr *)&address,(socklen_t *)&addrlen);
				printf("Host disconnected, ip %s, port %d\n",inet_ntoa(address.sin_addr), ntohs(address.sin_port));
	
				th_data->is_run = false;
			}
		}
	}
	close(th_data->socket);
	return NULL;
}
