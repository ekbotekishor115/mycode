#include <stdio.h>
#include <stdlib.h>

int main ()
{
	float v, a, u, t;
	printf("Enter the initial velocity in m/s\t");
	scanf("%f",&u);
	printf("Enter the amount of acceleration\t");
	scanf("%f",&a);
	printf("Enter the time in sec\t");
	scanf("%f",&t);
	v = u + a * t;
	printf("Velocity after %4.2f sec is %4.2f m/s\n",t,v);
	exit (0);
}
