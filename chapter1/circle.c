#include <stdio.h>

int main()
{
	float r,C,A;
	printf("Enter the radius of circle\t");
	scanf("%f",&r);
	C=2*r*22.0/7;
	A=r*r*22.0/7;
	printf("Circumference of circle is %6.2f\n",C);
	printf("Area of circle is %6.2f\n",A);
	return 0;	
}
