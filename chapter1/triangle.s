	.file	"triangle1.c"
	.section	.rodata
	.align 8
.LC1:
	.string	"Enter the sides of the Triangle\t"
.LC2:
	.string	"%f %f %f"
.LC3:
	.string	"scan done"
.LC5:
	.string	"Calc 1"
.LC6:
	.string	"Calc 2"
	.align 8
.LC7:
	.string	"Area of triangle is %6.2f sq units\n"
	.text
	.globl	main
	.type	main, @function
main:
.LFB2:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$32, %rsp
	pxor	%xmm0, %xmm0
	movss	%xmm0, -4(%rbp)
	movl	$.LC1, %edi
	movl	$0, %eax
	call	printf
	leaq	-20(%rbp), %rcx
	leaq	-16(%rbp), %rdx
	leaq	-12(%rbp), %rax
	movq	%rax, %rsi
	movl	$.LC2, %edi
	movl	$0, %eax
	call	__isoc99_scanf
	movl	$.LC3, %edi
	call	puts
	movss	-12(%rbp), %xmm1
	movss	-16(%rbp), %xmm0
	addss	%xmm1, %xmm0
	movss	-20(%rbp), %xmm1
	addss	%xmm1, %xmm0
	movss	.LC4(%rip), %xmm1
	divss	%xmm1, %xmm0
	movss	%xmm0, -8(%rbp)
	movl	$.LC5, %edi
	call	puts
	movss	-12(%rbp), %xmm1
	movss	-8(%rbp), %xmm0
	subss	%xmm1, %xmm0
	movaps	%xmm0, %xmm1
	mulss	-8(%rbp), %xmm1
	movss	-16(%rbp), %xmm2
	movss	-8(%rbp), %xmm0
	subss	%xmm2, %xmm0
	mulss	%xmm0, %xmm1
	movss	-20(%rbp), %xmm2
	movss	-8(%rbp), %xmm0
	subss	%xmm2, %xmm0
	mulss	%xmm1, %xmm0
	cvtss2sd	%xmm0, %xmm0
	call	sqrt
	cvtsd2ss	%xmm0, %xmm3
	movss	%xmm3, -4(%rbp)
	movl	$.LC6, %edi
	call	puts
	cvtss2sd	-4(%rbp), %xmm0
	movl	$.LC7, %edi
	movl	$1, %eax
	call	printf
	movl	$0, %edi
	call	exit
	.cfi_endproc
.LFE2:
	.size	main, .-main
	.section	.rodata
	.align 4
.LC4:
	.long	1073741824
	.ident	"GCC: (Ubuntu 5.4.0-6ubuntu1~16.04.12) 5.4.0 20160609"
	.section	.note.GNU-stack,"",@progbits
