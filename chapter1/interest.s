	.file	"interest1.c"
	.section	.rodata
	.align 8
.LC0:
	.string	"Enter principle, rate and time\t"
.LC1:
	.string	"%f %f %f"
.LC3:
	.string	"Simple Interest is %f6.2\n"
.LC4:
	.string	"Maturity Amount is %f6.2\n"
	.text
	.globl	main
	.type	main, @function
main:
.LFB2:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$32, %rsp
	movl	$.LC0, %edi
	movl	$0, %eax
	call	printf
	leaq	-20(%rbp), %rcx
	leaq	-16(%rbp), %rdx
	leaq	-12(%rbp), %rax
	movq	%rax, %rsi
	movl	$.LC1, %edi
	movl	$0, %eax
	call	__isoc99_scanf
	movss	-12(%rbp), %xmm1
	movss	-16(%rbp), %xmm0
	mulss	%xmm1, %xmm0
	movss	-20(%rbp), %xmm1
	mulss	%xmm1, %xmm0
	movss	.LC2(%rip), %xmm1
	divss	%xmm1, %xmm0
	movss	%xmm0, -4(%rbp)
	movss	-12(%rbp), %xmm0
	addss	-4(%rbp), %xmm0
	movss	%xmm0, -8(%rbp)
	cvtss2sd	-4(%rbp), %xmm0
	movl	$.LC3, %edi
	movl	$1, %eax
	call	printf
	cvtss2sd	-8(%rbp), %xmm0
	movl	$.LC4, %edi
	movl	$1, %eax
	call	printf
	movl	$0, %edi
	call	exit
	.cfi_endproc
.LFE2:
	.size	main, .-main
	.section	.rodata
	.align 4
.LC2:
	.long	1120403456
	.ident	"GCC: (Ubuntu 5.4.0-6ubuntu1~16.04.12) 5.4.0 20160609"
	.section	.note.GNU-stack,"",@progbits
