#include <stdio.h>

int main()
{
	int number1, number2, number3, number4;
	printf("Enter numbers\t");
	scanf("%d %d %d", &number1, &number2, &number3);
	printf("The numbers before swap %d %d %d\n", number1, number2, number3);
	number4 = number1;
	number1 = number2;
	number2 = number3;
	number3 = number4;
	printf("The numbers after swap %d %d %d\n", number1, number2, number3);
	return 0;
}
