#include <stdio.h>
#include <stdlib.h>

int main()
{
	float p, roi, t, I, amt;
	printf("Enter principle, rate and time\t");
	scanf("%f %f %f",&p,&roi,&t);
	I = p*roi*t/100;
	amt = p+I;
	printf("Simple Interest is %f6.2\n",I);
	printf("Maturity Amount is %f6.2\n",amt);
	exit(0);
}

