#include <stdio.h>

int main()
{
	int n1, n2;
	printf("Enter numbers\t");
	scanf("%d %d",&n1, &n2);
	printf("Numbers before swap %d %d\n",n1, n2);
	n2=n1+n2;
	n1=n2-n1;
	n2=n2-n1;
	printf("Numbers after swap %d %d\n",n1, n2);
	return 0;
}
