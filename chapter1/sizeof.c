#include <stdio.h>

int main()
{
	printf("Characters take %ld bytes in memory\n", sizeof(char));
	printf("Integers take %ld bytes in memory\n", sizeof(int));
	printf("Float takes %ld bytes in memory\n", sizeof(float));
	printf("Long takes %ld bytes in memory\n", sizeof(long));
	printf("Double takes %ld bytes in memory\n", sizeof(double));
//	printf(sizeof(int));
	return 0;
}
