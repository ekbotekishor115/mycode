#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>

struct node
{
	int data;
	struct node *next;
};

void* xmalloc(size_t size);
void create_list(struct node* head, int num_of_node);
void display(struct node *head);
void destroy_list(struct node *head);


int main(int argc, char **argv)
{
	if(argc != 2)
	{
		printf("Usage Error, %s, <node count>\n",argv[0]);
		exit(EXIT_FAILURE);
	}
	int count = atoi(argv[1]);
	if(count < 0)
	{
		printf("Number should be positive\n");
		exit(EXIT_FAILURE);
	}
	struct node *head = (struct node*) xmalloc(sizeof(struct node));
	head->data = rand();
	head->next = NULL;
	create_list(head,count);
	display(head);
	destroy_list(head);	
}

void* xmalloc(size_t size)
{
	void* temp = malloc(size);
	if(!temp)
	{
		perror("malloc");
		exit(EXIT_FAILURE);
	}
}

void create_list(struct node* head, int num_of_node)
{
	struct node* first = head;
	for(int i=0; i<num_of_node; i++)
	{
		struct node* temp=(struct node*)xmalloc(sizeof(struct node));
		temp->data = rand();
		temp->next = NULL;
		if(head->next ==NULL)
		{
			head->next = temp;
		}
		else
		{
			while(first->next !=NULL)
			{
				first = first->next;
			}
			first->next = temp;
		}
	}
}

void display(struct node *head)
{
	struct node *first = head;
	printf("START LINKED LIST\n");
	while(first->next !=NULL)
	{
		fprintf(stdout,"%d|->",first->data);
		first = first->next;
	}
	fprintf(stdout,"NULL\n");
	printf("END LINKED LIST\n");
}

void destroy_list(struct node *head)
{
	while(head->next != NULL)
	{
		struct node *temp = head->next;
		free(head);
		head = temp;
	}
	free(head);
}
