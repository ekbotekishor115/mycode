#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/sem.h>
#include <time.h>
#include <string.h>
#include "semun.h"

int main(int argc,char **argv)
{
	struct semid_ds ds;
	union semun arg,dummy;
	int semid,j;

	if (argc != 2 || strcmp(argv[1],"--help") == 0)
	{
		printf("%s semid\n",argv[0]);
		exit(EXIT_FAILURE);
	}

	semid = atoi(argv[1]);

	arg.buf = &ds;
	if (semctl(semid,0,IPC_STAT,arg) == -1)
	{
		perror("semctl");
		exit(EXIT_FAILURE);
	}

	printf("Semaphore changed: %s\n",ctime(&ds.sem_ctime));
	printf("Last semop():	%s\n",ctime(&ds.sem_otime));

	arg.array = calloc(ds.sem_nsems,sizeof(arg.array[0]));
	if (arg.array == NULL)
	{
		perror("calloc");
		exit(EXIT_FAILURE);
	}
	if (semctl(semid,0,GETALL,arg) == -1)
	{
		perror("semctl-GETALL");
		exit(EXIT_FAILURE);
	}
	
	printf("Sem # Value SEMPID SEMNCNT SEMZCNT\n");

	for (j = 0; j < ds.sem_nsems; j++)
	{
		printf("%3d	%5d	%5d	%5d	%5d\n",j,arg.array[j],semctl(semid,j,GETPID,dummy),semctl(semid,j,GETNCNT,dummy),semctl(semid,j,GETZCNT));
	}
	
	exit(EXIT_SUCCESS);
}
