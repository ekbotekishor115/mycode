#include <stdio.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/wait.h>
#include <unistd.h>

int main(int argc, char *argv[])
{
	int fd ,flags;
	char template[] = "/tmp/testtXXXXXX";
	
	setbuf(stdout, NULL);
	
	fd = mkstemp(template);
	if(fd == -1)
		perror("mkstemp");
	
	printf("File offset befoer fork(): %lld\n",(long long) lseek(fd, 0,SEEK_CUR));
	
	flags = fcntl(fd, F_GETFL);
	if(flags == -1)
		perror("fcntl - F_GETFL");
	printf("O_APPEND flag before fork() is: %s\n",(flags & O_APPEND) ? "no" : "off");
	switch(fork())
	{
		case -1:
			perror("fork");
			break;
		case 0:
			if(lseek(fd,1000,SEEK_SET)== -1)
				perror("lseek");
			flags = fcntl(fd,F_GETFL);
			if(flags == -1)
				perror("fcntl - F_GETFL");
			flags |= O_APPEND;
			if(fcntl(fd, F_SETFL, flags) == -1)
				perror("fcntl -F_SETFL");
			_exit(EXIT_SUCCESS);
		
		default:
			if(wait (NULL) == -1)
				perror("wait");
			printf("Chld has exited\n");
			
			printf("File offset in Parent: %lld\n",(long long) lseek(fd, 0, SEEK_CUR));
			
			flags = fcntl(fd,F_GETFL);
			if(flags == -1)
				perror("fcntl - F_GETFL");
			printf("O_APPEND flag in Parent is: %s\n",(flags & O_APPEND) ? "on" : "off");
			exit(EXIT_SUCCESS);
	}
}
