#include <stdio.h>
#include <fcntl.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

int main(int argc,char*argv[])
{
	char f[10],s[10];
	char buf[BUFSIZ];
	
	if(argc != 3)
	{
		printf("%s,wrong argv\n",argv[0]);
		exit(EXIT_FAILURE);
	}
	strcpy(f,argv[1]);
	strcpy(s,argv[2]);

	printf("first argument is %s\n second argument is %s\n",f,s);

	//opening the source file for reading
	
	int fd = open(f,O_RDONLY);
	if(fd < 0)
	{
		perror("Open system call");
		exit(EXIT_FAILURE);
	}
	
	//reading the content in source file
	ssize_t count = read(fd,buf,BUFSIZ);
	if(count < 0)
	{
		perror("Read system call");
		exit(EXIT_FAILURE);
	}
	

	//closing the source file
	int cl_fd = close(fd);
	if(cl_fd < 0)
	{
		perror("File not closed");
	}
	
	//creating destination file
	int cr_fd = creat(s,S_IRUSR | S_IWUSR | S_IRGRP | S_IWGRP | S_IROTH | S_IWOTH);
	if(cr_fd < 0)
	{
		perror("Create file failed");
		exit(EXIT_FAILURE);
	}

	//writing into the destination file
	ssize_t wr_count = write(cr_fd,buf,count);
	if(wr_count < 0)
	{
		perror("Write failed");
		close(cr_fd);
		exit(EXIT_FAILURE);
	}
}
