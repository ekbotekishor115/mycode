#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

int main()
{
	long max_byte_size = sysconf(_SC_ARG_MAX);
	printf("max_byte_size=%ld\n",max_byte_size);
	long LOGIN_NAME_MAX = sysconf(_SC_LOGIN_NAME_MAX);
	printf("LOGIN_NAME_MAX=%ld\n",LOGIN_NAME_MAX);
	long OPEN_MAX = sysconf(_SC_OPEN_MAX);
        printf("OPEN_MAX=%ld\n",OPEN_MAX);
        long NGROUPS_MAX = sysconf(_SC_NGROUPS_MAX);
        printf("NGROUPS_MAX=%ld\n",NGROUPS_MAX);
	long RTSIG_MAX = sysconf(_SC_RTSIG_MAX);
        printf("RTSIG_MAX=%ld\n",RTSIG_MAX);
        long SIGQUEUE_MAX = sysconf(_SC_SIGQUEUE_MAX);
        printf("SIGQUEUE_MAX=%ld\n",SIGQUEUE_MAX);
	long STREAM_MAX = sysconf(_SC_STREAM_MAX);
        printf("STREAM_MAX=%ld\n",STREAM_MAX);
        long NAME_MAX = sysconf(_PC_NAME_MAX);
        printf("NAME_MAX=%ld\n",NAME_MAX);
	long PATH_MAX = sysconf(_PC_PATH_MAX);
        printf("PATH_MAX=%ld\n",PATH_MAX);
        long PIPE_BUF = sysconf(_PC_PIPE_BUF);
        printf("PIPE_BUF=%ld\n",PIPE_BUF);
	long time_unit = sysconf(_SC_CLK_TCK);
        printf("time_unit=%ld\n",time_unit);
        long PAGE_SIZE_MAX = sysconf(_SC_PAGESIZE);
        printf("PAGE_SIZE_MAX=%ld\n",PAGE_SIZE_MAX);


	return 0;
}
