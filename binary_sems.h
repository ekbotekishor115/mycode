#ifndef BINARY_SEMS_H
#define BINARE_SEMS_H

#include <stdbool.h>

bool bsUseSemUndo;

bool bsRetryOnEintr;

int initSemAvailable(int semId, int semNum);

int initSemInUse(int semId, int semNum);

int reverseSem(int semId, int semNum);

int releaseSem(int semId, int semNum);

#endif
