#include <stdio.h>
#include <stdlib.h>

struct test
{
	int num1;
	char c;
	int num2;
}

struct test t = {10,'A',0x10430257};
int n1,n2;
char x;

int main()
{
	n1 = t.num1;
	n2 = t.num2;
	x = t.c;

	t.num1 = 100;
	t.num2 = 200;
	t.c = 'z';
	exit(0);
}

