.section .rodata
	msgp1:
	.string "%s<total count of numbers to be sorted>\n"
	msg_success:
	.string	"merge sort success\n"
	
	msg_failed:
	.string	"merge sort failed\n"
	
	msg_output:
	.string	"arr[%d] = %d\n"
	
	msg_calloc:
	.string	"memory can not be allocated\n"

	msg_test:
	.string	"Testing SegFault %d\n"
.section .text
	.globl	main
	.type	main, @function

main:
	pushl	%ebp
	movl	%esp, %ebp
	andl	$-16, %esp
	subl	$16, %esp

	movl	4(%ebp), %eax
	cmpl	$2, %eax
	je	continue
	movl	$msgp1, (%esp)
	movl	8(%ebp), %ebx
	movl	%ebx, 4(%esp)
	call	printf
	movl	$-1, (%esp)
	call	exit

continue:
	movl	12(%ebp), %eax
	movl	%eax, (%esp)
	call	atoi
	movl	%eax, -4(%ebp)
	movl	%eax, (%esp)
	movl	$4, 4(%esp)
	call	xcalloc
	movl	%eax, -8(%ebp)
	movl	-4(%ebp), %ebx
	movl	-8(%ebp), %eax
	movl	%eax, (%esp)
	movl	%ebx, 4(%esp)
	call	input
	call	sort
	call	output
	call	test_sort
	call	x_free
	
	movl	$0, (%esp)
	call	exit

	.globl	test_sort
	.type	test_sort, @function

test_sort:
	pushl	%ebp
	movl	%esp, %ebp
	subl	$16, %esp
	
	movl	$0, -4(%ebp)
	jmp	for_test

if_test:
	movl	-4(%ebp), %eax
	movl	8(%ebp) ,%ebx
	movl	(%ebx,%eax,4), %ecx
	addl	$1, %eax
	movl	(%ebx,%eax,4), %edx
	cmpl	%edx, %ecx
	jl	if_out
	movl	$msg_failed, (%esp)
	call	printf
	movl	%ebp, %esp
	popl	%ebp
	ret
if_out:
	addl	$1, -4(%ebp)
	
for_test:
	movl	-4(%ebp), %eax
	movl	12(%ebp), %ebx
	subl	$1, %ebx
	cmpl	%ebx, %eax
	jl	if_test
	movl	$msg_success, (%esp)
	call	printf
	
	movl	%ebp, %esp
	popl	%ebp
	ret

	.globl	output
	.type	output, @function

output:
	pushl	%ebp
	movl	%esp, %ebp
	subl	$16, %esp

	movl	$0, -4(%ebp)	
	jmp	for_op_cond

for_op:
	movl	8(%ebp), %edx
	movl	-4(%ebp), %ecx
	movl	(%edx,%ecx,4), %ebx
	movl	$msg_output, (%esp)
	movl	%ecx, 4(%esp)
	movl	%ebx, 8(%esp)
	call	printf
	addl	$1, -4(%ebp)

for_op_cond:
	movl	12(%ebp), %eax
	movl	-4(%ebp), %ebx
	cmpl	%eax, %ebx
	jl	for_op

	movl	%ebp, %esp
	popl	%ebp
	ret

	.globl	sort
	.type	sort, @function
sort:
	pushl	%ebp
	movl	%esp, %ebp
	subl	$16, %esp
	
	movl	8(%ebp), %eax
	movl	12(%ebp), %ebx
	subl	$1, %ebx

	movl	%eax, (%esp)
	movl	$0, 4(%esp)
	movl	%ebx, 8(%esp)
	call	merge_sort
#	movl	$msg_test,(%esp)
#	movl	$1,4(%esp)
#	call	printf

	movl	%ebp, %esp
	popl	%ebp
	ret

	.globl	merge_sort
	.type	merge_sort, @function
merge_sort:
	pushl	%ebp
	movl	%esp, %ebp
	subl	$16, %esp

#	movl	$msg_test,(%esp)
#	movl	$2,4(%esp)
#	call	printf	

	movl	12(%ebp), %ecx
	movl	16(%ebp), %edx
	subl	%ecx, %edx
	movl	%edx, %eax
	movl	$2, %ebx
	xorl	%edx, %edx
	divl	%ebx
	addl	%ecx, %eax
	movl	%eax, -4(%ebp)
	
	movl	12(%ebp), %ecx
	movl	16(%ebp), %edx
	cmpl	%edx, %ecx
	jl	continue_sort
	movl	%ebp, %esp
	popl	%ebp
	ret
continue_sort:
#	movl	8(%ebp),%eax
#	movl	%eax, (%esp)
#	movl	$3, 4(%esp)
#	call	output	

	movl	8(%ebp), %eax
	movl	12(%ebp), %ebx
	movl	-4(%ebp), %ecx
	movl	%eax, (%esp)
	movl	%ebx, 4(%esp)
	movl	%ecx, 8(%esp)
	call	merge_sort
	movl	8(%ebp), %eax
	movl	-4(%ebp), %ebx
	movl	16(%ebp), %ecx
	addl	$1, %ebx
	movl	%eax, (%esp)
	movl	%ebx, 4(%esp)
	movl	%ecx, 8(%esp)
	call	merge_sort
	
#	movl	8(%ebp), %eax
#	movl	%eax, (%esp)
#	movl	$3, 4(%esp)
#	call	output

#	movl	$msg_test, (%esp)
#	movl	$22, 4(%esp)
#	call	printf

	movl	8(%ebp), %eax
	movl	12(%ebp), %ebx
	movl	-4(%ebp), %ecx
	movl	16(%ebp), %edx
	movl	%eax, (%esp)
	movl	%ebx, 4(%esp)
	movl	%ecx, 8(%esp)
	movl	%edx, 12(%esp)
	call	merge

	movl	%ebp, %esp
	popl	%ebp
	ret

	.globl	merge
	.type	merge, @function
merge:
	pushl	%ebp
	movl	%esp, %ebp
	subl	$48, %esp

#	movl	$msg_test, (%esp)
#	movl	$3,4(%esp)
#	call	printf
	movl	$0, -4(%ebp)  # i
	movl	$0, -16(%ebp)  # from_a1
	movl	$0, -20(%ebp)  # from_a2
	movl	16(%ebp), %ebx
	movl	12(%ebp), %ecx
	subl	%ecx, %ebx
	addl	$1, %ebx
	movl	%ebx, -24(%ebp) # n1

	movl	%ebx, (%esp)
	movl	$4, 4(%esp)
	call	xcalloc
	movl	%eax, -28(%ebp) # *a1

	movl	20(%ebp), %eax
	movl	16(%ebp), %ebx
	subl	%ebx, %eax
	movl	%eax, -32(%ebp) # n2

	movl	%eax,(%esp)
	movl	$4, 4(%esp)
	call	xcalloc
	movl	%eax, -36(%ebp) # *a2

	jmp	for1_cond

for1:
	movl	12(%ebp), %eax
	movl	-4(%ebp), %ecx
	addl	%eax, %ecx
	movl	8(%ebp), %ebx
	movl	(%ebx,%ecx,4), %eax # a[p+i]

	movl	-28(%ebp), %ebx
	movl	-4(%ebp), %ecx
	movl	%eax, (%ebx,%ecx,4) # a1[i]

	addl	$1, -4(%ebp)
for1_cond:
	movl	-4(%ebp), %ecx
	movl	-24(%ebp), %edx
	cmpl	%edx, %ecx
	jl	for1

	movl	$0, -4(%ebp)
	jmp	for2_cond

for2:
	movl	16(%ebp), %eax
	movl	-4(%ebp), %ecx
	addl	%eax, %ecx
	addl	$1, %ecx
	movl	8(%ebp), %ebx
	movl	(%ebx,%ecx,4), %eax # a[q+i+1]

	movl	-36(%ebp), %ebx
	movl	-4(%ebp), %ecx
	movl	%eax, (%ebx,%ecx,4) # a2[i]

	addl	$1, -4(%ebp)
for2_cond:
	movl	-4(%ebp), %ecx
	movl	-32(%ebp), %edx
	cmpl	%edx, %ecx
	jl	for2

	movl	12(%ebp), %eax
	movl	%eax, -12(%ebp)
	movl	$0, -4(%ebp)
	movl	$0, -8(%ebp)

	jmp	while_cond		#while loop start

while:
	movl	-4(%ebp), %eax
	cmpl	-24(%ebp), %eax
	jne	continue1
	movl	$1, -16(%ebp)
	jmp	while_out_kishor
continue1:
#	movl	$msg_test,(%esp)
#	movl	$4,4(%esp)
#	call	printf

	movl	-8(%ebp), %eax
	cmpl 	-32(%ebp), %eax
	jne	continue2
	movl	$1, -20(%ebp)
	jmp	while_out_kishor
continue2:
#	movl	$msg_test,(%esp)
#	movl	$5,4(%esp)
#	call	printf

	movl	-4(%ebp), %ecx
	movl	-28(%ebp), %ebx
	movl	(%ebx,%ecx,4), %eax
	movl	-8(%ebp), %ecx
	movl	-36(%ebp), %ebx
	movl	(%ebx,%ecx,4), %edx
	cmpl	%edx, %eax
	jle	if
	movl	8(%ebp), %eax
	movl	-12(%ebp), %ecx
	movl	%edx, (%eax,%ecx,4)
	addl	$1, -8(%ebp)
	jmp	end_cond
if:
#	movl	$msg_test,(%esp)
#	movl	$6,4(%esp)
#	call	printf

	movl	-4(%ebp), %ecx
	movl	-28(%ebp), %ebx
	movl	(%ebx,%ecx,4), %edx
	movl	8(%ebp), %eax
	movl	-12(%ebp), %ecx
	movl	%edx, (%eax,%ecx,4)
	addl	$1, -4(%ebp)
end_cond:
	addl	$1, -12(%ebp)
	movl	-12(%ebp),%ecx

#	movl	$msg_test,(%esp)
#	movl	%ecx,4(%esp)
#	call	printf
while_cond:
	jmp	while
while_out_kishor:

#	jmp	chk_arr_exhaust

#chk_arr_exhaust:
	movl	-16(%ebp), %eax
	cmpl	$1, %eax
	je	a1_exhaust
	jmp	chk_arr
while_a1:
	movl	-36(%ebp), %edx
	movl	-8(%ebp), %ecx
	movl	(%edx,%ecx,4), %eax
	movl	8(%ebp), %ebx
	movl	-12(%ebp), %ecx
	movl	%eax, (%ebx,%ecx,4)
	addl	$1, -12(%ebp)
	addl	$1, -8(%ebp)
a1_exhaust:
	movl	-32(%ebp), %eax
	movl	-8(%ebp), %ebx
	cmpl	%eax, %ebx
	jl	while_a1	
chk_arr:
	movl	-20(%ebp), %eax
	cmpl	$1, %eax
	je	a2_exhaust
	jmp	out
while_a2:
	movl	-28(%ebp), %edx
	movl	-4(%ebp), %ecx
	movl	(%edx,%ecx,4), %eax
	movl	8(%ebp), %ebx
	movl	-12(%ebp), %ecx
	movl	%eax, (%ebx,%ecx,4)
	addl	$1, -4(%ebp)
	addl	$1, -12(%ebp)
a2_exhaust:
	movl	-24(%ebp), %eax
	movl	-4(%ebp), %ecx
	cmpl	%eax, %ecx
	jl	while_a2

out:	
	movl	-28(%ebp), %eax
	movl	%eax, (%esp)
	call	x_free
	movl	-36(%ebp), %eax
	movl	%eax, (%esp)
	call	x_free
	
	movl	%ebp, %esp
	popl	%ebp
	ret

	.globl	x_free
	.type	x_free, @function
x_free:
	pushl	%ebp
	movl	%esp, %ebp
	subl	$16, %esp

	movl	8(%ebp), %ebx
	cmpl	$0, %ebx
	je	no_free
	movl	%ebx, (%esp)
#	call	assert
	call	free
	movl	$0, 8(%ebp)
no_free:	
	movl	%ebp, %esp
	popl	%ebp
	ret

	.globl	input
	.type	input, @function
input:
	pushl	%ebp
	movl	%esp, %ebp
	subl	$16, %esp

	movl	$0, (%esp)
	call	time
	movl	%eax, (%esp)
	call	srand

	movl	$0, -4(%ebp)
	jmp	for_input_cond
for_input:
	call	rand
	movl	-4(%ebp), %ecx
	movl	$1000000, %ebx
	xorl	%edx, %edx
	divl	%ebx
	movl	8(%ebp), %eax
	movl	%edx, (%eax,%ecx,4)
	
	addl	$1, -4(%ebp)
for_input_cond:
	movl	-4(%ebp), %eax
	movl	12(%ebp), %ecx
	cmpl	%ecx, %eax
	jl	for_input

	movl	%ebp, %esp
	popl	%ebp
	ret

	.globl	xcalloc
	.type	xcalloc, @function
xcalloc:
	pushl	%ebp
	movl	%esp, %ebp
	subl	$16, %esp

	movl	8(%ebp), %eax
	movl	%eax, (%esp)
	movl	$4, 4(%esp)
	call	calloc
	movl	%eax, -4(%ebp)
	cmpl	$0, %eax
	jne	return_val
	movl	$msg_calloc, (%esp)
	call	printf
	movl	$0, (%esp)
	
	movl	%ebp, %esp
	popl	%ebp
	ret
return_val:
	movl	%ebp, %esp
	popl	%ebp
	ret
