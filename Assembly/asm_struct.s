.section .data
	.globl	inA
	.type	inA,@object
	.size	inA,12
	.align	4
	inA:
	.int	10
	.ascii	"A"
	.zero	3
	.float	3.14


.section .data
	.globl	inB
	.type	inB,@object
	.size	inB,40
	.align	4
	inB:
	.int	1,2,3,4,5
	.long	0xd0e0f0
	.long	0xa0b0c0
	.value	0x12,0x22be,0x3f2a,0xff12
	.int	100
