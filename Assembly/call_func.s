.section .rodata
	msgp1:
	.string	"a=%d\nb=%d\nc=%d\n"

.section .text
	.globl	main
	.type	main,@function
	
main:
	pushl	%ebp
	movl	%esp,%ebp
	andl	$-16,%esp
	subl	$16,%esp

	movl	$10,%ebx
	movl	$10,-4(%ebp)	
	movl	$11,-8(%ebp)
	movl	$12,-12(%ebp)
	movl	$13,-16(%ebp)
	pushl	-12(%ebp)
	pushl	-8(%ebp)
	pushl	-4(%ebp)
	call	f
	addl	$12,%esp

	pushl	$0
	call	exit

	.globl	f
	.type	f,@function

f:
	pushl	%ebp	# == 0
	movl	%esp,%ebp

	movl	8(%ebp),%eax
	movl	12(%ebp),%edx
	movl	16(%ebp),%ecx
	pushl	%ecx
	pushl	%edx
	pushl	%eax
	pushl	$msgp1
	call	printf
	addl	$16,%esp

	movl	%ebp,%esp
	popl	%ebp
	ret	
	
	.globl	g
	.type	g,@function
g:
	pushl	%ebp
	movl	%esp,%ebp

	movl	12(%ebp),%eax
	movl	16(%ebp),%ebx
	addl	%eax, %ebx

	movl	%ebp,%esp
	popl	%ebp
	ret	

