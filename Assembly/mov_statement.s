.section .rodata
	msg_p1:
	.string	"u8 is = %hhd\nu16 is =%hx\nu32 is = %x\n"#u64 is = %lx\n"#u128 is = %llx\n"

.section .data
	.globl	b_num
	.type	b_num,@object
	.size	b_num,1
	.align	4
	b_num:
		.byte	100

	.globl	s_num
	.type	s_num,@object
	.size	s_num,2
	.align	4
	s_num:
		.value	0x1020

	.globl	i_num 
	.type	i_num,@object
	.size	i_num,4
	.align	4
	i_num:
		.int	0x20304050
	
	.globl	q_num
	.type	q_num,@object
	.size	q_num,8
	.align	4
	q_num:
		.long	0xa0b0c0d0
		.long	0x43302

	.globl	dq_num
	.type	dq_num,@object
	.size	dq_num,16
	.align	4
	dq_num:
		.long	0x40302010
		.long	0x80706050
		.long	0xc0b0a0a0
		.long	0xf0e0d0
		
# .section .data
#short s_num = 0x1020
#int i_num = 0x20304050
#long int q_num = 0x23043a0b0c0d0
#long long dq_num = 0xd0e0f0g0a0b0c05060708010203040 ==> 16 byte variable

#.sction .bss
#short u16 ==>2byte
#int u32 ==> 4byte
#long int u64 == > 8 byte
#long long u128 ==> 16 byte

.section .bss
	.comm	u8,1,4
	.comm	u16,2,4
	.comm	u32,4,4
	.comm	u64,8,4
	.comm	u128,16,16

.section .text
	.globl	main
	.type	main,@function
	
main:
	pushl	%ebp
	movl	%esp, %ebp
	subl	$16, %esp

# 8 bit
	movb	b_num, %al
	addb	$1, %al
	movb	%al, u8

# 16 bit	
	movw	s_num, %ax
	movw	%ax, u16

# 32 bit
	movl	i_num, %eax
	movl	%eax, u32

# 64 bit
	movq	q_num, %mm0
	movq	%mm0, u64

# 128 bit
#	movdqu	dq_num, %xmm0
#	movdqa	%xmm0, u128

#	pushl	%mm0h
#	pushl	%mm0l
	pushl	u32
	pushl	u16
	pushl	u8
#	pushdqa	u128
	pushl	$msg_p1
	call	printf
	addl	$16, %esp

	pushl	$0
	call	exit
