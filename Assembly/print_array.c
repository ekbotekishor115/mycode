#include <stdio.h>
#include <stdlib.h>

int array[5];
int i;

int main()
{
	for(i = 0; i < 5; i++)
	{
		array[i] = i * 10;
	}

	for(i = 0; i < 5; i++)
	{
		printf("array[%d] = %d\n",i,array[i]);
	}
	exit(0);
}
