#int A[2] = {0x12E492FA,0xFF975BAC};
.section .data
	.globl	A
	.type	A,@object
	.size	A,8
	.align	4
	A:
	.int	0x12E492FA,0xFF975BAC
	
#short int n1 = 0x1289,n2 = 0x5E98;
.section .data
	.globl	n1
	.type	n1,@object
	.size	n1,2
	.align	4
	n1:
	.value	0x1289

	.globl	n2
	.type	n2,@object
	.size	n2,2
	.align	4
	n2:
	.value	0x5E98

#char s[] = "Hello";
.section .data
	.globl	s
	.type	s,@object
	.size	s,5
	.align	4
	s:
	.string	"Hello"

#long long int p= 0x189EB2CBC4A0B0D0;
.section .data
	.globl	p
	.type	p,@object
	.size	p,8
	.align	4
	p:
	.long	0xC4A0B0D0
	.long	0x189EB2CB
	
#char s[2] = "Assembly";
.section .data
	.globl	s
	.type	s,@object
	.size	s,2
	.align	4
	s:
	.string	"Assembly"
#int final = 10;
.section .data
	.globl	final
	.type	final,@object
	.size	final,4
	.align	4
	final:
	.int	10
	

