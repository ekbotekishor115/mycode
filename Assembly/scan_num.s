.section .rodata
	msgp1:
	.string	"Enter an integer\n"
	msgp2:
	.string	"%d"
	msgp3:
	.string	"g_num: %d\n"

.section .data
	.globl	g_num
	.type	g_num,@object
	.size	g_num,4
	.align	4
	g_num:
		.int	10

.section .text
	.globl	main
	.type	main,@function

main:
	pushl	%esp
	movl	%esp,%ebp
	andl	$-16, %esp
	subl	$16,%esp
	
	pushl	$msgp1
	call	printf
	pushl	$g_num
	pushl	$msgp2
	call	scanf
	pushl	g_num
	pushl	$msgp3
	call	printf
	
	addl	$24,%esp
	pushl	$0
	call	exit
	
