.section .rodata
	msg_p1:
	.string	"array[%d] = %d\n"

.section .bss
	.comm	array,20,4
	.comm	i,4,4
	.comm	j,4,4

.section .text
	.globl	main
	.type	main,@function

main:
	pushl	%ebp
	movl	%esp, %ebp
	andl	$-16, %esp
	subl	$16, %ebx
#	movl	$array, %ebx

	movl	$0, i
	movl	$10, j
	jmp	mn_cond

mn_for:
	movl	i, %edx
#	movl	%edx, %ecx
	#multiply %edx with 10
#	sall	$3, %edx
	movl	j, %ecx
#	sall	$2, %ecx
	movl	%ecx,array(,%edx,4)
	addl	$1, i

	addl	$10, j
mn_cond:
	movl	i, %eax
	cmpl	$5, %eax
	jl	mn_for

	movl	$0, i
	movl	$0, j
	jmp	mn_cond2

mn_for2:
	movl	i, %edx
#	movl	%edx, %ecx
#	sall	$0x0A, %edx
#	sall	$2, %ecx
	movl	array(,%edx,4), %ecx
	
	pushl	%ecx
	pushl	%edx
	pushl	$msg_p1
	call	printf
	addl	$12, %esp
	
	addl	$1, i

mn_cond2:
	movl	i, %eax
	cmpl	$5, %eax
	jl	mn_for2

	pushl	$0
	call	exit
