.section .rodata
	msgp1:
	.string	"Hello,world\n"

.section .text
	.globl	main
	.type	main,@function

main:
	pushl	%esp
	movl	%esp, %ebp
	andl	$-16,%esp
	subl	$16,%esp

	pushl	$msgp1
	call	printf

	addl	$4,%esp
	pushl	$0
	call	exit

