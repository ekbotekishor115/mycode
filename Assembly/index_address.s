.section .rodata
	msgp1:
	.string	"inT.i_num = %d\ninT.c = %c\ninT.lng_num = %ld\n"
	
	msgp2:
	.string	"inT.arr[%d] = %d\n"
.section .data
	.globl	inT
	.type	inT,@object
	.size	inT,36
	.align	4
	inT:
	.int	100
	.ascii	"A"
	.zero	3
	.long	1000
	.int	10,20,30,40,50
	.int	0

.section .bss
	.comm	i,4,4

.section .text
	.globl	main
	.type	main,@function

main:
	pushl	%ebp
	movl	%esp,%ebp
	andl	$-16,%esp
	subl	$16,%ebx

	movl	$inT,%eax
	pushl	8(%eax)
	pushl	4(%eax)
	pushl	(%eax)
	pushl	$msgp1
	call	printf
	
	movl	$0, i
	jmp	mn_cond

mn_for:
	movl	i,%edx
	movl	$12,%ecx
	movl	inT(%ecx,%edx,4), %ebx
#	addl	$4,%ecx
	pushl	%ebx
	pushl	%edx
	pushl	$msgp2
	call	printf
	addl	$1,i

mn_cond:
	movl	i,%eax
	cmpl	$5,%eax
	jl	mn_for

	addl	$28,%esp
	pushl	$0
	call	exit
