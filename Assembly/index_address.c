#include <stdio.h>
#include <stdlib.h>

struct T
{
	int i_num;
	char c;
	long int lng_num;
	int A[5];
	int *p;
};

struct T inT = {100,'A',1000,{10,20,30,40,50},0};

int i;

int main()
{
	printf("inT.i_num = %d\ninT.c = %c\ninT.lng_num = %ld\n",inT.i_num,inT.c,inT.lng_num);
	for (i = 0; i < 5; i++)
	{
		printf("arr[%d] = %d\n",i,inT.A[i]);
	}
	exit(0);
}
