if (a < b || c > d)
{
	printf("Hello\n");
}
else
{
	printf("world\n");
}

if (a > c && d < z)
{
	printf("Hello\n");
}
else 
{
	printf("world\n");

}

//if - else for ||
	movl	a,%eax
	cmpl	b,%eax
	jl	if
	movl	c,%ebx
	cmpl	d,%ebx
	jng	else

if:
	pushl	$msgp1
	call	printf
	addl	$4,%esp
else:
	pushl	$msgp2
	call	printf
	addl	$4,%esp

// 2nd if -else for &&
	movl	a,%eax
	cmpl	c,%eax
	jng	else
	movl	d,%ebx
	cmpl	z,%ebx
	jnl	else
	pushl	$msgp1
	call	printf
	addl	$4,%esp
	jmp	exit
else:
	pushl	$msgp2
	call	printf
	addl	$4,%esp
	jmp	exit

exit:
	pushl	$0
	call	exit
