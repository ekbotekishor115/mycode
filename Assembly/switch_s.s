.section .rodata
	msgp1:
	.string	"Choice is 1\n"
	msgp2:
	.string	"Choice is 2\n"
	msgp3:
	.string	"Choice is 3\n"
	msgp4:
	.string	"Choice is other then 1, 2 and 3\n"
	enter_num:
	.string	"Enter num:\n"
	scan_num:
	.string	"%d"
.section .data
	.globl	x
	.type	x,@object
	.size	x,4
	.align	4
	x:
	.int	0

.section .text
	.globl	main
	.type	main,@function

main:
	pushl	%esp
	movl	%esp,%ebp
	andl	$-16,%esp
	
	pushl	$enter_num
	call	printf
	pushl	$x
	push	$scan_num
	call	scanf
	addl	$12,%esp
	movl	x,%eax
	jmp	switch
switch:
	cmpl	$1,%eax
	je	case_1
	cmpl	$2,%eax
	je	case_2
	cmpl	$3,%eax
	je	case_3
	jne	default

case_1:
	pushl	$msgp1
	call	printf
	addl	$4,%esp
	jmp	call_exit
case_2:
	pushl	$msgp2
	call	printf
	addl	$4,%esp
	jmp	call_exit
case_3:
	pushl	$msgp3
	call	printf
	addl	$4,%esp
	jmp	call_exit
default:
	pushl	$msgp4
	call	printf
	addl	$4,%esp
	jmp	call_exit

call_exit:
	pushl	$0
	call	exit



	
