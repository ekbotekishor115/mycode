.section .rodata
	msgp1:
	.string	"Enter a character:\n"
	scan1:
	.string	"%c"
	msgp2:
	.string	"Enter a short integer:\n"
	scan2:
	.string	"%hd"
	msgp3:
	.string	"Enter an integer\n"
	scan3:
	.string	"%d"
	msgp4:
	.string	"t.c=%c t.si=%hd t.i=%d\n"

.section .bss
	.comm	t,12,4
	.comm	c,1,4
	.comm	si,2,4
	.comm	i,4,4

.section .text
	.globl	main
	.type	main,@function

main:
	pushl	%ebp
	movl	%esp, %ebp
	andl	$-16, %esp
	subl	$16, %esp

	movl	$t,%edx

	pushl	$msgp1
	call	printf
	addl	$4, %esp
	pushl	(%ebx)
#	pushl	$c	
	pushl	$scan1
	call	scanf
	addl 	$8, %esp
#	movb	c,%al
	movl	%ebx, (%edx)
#	movb	%ah, (%edx)

	pushl	$msgp2
	call	printf
	addl	$4, %esp
	pushl	(%ebx)
#	pushl	$si
	pushl	$scan2
	call	scanf
	addl	$8, %esp
#	movw	si,%bx
	movl	%ebx, 4(%edx)
	
	pushl	$msgp3
	call	printf
	addl	$4, %esp
	pushl	(%ecx)
#	pushl	$i
	pushl	$scan3
	call	scanf
	addl	$8, %esp
#	movl	i, %ecx
	movl	%ecx, 8(%edx)

	pushl	8(%edx)
	pushl	4(%edx)
	pushl	(%edx)
	pushl	$msgp4
	call	printf

	addl	$16,%esp
	
	pushl	$0
	call	exit
