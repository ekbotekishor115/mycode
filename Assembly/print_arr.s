.section .rodata
	msg_p1:
	.string	"arr[%d] = %d\n"

.section .bss
	.comm	arr,20,4
	.comm	i,4,4

.section .text
	.globl	main
	.type	main,@function
main:
	pushl	%ebp
	movl 	%esp, %ebp
	andl	$-16, %esp
	subl	$16, %esp
	movl	$arr, %ebx
#for loop
	movl	$0, i
	jmp	mn_cond
	
mn_for:
	movl	i, %edx
	movl	%edx, %ecx
	sall	$2, %ecx
	addl	%ebx, %ecx
	movl	%edx, (%ecx)
	addl	$1, i

mn_cond:
	movl	i, %eax
	cmpl	$5, %eax
	jl	mn_for

#2nd for loop to print the array
	movl	$0, i
	jmp	mn_cond2

mn_for2:
	movl	i, %edx
	movl	%edx, %ecx
	sall	$2, %ecx
	addl	%ebx, %ecx
	movl	%edx, (%ecx)
	
	pushl	%edx
	pushl	(%ecx)
	pushl	$msg_p1
	call	printf
	addl	$1, i
		
mn_cond2:
	movl	i, %eax
	cmpl	$5, %eax
	jl	mn_for2

	addl	$12, %esp
	pushl	$0
	call	exit
