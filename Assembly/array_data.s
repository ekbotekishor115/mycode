.section .rodata
	msgp1:
	.string	"arr[0] = %d\narr[1] = %d\narr[2] = %d\narr[3] = %d\narr[4] = %d\n"
	
.section .bss
	.comm	arr,20,4

.section .text
	.globl	main
	.type	main, @function
	
	main:
		pushl	%ebp
		movl	%esp, %ebp
		subl	$16, %esp
		
		movl	$arr, %edx
		movl	$1, (%edx)

		movl	$2, 4(%edx)
		
		movl	$3, 8(%edx)

		movl	$4, 12(%edx)

		movl	$5, 16(%edx)
		
		pushl	16(%edx)
		pushl	12(%edx)
		pushl	8(%edx)
		pushl	4(%edx)
		pushl	(%edx)

		pushl	$msgp1
		call	printf

		addl	$16,%esp
		pushl	$0
		call	exit

