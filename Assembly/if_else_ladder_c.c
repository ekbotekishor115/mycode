#include <stdio.h>
#include <stdlib.h>

int day;

int main()
{
	printf("Enter day Number:	");
	scanf("%d", &day);
	if (day == 1)
	{
		printf("SUNDAY.\n");
	}
	else if(day == 2)
	{
		printf("MONDAY.\n");
	}
	else if(day == 3)
	{
		printf("TUESDAY.\n");
	}
	else if (day == 4)
	{
		printf("WEDNESDAY.\n");
	}
	else if(day == 5)
	{
		printf("THURSDAY.\n");
	}
	else if (day == 6)
	{
		printf("FRIDAY.\n");
	}
	else if (day == 7)
	{
		printf("SATURDAY.\n");
	}
	else
	{
		printf("INVALID DAY.\n");
	}
	exit(0);


}
