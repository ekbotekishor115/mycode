.section .rodata
	msg_p1:
	.string	"Usage Error"
	msg_p2:
	.string	"invalid command line argument"
	msg_p3:
	.string	"sort success"
	msg_p4:
	.string	"sort failure"
	msg_o1:
	.string	"a[%d]:%d\n"
	msg_x1:
	.string	"fatal memory"

.section .text
	.globl	main
	.type	main,@function

main:
	pushl	%ebp
	movl	%esp, %ebp
	andl	$-16, %esp
	subl	$16, %esp
	
	movl	$0, -4(%ebp)
	movl	$0, -8(%ebp)
	
	movl	4(%ebp), %eax
	cmpl	$2, %eax
	je	continue
	movl	$msg_p1, (%esp)
	call	puts
	movl	$-1, (%esp)
	call	exit

continue:
	movl	12(%ebp), %eax
	movl	%eax, (%esp)
	call	atoi
	movl	%eax, -8(%ebp)
	cmpl	$0, %eax
	jg	continue2
	movl	$msg_p2, (%esp)
	call	puts
	movl	$-1, (%esp)
	call	exit

continue2:
	movl	-8(%ebp), %eax
	movl	%eax, (%esp)
	movl	$4, 4(%esp)
	call	xcalloc
	movl	%eax, -4(%ebp)
	movl	-8(%ebp),%ebx
	movl	%eax, (%esp)
	movl	%ebx, 4(%esp)
	call	input
	call	output
	movl	-4(%ebp), %eax
	movl	-8(%ebp), %ebx
	movl	%eax, (%esp)
	movl	%ebx, 4(%esp)
	call	sort
	movl	-4(%ebp), %eax
	movl	-8(%ebp), %ebx
	movl	%eax, (%esp)
	movl	%ebx, 4(%esp)
	call	test_sort
	cmpl	$1, %eax
	je	if
	movl	$msg_p4, (%esp)
	call	puts
	movl	$-1, (%esp)
	call	exit
	
if:
	movl	$msg_p3, (%esp)
	call	puts

	
#	movl	-4(%ebp), %eax
#	movl	-8(%ebp), %ebx
#	movl	%eax, (%esp)
#	movl	%ebx, 4(%esp)
#	call	output
	movl	-4(%ebp),%ebx
	movl	%ebx, (%esp)
	call	free
	movl	$0, -4(%ebp)
	movl	$0, (%esp)
	call	exit

	.globl	input
	.type	input, @function
	
input:
	pushl	%ebp
	movl	%esp, %ebp
	subl	$16, %esp

	movl	$0, (%esp)
	call	time
	movl	%eax, (%esp)
	call	srand
	movl	$0, -4(%ebp)
	jmp	for_cond

for:
	movl	-4(%ebp), %ebx
	movl	8(%ebp), %edx
	leal	(%edx,%ebx,4), %ecx
	call	rand
	movl	%eax, (%ecx)
	addl	$1, -4(%ebp)

for_cond:
	movl	-4(%ebp), %ebx
	movl	12(%ebp), %ecx
	cmpl	%ecx, %ebx
	jl	for

	movl	%ebp, %esp
	popl	%ebp
	ret

	.globl	output
	.type	output, @function

output:
	pushl	%ebp
	movl	%esp, %ebp
	subl	$16, %esp
	
	movl	$0, -4(%ebp)
	jmp	for1_cond

for1:
	movl	-4(%ebp), %ebx
	movl	8(%ebp), %ecx
	movl	(%ecx,%ebx,4), %edx
	movl	$msg_o1, (%esp)
	movl	%ebx, 4(%esp)
	movl	%edx,8(%esp)
	call	printf
	addl	$1, -4(%ebp)

for1_cond:
	movl	-4(%ebp), %ebx
	movl	12(%ebp), %ecx
	cmpl	%ecx, %ebx
	jl	for1

	movl	%ebp, %esp
	popl	%ebp
	ret

	.globl	sort
	.type	sort, @function
sort:
	pushl	%ebp
	movl	%esp, %ebp
	subl	$16, %esp

	movl	$1, -8(%ebp)
	jmp	for2_cond

for2:
	movl	8(%ebp), %ecx
	movl	-8(%ebp), %eax
	movl	(%ecx,%eax,4), %edx
	movl	%edx, -12(%ebp)
	subl	$1, %eax
	movl	%eax, -4(%ebp)
	jmp	while_cond	

while:
	addl	$1, %eax
	movl	%edx, (%ebx,%eax,4)
	subl	$1, %eax
	addl	$1, %eax
	movl	%ecx, (%ebx,%eax,4)
	addl	$1, -8(%ebp)
	jmp	for2_cond 
while_cond:
	movl	-4(%ebp),%eax
	cmpl	$0, %eax
	jl	while_out
	movl	8(%ebp), %ebx
	movl	-8(%ebp), %eax
	movl	(%ebx,%eax,4), %edx
	movl	-12(%ebp), %ecx
	cmpl	%ecx, %edx
	jg	while
while_out:	
	addl	$1, -8(%ebp)

for2_cond:
	movl	-8(%ebp), %eax
	movl	12(%ebp), %ebx
	cmpl	%ebx, %eax
	jl	for2

	movl	%ebp, %esp
	popl	%ebp
	ret

	.globl	test_sort
	.type	test_sort,@function
test_sort:
	pushl	%ebp
	movl	%esp, %ebp
	subl	$16, %esp

	movl	$0, -4(%ebp)
	movl	-4(%ebp), %eax
	jmp	for3_cond
ret_0:
	ret	$0
	jmp	for3_cond
	
for3:
	movl	8(%ebp), %ecx
	movl	-4(%ebp), %edx
	movl	(%ecx,%edx,4), %eax
	addl	$1, %edx
	movl	(%ecx,%edx,4), %ebx
	cmpl	%ebx, %eax
	jg	if

for3_cond:
	movl	-4(%ebp), %eax
	movl	8(%ebp), %ebx
	subl	$1, %ebx
	cmpl	%ebx, %eax
	jl	for3

	movl	%ebp, %esp
	popl	%esp
	ret	

	.globl	xcalloc
	.type	xcalloc,@function
xcalloc:
	pushl	%ebp
	movl	%esp, %ebp
	subl	$16, %esp

	movl	$0, -4(%ebp)	
	movl	8(%ebp), %eax
	movl	%eax, (%esp)
	movl	$4, 4(%esp)
	call	calloc
	movl	%eax, -4(%ebp)
	movl	-4(%ebp), %ebx
	cmpl	$0, %eax
	jne	continue3
	movl	$msg_x1, (%esp)
	call	puts
	movl	$0, (%esp)
	call	exit

continue3:
	movl	%ebp, %esp
	popl	%ebp
	ret	
