.section .rodata
	msgp1:
	.string	"Enter A[%d]:\t"
	msgp2:
	.string	"%d"
	msgp3:
	.string	"A[%d]:%d\n"

.section .bss
	.comm	A,20,4
	.comm	i,4,4

.section .text
	.globl	main
	.type	main,@function
	
main:
	pushl	%esp
	movl	%esp,%ebp
	andl	$-16,%esp
	subl	$16,%esp
	movl	$0,i
	jmp	for_cond

for1:
	pushl	i
	pushl	$msgp1
	call	printf
	addl	$8,%esp
	movl	i,%eax	
	leal	A(,%eax,4),%ecx
	pushl	%ecx
	pushl	$msgp2
	call	scanf
	
	addl	$8,%esp
	addl	$1,i
#	jmp	for_cond
	
for_cond:
	movl	i,%eax
	cmpl	$5,%eax
	jl	for1
	
	movl	$0,i
	jmp	for_cond2

for2:
	movl	i,%eax
	movl	A(,%eax,4),%ecx
	pushl	%ecx
	pushl	%eax
	pushl	$msgp3
	call	printf
	
	addl	$12,%esp
	addl	$1,i
#	jmp	for_cond2

for_cond2:
	movl	i,%eax
	cmpl	$5,%eax
	jl	for2
		
	pushl	$0
	call	exit	
