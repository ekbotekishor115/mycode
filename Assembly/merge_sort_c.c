#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <assert.h>

#define MAX_NUMBER 1000000
#define TRUE	1
#define FALSE	0

typedef int bool;

void *xcalloc(int , size_t);
void x_free(void *);
void input(int *, int);
void output(int *, int);

void sort(int a[], int);
void merge(int a[], int p, int q, int r);
void merge_sort(int a[], int p, int r);

void test_sort(int a[], int n);

int main(int argc, char **argv)
{
	int n;
	int *arr;
	if (argc != 2)
	{
		printf("%s<total count of numbers to be sort>\n",argv[0]);
		exit(EXIT_FAILURE);
	}

	n = atoi(argv[1]);
	arr = (int *)xcalloc(n,sizeof(int));
	input(arr,n);
	sort(arr,n);
	output(arr,n);
	test_sort(arr,n);
	x_free(arr);

	exit(EXIT_SUCCESS);
}

void test_sort(int a[], int n)
{
	int i;
	for (i = 0; i < (n - 1); i++)
	{
		if (a[i] > a[i+1])
		{
			printf("merge sort failed\n");
			return;
		}
	}
	printf("merge sort success\n");
}

void output(int *arr, int n)
{
	for (int i = 0; i < n; i++)
	{
		printf("arr[%d] = %d\n",i,arr[i]);
	}
}

void sort(int a[], int n)
{
	merge_sort(a,0,n - 1);
}

void merge_sort(int a[], int p, int r)
{
	int q = (r - p)/2 + p;
	if (p < r)
	{
		merge_sort(a,p,q);
		merge_sort(a,q+1,r);
		merge(a,p,q,r);
	}
}

void merge(int a[], int p, int q, int r)
{
	int i,j,k;
	bool from_a1 = FALSE;
	bool from_a2 = FALSE;
	int n1 = q - p + 1;
	int *a1 = (int *)xcalloc(n1, sizeof(int));

	int n2 = r - q;
	int *a2 = (int *)xcalloc(n2, sizeof(int));

	//copy left array in a1[]
	for (i = 0; i< n1; i++)
	{
		a1[i] = a[p +i];
	}

	//copy right array in a2[]
	for (i = 0; i< n2; i++)
	{
		a2[i] = a[q + i + 1];
	}

	k = p; //index into original array, a[], (p <= k < n)
	i = 0; //index into left array, a1[], (0 <= i < n1)
	j = 0; //index into right array, a2[], (0 <= j < n2)

	//loop through untill one of the array exhausts.

	while (TRUE)
	{
		if(i == n1)
		{
			from_a1 = TRUE;
			break;
		}
		if (j == n2)
		{
			from_a2 = TRUE;
			break;
		}

		if (a1[i] <= a2[j])
		{
			a[k] = a1[i];
			i = i +1;
		}
		else
		{
			a[k] = a2[j];
			j = j + 1;
		}
		k = k + 1;
	}

	if (from_a1 = TRUE)
	{
		//a1[] exhausted , copy remaining already sorted item from a2[]
		while (j < n2)
		{
			a[k] = a2[j];
			k = k + 1;
			j = j + 1;
		}
	}

	if (from_a2 == TRUE)
	{
		//a2[] exhausted, copy remaining already sorted item from a1[]
		while(i < n1)
		{
			a[k] = a1[i];
			i = i + 1;
			k = k + 1;
		}
	}

	x_free(a1);
	x_free(a2);
}

void x_free(void *ptr)
{
	assert(ptr);
	free(ptr);
	ptr = NULL;
}

void input(int *arr, int num)
{
	srand(time(0));
	for (int i = 0; i < num; i++)
	{
		arr[i] = rand() % MAX_NUMBER;
	}
}

void *xcalloc(int n, size_t size)
{
	void *temp = calloc(n, size);
	if (!temp)
	{
		printf("memory can not be allocated\n");
		return NULL;
	}
	return temp;
}
