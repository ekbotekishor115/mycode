.section .rodata
	msgp1:
	.string	"Hello\n"
	msgp2:
	.string	"world\n"
	
.section .bss
	.comm	a,4,4
	.comm	b,4,4
	.comm	c,4,4
	.comm	d,4,4
	.comm	z,4,4

.section .text
	.globl	main
	.type	main,@function

main:	
	pushl	%esp
	movl	%esp,%ebp
	andl	$16,%esp

	movl	$1,a
	movl	$2,b
	movl	$3,c
	movl	$4,d
	movl	$5,z

	movl	a,%eax
	cmpl	b,%eax
	jnge	print_1
	jng	chk_cond
	cmpl	c,%eax
	jnle	chk_cond_and
	jle	print_4

chk_cond:
	movl	c,%ebx
	cmpl	d,%ebx
	jnle	print_2

chk_cond_and:
	movl	d,%ecx
	cmpl	z,%ecx
	jnge	print_3

print_1:
	pushl	$msgp1
	call	printf
	addl	$4,%esp
	
print_2:
	pushl	$msgp2
	call	printf
	addl	$4,%esp

print_3:
	pushl	$msgp1
	call	printf
	addl	$4,%esp

print_4:
	pushl	$msgp2
	call	printf
	addl	$4,%esp

	pushl	$0
	call	exit
