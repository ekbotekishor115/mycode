.section .rodata
	msgp1:
	.string	"Enter day number:	"
	msg_scan:
	.string	"%d"
	msg_day1:
	.string	"SUNDAY.\n"
	msg_day2:
	.string	"MONDAY.\n"
	msg_day3:
	.string	"TUESDAY.\n"
	msg_day4:
	.string	"WEDNESDAY.\n"	
	msg_day5:
	.string	"THURSDAY.\n"
	msg_day6:
	.string	"FRIDAY.\n"
	msg_day7:
	.string	"SATURDAY.\n"
	msg_inval:
	.string	"INVALID DATE.\n"

.section .bss
	.comm	day,4,4

.section .text
	.globl	main
	.type	main,@function

main:
	pushl	%ebp
	movl	%esp, %ebp
	andl	$-16, %esp

	pushl	$msgp1
	call	printf
	pushl	$day
	pushl	$msg_scan
	call	scanf
	addl	$12, %esp
	jmp	mn_cond

mn_cond:
	movl	day, %eax
	cmpl	$1, %eax
	je	if_one
	cmpl	$2, %eax
	je	if_two
	cmpl	$3, %eax
	je	if_three
	cmpl	$4, %eax
	je	if_four
	cmpl	$5, %eax
	je	if_five
	cmpl	$6, %eax
	je	if_six
	cmpl	$7, %eax
	je	if_seven
	jne	else
#	jz	else
if_one:
	pushl	$msg_day1
	call	printf
	addl	$4, %esp
	pushl	$0
	call	exit
if_two:
	pushl	$msg_day2
	call	printf
	addl	$4, %esp
	pushl	$0
	call	exit
if_three:
	pushl	$msg_day3
	call	printf
	addl	$4, %esp
	pushl	$0
	call	exit
if_four:
	pushl	$msg_day4
	call	printf
	addl	$4, %esp
	pushl	$0
	call	exit
if_five:
	pushl	$msg_day5
	call	printf
	addl	$4, %esp
	pushl	$0
	call	exit
if_six:
	pushl	$msg_day6
	call	printf
	addl	$4, %esp
	pushl	$0
	call	exit
if_seven:
	pushl	$msg_day7
	call	printf
	addl	$4, %esp
	pushl	$0
	call	exit
else:
	pushl	$msg_inval
	call	printf
	addl	$4, %esp

	pushl	$0
	call	exit
