	.file	"insertion_sort_c.c"
	.section	.rodata
.LC0:
	.string	"Usage Error"
.LC1:
	.string	"Invalid command line argument"
.LC2:
	.string	"sort success"
.LC3:
	.string	"sort failure"
	.text
	.globl	main
	.type	main, @function
main:
.LFB2:
	.cfi_startproc
	leal	4(%esp), %ecx
	.cfi_def_cfa 1, 0
	andl	$-16, %esp
	pushl	-4(%ecx)
	pushl	%ebp
	.cfi_escape 0x10,0x5,0x2,0x75,0
	movl	%esp, %ebp
	pushl	%ecx
	.cfi_escape 0xf,0x3,0x75,0x7c,0x6
	subl	$20, %esp
	movl	%ecx, %eax
	movl	$0, -16(%ebp)
	movl	$0, -12(%ebp)
	cmpl	$2, (%eax)
	je	.L2
	subl	$12, %esp
	pushl	$.LC0
	call	puts
	addl	$16, %esp
	subl	$12, %esp
	pushl	$1
	call	exit
.L2:
	movl	4(%eax), %eax
	addl	$4, %eax
	movl	(%eax), %eax
	subl	$12, %esp
	pushl	%eax
	call	atoi
	addl	$16, %esp
	movl	%eax, -12(%ebp)
	cmpl	$0, -12(%ebp)
	jne	.L3
	subl	$12, %esp
	pushl	$.LC1
	call	puts
	addl	$16, %esp
	subl	$12, %esp
	pushl	$1
	call	exit
.L3:
	subl	$8, %esp
	pushl	$4
	pushl	-12(%ebp)
	call	xcalloc
	addl	$16, %esp
	movl	%eax, -16(%ebp)
	subl	$8, %esp
	pushl	-12(%ebp)
	pushl	-16(%ebp)
	call	input
	addl	$16, %esp
	subl	$8, %esp
	pushl	-12(%ebp)
	pushl	-16(%ebp)
	call	sort
	addl	$16, %esp
	subl	$8, %esp
	pushl	-12(%ebp)
	pushl	-16(%ebp)
	call	test_sort
	addl	$16, %esp
	cmpl	$1, %eax
	jne	.L4
	subl	$12, %esp
	pushl	$.LC2
	call	puts
	addl	$16, %esp
	subl	$8, %esp
	pushl	-12(%ebp)
	pushl	-16(%ebp)
	call	output
	addl	$16, %esp
	subl	$12, %esp
	pushl	-16(%ebp)
	call	free
	addl	$16, %esp
	movl	$0, -16(%ebp)
	subl	$12, %esp
	pushl	$0
	call	exit
.L4:
	subl	$12, %esp
	pushl	$.LC3
	call	puts
	addl	$16, %esp
	subl	$12, %esp
	pushl	$1
	call	exit
	.cfi_endproc
.LFE2:
	.size	main, .-main
	.globl	input
	.type	input, @function
input:
.LFB3:
	.cfi_startproc
	pushl	%ebp
	.cfi_def_cfa_offset 8
	.cfi_offset 5, -8
	movl	%esp, %ebp
	.cfi_def_cfa_register 5
	pushl	%ebx
	subl	$20, %esp
	.cfi_offset 3, -12
	subl	$12, %esp
	pushl	$0
	call	time
	addl	$16, %esp
	subl	$12, %esp
	pushl	%eax
	call	srand
	addl	$16, %esp
	movl	$0, -12(%ebp)
	jmp	.L7
.L8:
	movl	-12(%ebp), %eax
	leal	0(,%eax,4), %edx
	movl	8(%ebp), %eax
	leal	(%edx,%eax), %ebx
	call	rand
	movl	%eax, (%ebx)
	addl	$1, -12(%ebp)
.L7:
	movl	-12(%ebp), %eax
	cmpl	12(%ebp), %eax
	jb	.L8
	nop
	movl	-4(%ebp), %ebx
	leave
	.cfi_restore 5
	.cfi_restore 3
	.cfi_def_cfa 4, 4
	ret
	.cfi_endproc
.LFE3:
	.size	input, .-input
	.section	.rodata
.LC4:
	.string	"a[%d]:%d\n"
	.text
	.globl	output
	.type	output, @function
output:
.LFB4:
	.cfi_startproc
	pushl	%ebp
	.cfi_def_cfa_offset 8
	.cfi_offset 5, -8
	movl	%esp, %ebp
	.cfi_def_cfa_register 5
	subl	$24, %esp
	movl	$0, -12(%ebp)
	jmp	.L11
.L12:
	movl	-12(%ebp), %eax
	leal	0(,%eax,4), %edx
	movl	8(%ebp), %eax
	addl	%edx, %eax
	movl	(%eax), %eax
	subl	$4, %esp
	pushl	%eax
	pushl	-12(%ebp)
	pushl	$.LC4
	call	printf
	addl	$16, %esp
	addl	$1, -12(%ebp)
.L11:
	movl	-12(%ebp), %eax
	cmpl	12(%ebp), %eax
	jb	.L12
	nop
	leave
	.cfi_restore 5
	.cfi_def_cfa 4, 4
	ret
	.cfi_endproc
.LFE4:
	.size	output, .-output
	.globl	sort
	.type	sort, @function
sort:
.LFB5:
	.cfi_startproc
	pushl	%ebp
	.cfi_def_cfa_offset 8
	.cfi_offset 5, -8
	movl	%esp, %ebp
	.cfi_def_cfa_register 5
	subl	$16, %esp
	movl	$1, -8(%ebp)
	jmp	.L15
.L19:
	movl	-8(%ebp), %eax
	leal	0(,%eax,4), %edx
	movl	8(%ebp), %eax
	addl	%edx, %eax
	movl	(%eax), %eax
	movl	%eax, -4(%ebp)
	movl	-8(%ebp), %eax
	subl	$1, %eax
	movl	%eax, -12(%ebp)
	jmp	.L16
.L18:
	movl	-12(%ebp), %eax
	addl	$1, %eax
	leal	0(,%eax,4), %edx
	movl	8(%ebp), %eax
	addl	%eax, %edx
	movl	-12(%ebp), %eax
	leal	0(,%eax,4), %ecx
	movl	8(%ebp), %eax
	addl	%ecx, %eax
	movl	(%eax), %eax
	movl	%eax, (%edx)
	subl	$1, -12(%ebp)
	movl	-12(%ebp), %eax
	addl	$1, %eax
	leal	0(,%eax,4), %edx
	movl	8(%ebp), %eax
	addl	%eax, %edx
	movl	-4(%ebp), %eax
	movl	%eax, (%edx)
.L16:
	cmpl	$0, -12(%ebp)
	js	.L17
	movl	-8(%ebp), %eax
	leal	0(,%eax,4), %edx
	movl	8(%ebp), %eax
	addl	%edx, %eax
	movl	(%eax), %eax
	cmpl	-4(%ebp), %eax
	jg	.L18
.L17:
	addl	$1, -8(%ebp)
.L15:
	movl	-8(%ebp), %eax
	cmpl	12(%ebp), %eax
	jb	.L19
	nop
	leave
	.cfi_restore 5
	.cfi_def_cfa 4, 4
	ret
	.cfi_endproc
.LFE5:
	.size	sort, .-sort
	.globl	test_sort
	.type	test_sort, @function
test_sort:
.LFB6:
	.cfi_startproc
	pushl	%ebp
	.cfi_def_cfa_offset 8
	.cfi_offset 5, -8
	movl	%esp, %ebp
	.cfi_def_cfa_register 5
	subl	$16, %esp
	movl	$0, -4(%ebp)
	jmp	.L22
.L25:
	movl	-4(%ebp), %eax
	leal	0(,%eax,4), %edx
	movl	8(%ebp), %eax
	addl	%edx, %eax
	movl	(%eax), %edx
	movl	-4(%ebp), %eax
	addl	$1, %eax
	leal	0(,%eax,4), %ecx
	movl	8(%ebp), %eax
	addl	%ecx, %eax
	movl	(%eax), %eax
	cmpl	%eax, %edx
	jle	.L23
	movl	$0, %eax
	jmp	.L24
.L23:
	addl	$1, -4(%ebp)
.L22:
	movl	12(%ebp), %eax
	leal	-1(%eax), %edx
	movl	-4(%ebp), %eax
	cmpl	%eax, %edx
	ja	.L25
	movl	$1, %eax
.L24:
	leave
	.cfi_restore 5
	.cfi_def_cfa 4, 4
	ret
	.cfi_endproc
.LFE6:
	.size	test_sort, .-test_sort
	.section	.rodata
.LC5:
	.string	"fatal memory"
	.text
	.globl	xcalloc
	.type	xcalloc, @function
xcalloc:
.LFB7:
	.cfi_startproc
	pushl	%ebp
	.cfi_def_cfa_offset 8
	.cfi_offset 5, -8
	movl	%esp, %ebp
	.cfi_def_cfa_register 5
	subl	$24, %esp
	movl	$0, -12(%ebp)
	subl	$8, %esp
	pushl	12(%ebp)
	pushl	8(%ebp)
	call	calloc
	addl	$16, %esp
	movl	%eax, -12(%ebp)
	cmpl	$0, -12(%ebp)
	jne	.L27
	subl	$12, %esp
	pushl	$.LC5
	call	puts
	addl	$16, %esp
	subl	$12, %esp
	pushl	$1
	call	exit
.L27:
	movl	-12(%ebp), %eax
	leave
	.cfi_restore 5
	.cfi_def_cfa 4, 4
	ret
	.cfi_endproc
.LFE7:
	.size	xcalloc, .-xcalloc
	.ident	"GCC: (Ubuntu 5.4.0-6ubuntu1~16.04.12) 5.4.0 20160609"
	.section	.note.GNU-stack,"",@progbits
