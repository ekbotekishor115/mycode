#int a[5] = {10,20,30,40,50};
.section .data
	.globl	a
	.type	a,@object
	.size	a,20
	.align	4
	a:
	.int	10
	.int	20
	.int	30
	.int	40
	.int	50
#----------------------
	a:
	.int	10,20,30,40,50

#int b[] = {100,200,300};
.section .data
	.globl	b
	.type	b,@object
	.size	b,12
	.align	4
	b:
	.int	100
	.int	200
	.int	300
#----------------------------
	b:
	.int	100,200,300

#int c[10] = {-1,-2};
.section .data
	.globl	c
	.type	c,@object
	.size	c,40
	.align	4
	c:
	.int	-1,-2
	.zero	32
#----------------------------
	c:
	.int	-1
	.int	-2
	.zero	32


#short d[3] = {0xAA12,0x9183,0x8843};
.section .data
	.globl	d
	.type	d,@object
	.size	d,6
	.align	4
	d:
	.value	0xAA12,0x9183,0x8843
#------------------------------
	d:
	.value	0xAA12
	.value	0x9183
	.value	0x8843

#char msg[] = "Hello,world";
.section .data
	.globl	msg
	.type	msg,@object
	.size	msg,11
	.align	4
	msg:
	.ascii	"H"
	.ascii	"e"
	.ascii	"l"
	.ascii	"l"
	.ascii	"o"
	.ascii	","
	.ascii	"w"
	.ascii	"o"
	.ascii	"r"
	.ascii	"l"
	.ascii	"d"
#-----------------------------
	msg:
	.string	"Hello,world"

#char *s = "Hey";
.section .rodata
	s:
	.string	"Hey"
