.section .rodata
	msgp1:
	.string	"Enter at address p+%d:"
	scan1:
	.string	"%d"
	msgp2:
	.string	"*(p+%d):%d\n"
	msgp3:
	.string	"xcalloc:fatal:out of memory"


.section .text
	.globl	main
	.type	main,@function
main:
	pushl	%ebp
	movl	%esp,%ebp
	andl	$-16,%esp
	subl	$16,%esp

	movl	$0, -4(%ebp)
	pushl	$4
	pushl	$5
	call	xcalloc
	addl	$8, %esp	
	movl	%eax,-4(%ebp)
	movl	$0,-8(%ebp)
	jmp	for1_cond

	for1:
		movl	-8(%ebp), %ecx
		pushl	%ecx	
		pushl	$msgp1
		call	printf
		addl	$8, %esp
		movl	-8(%ebp), %ecx
		movl	-4(%ebp), %ebx
		leal	(%ebx,%ecx,4),%edx
		pushl	%edx
		pushl	$scan1
		call	scanf
		addl	$8, %esp
		addl	$1,-8(%ebp)
		
	for1_cond:
		movl	-8(%ebp), %ecx
		cmpl	$5, %ecx
		jl	for1

		movl	$0, -8(%ebp)
		jmp	for2_cond
	
	for2:
		movl	-8(%ebp), %ecx
		movl	-4(%ebp), %ebx
		movl	(%ebx,%ecx,4),%edx
		pushl	%edx
		pushl	%ecx
		pushl	$msgp2
		call	printf
		addl	$12, %esp
		addl	$1, -8(%ebp)
	for2_cond:
		movl	-8(%ebp), %ecx
		cmpl	$5, %ecx
		jl	for2	

	pushl	-4(%ebp)
	call	free
	movl	$0,-4(%ebp)
	addl	$4, %esp
	pushl	$0
	call	exit
	
	.globl	xcalloc
	.type	xcalloc,@function

xcalloc:	
	pushl	%ebp
	movl	%esp,%ebp
	subl	$16,%esp

	movl	12(%ebp), %ecx
	movl	8(%ebp), %edx
	pushl	%ecx
	pushl	%edx
	call	calloc
	addl	$8,%esp
	movl	%eax, -4(%ebp)
	cmpl	$0,-4(%ebp)
	je	if_cond
	movl	%ebp,%esp
	popl	%ebp
	ret

	if_cond:
		pushl	$msgp3
		call	printf
		addl	$4,%esp
		pushl	$-1
		call	exit
