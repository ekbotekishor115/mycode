.section .rodata
	msg_p1:
	.string "Assembly\n"
.section .text
	.globl	main
	.type	main,@function

main:
	pushl	%ebp
	movl	%esp, %ebp
	andl	$-16, %esp
	subl	$16, %esp

	pushl	$msg_p1
	call	printf
	addl	$4, %esp
	pushl	$0
	call	exit
