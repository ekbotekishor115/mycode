.section .rodata
	msgp1:
	.string	"n1 = %d\nn2 = %x\nx = %c\nnum1 = %d\nc = %c\nnum2 = %d\n"

.section .data
	.globl	t
	.type	t,@object
	.size	t,12
	.align	4
	t:
	.int	10
	.ascii	"A"
	.zero	3
	.int	0x10430257


.section .bss
	.comm	n1,4,4
	.comm	n2,4,4
	.comm	x,1,4

.section .text
	.globl	main
	.type	main,@function

main:
	pushl	%ebp
	movl	%esp, %ebp
	subl	$16, %esp
	
	movl	$t, %eax
	movl	(%eax), %ebx
	movl	%ebx, n1
	
	movb	4(%eax), %dl
	movb	%dl, x
	
	movl 	8(%eax), %ebx
	movl	%ebx, n2

	movl	$100,(%eax) 

	movl	$'z',4(%eax)

	movl	$200,8(%eax)

	pushl	8(%eax)
	pushl	4(%eax)
	pushl	(%eax)
	pushl	x
	pushl	n2
	pushl	n1
	pushl	$msgp1
	call	printf

	addl	$16, %esp
	pushl	$0
	call	exit

	
