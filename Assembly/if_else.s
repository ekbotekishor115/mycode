.section .rodata
	msg_p1:
	.string	"j is lesser than k\n"
	msg_p2:
	.string	"j is greater than k\n"
	msg_p3:
	.string	"j is equal to k\n"

.section .data
	.globl	i
	.type	i,@object
	.size	i,4
	.align	4
	i:
	.int	6

	.globl	j
	.type	j,@object
	.size	j,4
	.align	4
	j:
	.int	3

.section .bss
	.comm	k,4,4

.section .text
	.globl	main
	.type	main,@function
	
main:
	pushl	%ebp
	movl	%esp, %ebp
	subl	$16, %esp

	movl	$0, k
	jmp	mn_cond

mn_for:
	movl	k, %ebx
	cmpl	j,%ebx
	jnle	mn_for1
	jnge	mn_for2
	je	mn_for3

mn_for1:
	pushl	$msg_p1
	call	printf
	addl	$1, k
	jmp	mn_cond

mn_for2:
	pushl	$msg_p2
	call	printf
	addl	$1,k
	jmp	mn_cond

mn_for3:
	pushl	$msg_p3
	call	printf
	addl	$1,k
	jmp	mn_cond

mn_cond:
	movl	k, %eax
	cmpl	i, %eax
	jle	mn_for

	addl	$12, %esp
	pushl	$0
	call	exit
