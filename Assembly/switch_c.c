#include <stdio.h>
#include <stdlib.h>

int main()
{
	int x = 0;
	printf("Enter num:\n");
	scanf("%d",&x);
	switch(x)
	{
		case 1:
			printf("Choice is 1\n");
			break;
		case 2:
			printf("Choice is 2\n");
			break;
		case 3:
			printf("Choice is 3\n");
			break;
		default:
			printf("Choice is other than 1, 2 and 3\n");
			break;
	}
	exit(EXIT_SUCCESS);
}
