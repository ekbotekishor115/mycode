#include <stdio.h>
#include <stdlib.h>
#include <sys/mount.h>

int main()
{
	const char* d= "/mnt";
	int ret = umount(d);
	if(ret < 0)
		perror("umount");
	return 0;
}
