#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/uio.h>

int main(int argc, char*argv[])
{
	char f[BUFSIZ],s[BUFSIZ];
	char buf[BUFSIZ];

	if (argc != 3)
	{
		printf("%s,wrong argv\n",argv[0]);
		exit(EXIT_FAILURE);
	}
	
	strcpy(f,argv[1]);
	strcpy(s,argv[2]);

	printf("first argument is %s\n second argument is %s\n",f,s);

	int fd = open(f,O_RDONLY);
	if (fd < 0)
	{
		perror("Open system call");
		exit(EXIT_FAILURE);
	}
	
//	char *str0 = f;
//	char *str1 = s;
//	char *str2 = buf;
	struct iovec iov[8];
	ssize_t count;

	iov[0].iov_base = f;
	iov[0].iov_len = BUFSIZ;
	iov[1].iov_base = s;
	iov[1].iov_len = BUFSIZ;
	iov[2].iov_base = buf;
	iov[2].iov_len = BUFSIZ;
	iov[3].iov_base = buf;
        iov[3].iov_len = BUFSIZ;
        iov[4].iov_base = buf;
        iov[4].iov_len = BUFSIZ;
        iov[5].iov_base = buf;
        iov[5].iov_len = BUFSIZ;
	iov[6].iov_base = buf; 
        iov[6].iov_len = BUFSIZ;
        iov[7].iov_base = buf;
        iov[7].iov_len = BUFSIZ;


	int cr_fd = creat(s, S_IRUSR | S_IWUSR | S_IRGRP | S_IWGRP | S_IROTH | S_IWOTH);
	if (cr_fd < 0)
	{
		perror("Create file failure");
		exit(EXIT_FAILURE);
	}
	
        ssize_t wr_count;

	while((count = readv(fd, iov, 8)) > 0) 
	{
		wr_count = writev(cr_fd, iov,8);
		if (wr_count < 0)
		{
			perror("Write failed");
		}
	}

	int cl_fd = close(fd);
	if(cl_fd < 0)
	{
		perror("File not closed");
	}
	
	close(cr_fd);
	exit(EXIT_SUCCESS);
}
