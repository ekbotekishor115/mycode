#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <signal.h>
#include <errno.h>
#include "fifo_seqnum.h"

int main(int argc,char **argv)
{
	int serverFd,dummyFd,clientFd;
	char clientFifo[CLIENT_FIFO_NAME_LEN];
	struct request req;
	struct response resp;
	int seqNum = 0;

	umask(0);
	if (mkfifo(SERVER_FIFO,S_IRUSR | S_IWUSR | S_IWGRP) == -1 && errno != EEXIST)
	{
		printf("mkfifo %s\n",SERVER_FIFO);
		exit(EXIT_FAILURE);
	}
	printf("1.....\n");

	serverFd = open(SERVER_FIFO,O_RDONLY);
	if (serverFd == -1)
	{
		printf("open %s",SERVER_FIFO);
		perror("open");
	}

	printf("2.....\n");
	dummyFd = open(SERVER_FIFO,O_WRONLY);
	if (dummyFd == -1)
	{
		printf("open %s",SERVER_FIFO);
		perror("open");
	}
	printf("3.....\n");
	
	if (signal(SIGPIPE,SIG_IGN) == SIG_ERR)
		perror("signal");
	printf("4.....\n");

	for (;;)
	{
		if (read(serverFd,&req,sizeof(struct request)) != sizeof(struct request))
		{
			fprintf(stderr,"Error reading request; discarding\n");
			continue;
		}

		printf("5.....\n");
		snprintf(clientFifo,CLIENT_FIFO_NAME_LEN,CLIENT_FIFO_TEMPLATE,(long)req.pid);
		printf("6.....\n");
		clientFd = open(clientFifo,O_WRONLY);
		if (clientFd == -1)
		{
			printf("open %s",clientFifo);
			continue;
		}
		printf("7.....\n");
	
		resp.seqNum = seqNum;
		if (write(clientFd,&resp,sizeof(struct response)) != sizeof(struct response))
			fprintf(stderr,"Error writing to FIFO %s\n",clientFifo);
		if (close(clientFd) == -1)
			perror("close");
	
		seqNum += req.seqLen;
	}
	printf("8.....\n");
}
