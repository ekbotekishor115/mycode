	.file	"sentinel1.c"
	.section	.rodata
.LC0:
	.string	"Enter number\t"
.LC1:
	.string	"%d"
.LC2:
	.string	"Want to enter more\t"
.LC3:
	.string	"\nEnter number\t"
.LC4:
	.string	"\nMaximum is %d\n"
.LC5:
	.string	"\nMinimum is %d\n"
	.text
	.globl	main
	.type	main, @function
main:
.LFB2:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movl	$.LC0, %edi
	movl	$0, %eax
	call	printf
	leaq	-16(%rbp), %rax
	movq	%rax, %rsi
	movl	$.LC1, %edi
	movl	$0, %eax
	call	__isoc99_scanf
	movl	-16(%rbp), %eax
	movl	%eax, -12(%rbp)
	movl	-12(%rbp), %eax
	movl	%eax, -8(%rbp)
	movl	$.LC2, %edi
	movl	$0, %eax
	call	printf
	call	getchar
	movb	%al, -1(%rbp)
	jmp	.L2
.L5:
	movl	$.LC3, %edi
	movl	$0, %eax
	call	printf
	leaq	-16(%rbp), %rax
	movq	%rax, %rsi
	movl	$.LC1, %edi
	movl	$0, %eax
	call	__isoc99_scanf
	movl	-16(%rbp), %eax
	cmpl	-8(%rbp), %eax
	jle	.L3
	movl	-16(%rbp), %eax
	movl	%eax, -8(%rbp)
	jmp	.L4
.L3:
	movl	-16(%rbp), %eax
	cmpl	-12(%rbp), %eax
	jge	.L4
	movl	-16(%rbp), %eax
	movl	%eax, -12(%rbp)
.L4:
	movl	$.LC2, %edi
	movl	$0, %eax
	call	printf
	call	getchar
	movb	%al, -1(%rbp)
.L2:
	cmpb	$121, -1(%rbp)
	je	.L5
	cmpb	$89, -1(%rbp)
	je	.L5
	movl	-8(%rbp), %eax
	movl	%eax, %esi
	movl	$.LC4, %edi
	movl	$0, %eax
	call	printf
	movl	-12(%rbp), %eax
	movl	%eax, %esi
	movl	$.LC5, %edi
	movl	$0, %eax
	call	printf
	movl	$0, %edi
	call	exit
	.cfi_endproc
.LFE2:
	.size	main, .-main
	.ident	"GCC: (Ubuntu 5.4.0-6ubuntu1~16.04.12) 5.4.0 20160609"
	.section	.note.GNU-stack,"",@progbits
