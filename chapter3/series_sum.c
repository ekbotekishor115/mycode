#include <stdio.h>
#include <stdlib.h>

int main()
{
	int num,i=1,j,sum=0;
	printf("Enter the number of terms\t");
	scanf("%d",&num);
	while(i<=num)
	{
		j=1;
		printf("(");
		while(j<=i)
		{
			printf("%d",j);
			sum =sum+j;
			j++;
			if(j<=i)
			{	
				printf("+");
			}
			else
			{
				printf(")");
				printf("+");
			}
		}
		if(j<i)
			printf("+");
		i++;
	}
	printf("= %d\n",sum);
	exit(0);
}
