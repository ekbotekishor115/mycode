#include <stdio.h>
#include <stdlib.h>

int main()
{
	int n,r=1,val=1,j;
	printf("Enter the number of rows in triangle\t");
	scanf("%d",&n);
	while(r<=n)
	{
		for(j = 1; j <= r; j++)
			printf("%d ",val++);
		printf("\n");
		r++;
	}
	exit(0);
}
