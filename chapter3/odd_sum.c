#include <stdio.h>
#include <stdlib.h>

int main()
{
	int n, sum=0,i;
	printf("Enter the value of n\t");
	scanf("%d",&n);
	while(i<=n)
	{
		if(i%2!=0)
			sum=sum+i;
		i++;
	}
	printf("Sum of the odd numbers till %d is %d\n",n,sum);
	exit(0);
}
