	.file	"2break1.c"
	.section	.rodata
.LC0:
	.string	"One"
.LC1:
	.string	"Two"
.LC2:
	.string	"Default"
	.align 8
.LC3:
	.string	"\n This statement is next to switch"
	.text
	.globl	main
	.type	main, @function
main:
.LFB2:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movl	$1, -4(%rbp)
	movl	-4(%rbp), %eax
	cmpl	$1, %eax
	je	.L3
	cmpl	$2, %eax
	je	.L4
	jmp	.L6
.L3:
	movl	$.LC0, %edi
	call	puts
	jmp	.L5
.L4:
	movl	$.LC1, %edi
	call	puts
	jmp	.L5
.L6:
	movl	$.LC2, %edi
	call	puts
	nop
.L5:
	movl	$.LC3, %edi
	call	puts
	movl	$0, %edi
	call	exit
	.cfi_endproc
.LFE2:
	.size	main, .-main
	.ident	"GCC: (Ubuntu 5.4.0-6ubuntu1~16.04.12) 5.4.0 20160609"
	.section	.note.GNU-stack,"",@progbits
