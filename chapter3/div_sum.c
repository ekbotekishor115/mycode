#include <stdio.h>
#include <stdlib.h>

int main()
{
	int n, i=1;
	float sum=0;
	printf("Enter the number of terms\t");
	scanf("%d",&n);
	printf("n is %d\n",n);
	while(i<=n)
	{
		sum = sum + 1/(float)i;
		i++;
	}
	printf("Sum of series is %f\n",sum);
	exit(0);
}

// SEGMENTATION fault with Tool-Chain
