	.file	"series_sum1.c"
	.section	.rodata
.LC0:
	.string	"Enter the number of terms\t"
.LC1:
	.string	"%d"
.LC2:
	.string	"= %d\n"
	.text
	.globl	main
	.type	main, @function
main:
.LFB2:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movl	$1, -4(%rbp)
	movl	$0, -12(%rbp)
	movl	$.LC0, %edi
	movl	$0, %eax
	call	printf
	leaq	-16(%rbp), %rax
	movq	%rax, %rsi
	movl	$.LC1, %edi
	movl	$0, %eax
	call	__isoc99_scanf
	jmp	.L2
.L7:
	movl	$1, -8(%rbp)
	movl	$40, %edi
	call	putchar
	jmp	.L3
.L5:
	movl	-8(%rbp), %eax
	movl	%eax, %esi
	movl	$.LC1, %edi
	movl	$0, %eax
	call	printf
	movl	-8(%rbp), %eax
	addl	%eax, -12(%rbp)
	addl	$1, -8(%rbp)
	movl	-8(%rbp), %eax
	cmpl	-4(%rbp), %eax
	jg	.L4
	movl	$43, %edi
	call	putchar
	jmp	.L3
.L4:
	movl	$41, %edi
	call	putchar
	movl	$43, %edi
	call	putchar
.L3:
	movl	-8(%rbp), %eax
	cmpl	-4(%rbp), %eax
	jle	.L5
	movl	-8(%rbp), %eax
	cmpl	-4(%rbp), %eax
	jge	.L6
	movl	$43, %edi
	call	putchar
.L6:
	addl	$1, -4(%rbp)
.L2:
	movl	-16(%rbp), %eax
	cmpl	%eax, -4(%rbp)
	jle	.L7
	movl	-12(%rbp), %eax
	movl	%eax, %esi
	movl	$.LC2, %edi
	movl	$0, %eax
	call	printf
	movl	$0, %edi
	call	exit
	.cfi_endproc
.LFE2:
	.size	main, .-main
	.ident	"GCC: (Ubuntu 5.4.0-6ubuntu1~16.04.12) 5.4.0 20160609"
	.section	.note.GNU-stack,"",@progbits
