#include <stdio.h>
#include <stdlib.h>

int main()
{
	int i=1,n;
	float sum, term, x;
	printf("Enter the value of x in Radians\t");
	scanf("%f",&x);
	printf("Enter the power of end term\t");
	scanf("%d",&n);
	sum = 0;
	term = x;
	i = 1;
	while(i<=n)
	{
		sum = sum + term;
		term = (term * x * x * -1)/((i+1)*(i+2));
		i = i+2;
	}
	printf("Sin of %4.2f is %f",x,sum);
	exit (0);
}

/*SEGMENTATION FAULT with tool chain*/

