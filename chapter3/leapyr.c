#include <stdio.h>
#include <stdlib.h>

int main()
{
	int yr;
	printf("Enter the year\t");
	scanf("%d",&yr);
	if(((yr%4==0) && (yr%100!=0)) || (yr%400==0))
		printf("%d is leap year\n",yr);
	else
		printf("%d is not leap year\n",yr);
	exit (0);
}
