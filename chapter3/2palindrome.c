#include <stdio.h>
#include <stdlib.h>

int main()
{
	int num,temp,reverse = 0,add = 0,digit;
	printf("Enter the number\t");
	scanf("%d",&num);
	while(1)
	{
		temp = num;
		reverse = 0;
		while(temp != 0)
		{
			digit = temp%10;
			reverse = reverse*10 + digit;
			temp = temp/10;
		}
		if (num == reverse)
		{
			printf("\nPalindrome is %d and no. of addition is %d\n",reverse,add);
			break;
		}
		else
		{
			printf("%d\n",num);
			printf("+%d\n",reverse);
			num = num + reverse;
			printf("--------\n");
			printf("%d\n",num);
			printf("--------\n");
			add++;
		}
	}
	exit(0);
}
