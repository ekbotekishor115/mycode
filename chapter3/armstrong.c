#include <stdio.h>
#include <stdlib.h>

int main()
{
	int num,tmp,digit,sum=0;
	printf("Enter the number\t");
	scanf("%d",&num);
	tmp=num;
	while(tmp!=0)
	{
		digit=tmp%10;
		sum=sum+digit*digit*digit;
		tmp=tmp/10;
	}

	if(num==sum)
	{
		printf("%d is an Armstrong number\n",num);
	}
	else
	{
		printf("%d is not an Armstrong number\n",num);
	}
	exit(0);
}
