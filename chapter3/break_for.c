#include <stdio.h>
#include <stdlib.h>

int main()
{
	int i;
	for(i=1;i<=10;i++) 
	{
		if(i==5)
			break;
		printf("%d\n",i);
	}
	if(i<11)
		printf("\nPremature Termination\n");
	exit(0);
}
