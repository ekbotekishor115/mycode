#include <stdio.h>
#include <stdlib.h>

int main()
{
	int num,digit,rev=0,tmp;
	printf("Enter the number\t");
	scanf("%d",&num);
	tmp=num;
	while (tmp!=0)
	{
		digit=tmp%10;
		tmp=tmp/10;
		rev=rev*10+digit;
	}
	if(num==rev)
		printf("%d is Palindrome\n",num);
	else
		printf("%d is not Palindrome\n",num);
	exit(0);
}


