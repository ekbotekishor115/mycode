	.file	"pyramid1.c"
	.section	.rodata
	.align 8
.LC0:
	.string	"Enter the number of rows in pyramid\t"
.LC1:
	.string	"%d"
.LC2:
	.string	"%d "
	.text
	.globl	main
	.type	main, @function
main:
.LFB2:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movl	$1, -4(%rbp)
	movl	$.LC0, %edi
	movl	$0, %eax
	call	printf
	leaq	-16(%rbp), %rax
	movq	%rax, %rsi
	movl	$.LC1, %edi
	movl	$0, %eax
	call	__isoc99_scanf
	jmp	.L2
.L10:
	movl	-4(%rbp), %eax
	movl	%eax, -8(%rbp)
	movl	$1, -12(%rbp)
	jmp	.L3
.L4:
	movl	$32, %edi
	call	putchar
	addl	$1, -12(%rbp)
.L3:
	movl	-16(%rbp), %eax
	subl	-4(%rbp), %eax
	cmpl	-12(%rbp), %eax
	jge	.L4
	movl	$1, -12(%rbp)
	jmp	.L5
.L9:
	movl	-4(%rbp), %eax
	addl	%eax, %eax
	subl	$1, %eax
	movl	%eax, %edx
	shrl	$31, %edx
	addl	%edx, %eax
	sarl	%eax
	cmpl	-12(%rbp), %eax
	jl	.L6
	movl	-8(%rbp), %eax
	leal	1(%rax), %edx
	movl	%edx, -8(%rbp)
	movl	%eax, %esi
	movl	$.LC2, %edi
	movl	$0, %eax
	call	printf
	jmp	.L7
.L6:
	movl	-4(%rbp), %eax
	addl	%eax, %eax
	subl	$1, %eax
	movl	%eax, %edx
	shrl	$31, %edx
	addl	%edx, %eax
	sarl	%eax
	addl	$1, %eax
	cmpl	-12(%rbp), %eax
	jne	.L8
	movl	-8(%rbp), %eax
	movl	%eax, %esi
	movl	$.LC2, %edi
	movl	$0, %eax
	call	printf
	jmp	.L7
.L8:
	subl	$1, -8(%rbp)
	movl	-8(%rbp), %eax
	movl	%eax, %esi
	movl	$.LC2, %edi
	movl	$0, %eax
	call	printf
.L7:
	addl	$1, -12(%rbp)
.L5:
	movl	-4(%rbp), %eax
	addl	%eax, %eax
	subl	$1, %eax
	cmpl	-12(%rbp), %eax
	jge	.L9
	movl	$10, %edi
	call	putchar
	addl	$1, -4(%rbp)
.L2:
	movl	-16(%rbp), %eax
	cmpl	%eax, -4(%rbp)
	jle	.L10
	movl	$0, %edi
	call	exit
	.cfi_endproc
.LFE2:
	.size	main, .-main
	.ident	"GCC: (Ubuntu 5.4.0-6ubuntu1~16.04.12) 5.4.0 20160609"
	.section	.note.GNU-stack,"",@progbits
