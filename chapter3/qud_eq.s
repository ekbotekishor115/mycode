	.file	"qud_eq1.c"
	.section	.rodata
	.align 8
.LC0:
	.string	"Enter the coefficients a,b and c\t"
.LC1:
	.string	"%d %d %d"
.LC2:
	.string	"Roots are real and unequal"
.LC3:
	.string	"Roots are %f %f \n"
.LC4:
	.string	"Roots are real and equal"
.LC5:
	.string	"Roots are : %f %f \n"
	.align 8
.LC6:
	.string	"No real roots , roots are imagionary"
	.text
	.globl	main
	.type	main, @function
main:
.LFB2:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$48, %rsp
	movl	$.LC0, %edi
	movl	$0, %eax
	call	printf
	leaq	-24(%rbp), %rcx
	leaq	-20(%rbp), %rdx
	leaq	-16(%rbp), %rax
	movq	%rax, %rsi
	movl	$.LC1, %edi
	movl	$0, %eax
	call	__isoc99_scanf
	movl	-20(%rbp), %edx
	movl	-20(%rbp), %eax
	imull	%eax, %edx
	movl	-16(%rbp), %eax
	leal	0(,%rax,4), %ecx
	movl	-24(%rbp), %eax
	imull	%ecx, %eax
	subl	%eax, %edx
	movl	%edx, %eax
	movl	%eax, -4(%rbp)
	cmpl	$0, -4(%rbp)
	jle	.L2
	movl	-20(%rbp), %eax
	negl	%eax
	pxor	%xmm3, %xmm3
	cvtsi2sd	%eax, %xmm3
	movsd	%xmm3, -40(%rbp)
	pxor	%xmm0, %xmm0
	cvtsi2sd	-4(%rbp), %xmm0
	call	sqrt
	addsd	-40(%rbp), %xmm0
	movl	-16(%rbp), %eax
	addl	%eax, %eax
	pxor	%xmm1, %xmm1
	cvtsi2sd	%eax, %xmm1
	divsd	%xmm1, %xmm0
	cvtsd2ss	%xmm0, %xmm4
	movss	%xmm4, -8(%rbp)
	movl	-20(%rbp), %eax
	negl	%eax
	pxor	%xmm2, %xmm2
	cvtsi2sd	%eax, %xmm2
	movsd	%xmm2, -40(%rbp)
	pxor	%xmm0, %xmm0
	cvtsi2sd	-4(%rbp), %xmm0
	call	sqrt
	movsd	-40(%rbp), %xmm2
	subsd	%xmm0, %xmm2
	movapd	%xmm2, %xmm0
	movl	-16(%rbp), %eax
	addl	%eax, %eax
	pxor	%xmm1, %xmm1
	cvtsi2sd	%eax, %xmm1
	divsd	%xmm1, %xmm0
	cvtsd2ss	%xmm0, %xmm5
	movss	%xmm5, -12(%rbp)
	movl	$.LC2, %edi
	call	puts
	cvtss2sd	-12(%rbp), %xmm1
	cvtss2sd	-8(%rbp), %xmm0
	movl	$.LC3, %edi
	movl	$2, %eax
	call	printf
	jmp	.L3
.L2:
	cmpl	$0, -4(%rbp)
	jne	.L4
	movl	-20(%rbp), %eax
	negl	%eax
	movl	-16(%rbp), %edx
	leal	(%rdx,%rdx), %esi
	cltd
	idivl	%esi
	pxor	%xmm0, %xmm0
	cvtsi2ss	%eax, %xmm0
	movss	%xmm0, -8(%rbp)
	movl	$.LC4, %edi
	call	puts
	cvtss2sd	-12(%rbp), %xmm1
	cvtss2sd	-8(%rbp), %xmm0
	movl	$.LC5, %edi
	movl	$2, %eax
	call	printf
	jmp	.L3
.L4:
	movl	$.LC6, %edi
	call	puts
.L3:
	movl	$0, %edi
	call	exit
	.cfi_endproc
.LFE2:
	.size	main, .-main
	.ident	"GCC: (Ubuntu 5.4.0-6ubuntu1~16.04.12) 5.4.0 20160609"
	.section	.note.GNU-stack,"",@progbits
