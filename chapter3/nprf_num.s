	.file	"nprf_num1.c"
	.section	.rodata
	.align 8
.LC0:
	.string	"How many numbers you want to print"
.LC1:
	.string	"%d"
.LC2:
	.string	"Perfect numbers are:"
.LC3:
	.string	"%d\n"
	.text
	.globl	main
	.type	main, @function
main:
.LFB2:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$32, %rsp
	movl	$1, -4(%rbp)
	movl	$0, -8(%rbp)
	movl	$1, -16(%rbp)
	movl	$.LC0, %edi
	call	puts
	leaq	-20(%rbp), %rax
	movq	%rax, %rsi
	movl	$.LC1, %edi
	movl	$0, %eax
	call	__isoc99_scanf
	movl	$.LC2, %edi
	call	puts
	jmp	.L2
.L7:
	movl	$1, -12(%rbp)
	jmp	.L3
.L5:
	movl	-4(%rbp), %eax
	cltd
	idivl	-12(%rbp)
	movl	%edx, %eax
	testl	%eax, %eax
	jne	.L4
	movl	-12(%rbp), %eax
	addl	%eax, -8(%rbp)
.L4:
	addl	$1, -12(%rbp)
.L3:
	movl	-12(%rbp), %eax
	cmpl	-4(%rbp), %eax
	jl	.L5
	movl	-8(%rbp), %eax
	cmpl	-4(%rbp), %eax
	jne	.L6
	movl	-4(%rbp), %eax
	movl	%eax, %esi
	movl	$.LC3, %edi
	movl	$0, %eax
	call	printf
	addl	$1, -16(%rbp)
.L6:
	addl	$1, -4(%rbp)
	movl	$0, -8(%rbp)
.L2:
	movl	-20(%rbp), %eax
	cmpl	%eax, -16(%rbp)
	jle	.L7
	movl	$0, %edi
	call	exit
	.cfi_endproc
.LFE2:
	.size	main, .-main
	.ident	"GCC: (Ubuntu 5.4.0-6ubuntu1~16.04.12) 5.4.0 20160609"
	.section	.note.GNU-stack,"",@progbits
