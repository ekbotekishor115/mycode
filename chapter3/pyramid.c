#include <stdio.h>
#include <stdlib.h>

int main()
{
	int n,r=1,val,j;
	printf("Enter the number of rows in pyramid\t");
	scanf("%d",&n);
	while(r<=n)
	{
		val = r;
		for(j = 1; j<=n-r; j++)
		{
			printf(" ");
		}
		for(j = 1; j <= 2*r-1; j++)
		{
			if(j <= (2*r-1)/2)
				printf("%d ",val++);
			else if(j == (2*r-1)/2+1)
				printf("%d ",val);

			else
				printf("%d ",--val);
		}
		printf("\n");
		r++;
	}
	exit(0);
}
