#include <stdio.h>
#include <stdlib.h>

int main()
{
	int n,i=0,sum=0;
	printf("Enter the number of terms\t");
	scanf("%d",&n);
	while(i<=n)
	{
		sum=sum+i*i;
		i++;
	}
	printf("Sum of the series is %d\n",sum);
	exit(0);
}
