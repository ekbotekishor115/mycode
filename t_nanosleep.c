#define _POSIX_C_SOURCE 199309
#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>
#include <signal.h>
#include <errno.h>
#include <string.h>
#include <time.h>

static void sigintHandler(int sig)
{
	return;
}

int main(int argc,char *argv[])
{
	struct timeval start, finish;
	struct timespec request, remain;
	struct sigaction sa;
	int s;
	
	if(argc !=3 || strcmp(argv[1],"--help") == 0)
		printf("%s secs nanosecs\n",argv[0]);

	request.tv_sec = atoi(argv[1]);//getLong(argv[1],0,"secs");
	request.tv_nsec = atoi(argv[2]);//getLong(argv[2],0,"nanosecs");
	
	sigemptyset(&sa.sa_mask);
	sa.sa_flags = 0;
	sa.sa_handler = sigintHandler;
	if (sigaction(SIGINT,&sa,NULL) == -1)
		perror("sigaction");
	if(gettimeofday(&start, NULL) == -1)
		perror("gettimeofday");

	for(;;)
	{
		s = nanosleep(&request, &remain);
		if(s == -1 && errno != EINTR)
			perror("nanosleep");

		if(gettimeofday(&finish, NULL) == -1)
			perror("gettimeofday");
		printf("Slept for %9.6f secs\n",finish.tv_sec - start.tv_sec + (finish.tv_usec - start.tv_usec) / 1000000.0);
		
		if(s == 0)
			break;

		printf("Remaining: %2ld.%09ld\n",(long) remain.tv_sec,remain.tv_nsec);
		request = remain;
		
	}
	printf("Sleep complete\n");
	exit(EXIT_SUCCESS);
}
