#include <stdio.h>
#include <stdlib.h>

int main()
{
	int elements[20],num,i,key,found=0;
	printf("Enter the number of elements(max.20)\t");
	scanf("%d",&num);
	printf("Enter the elements\n");
	for(i=0;i<num;i++)
		scanf("%d",&elements[i]);
	printf("Enter the key that you want to search:\t");
	scanf("%d",&key);
	for(i=0;i<num;i++)
		if(elements[i]==key)
		{
			printf("%d exists at location %d\n",key,i+1);
			found=1;
		}
	if(found==0)
		printf("%d does not exist in the list\n",key);
	exit(0);
}
