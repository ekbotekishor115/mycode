	.file	"thirty1.c"
	.section	.rodata
	.align 8
.LC0:
	.string	"Enter the order of matrix-1(10 by 10)\t"
.LC1:
	.string	"%d %d"
	.align 8
.LC2:
	.string	"Enter the elements of matrix-1:"
.LC3:
	.string	"%d"
	.align 8
.LC4:
	.string	"Enter the order of matrix-2(10 by 10)\t"
	.align 8
.LC5:
	.string	"Enter the elements of matrix-2:"
	.align 8
.LC6:
	.string	"Matrices are not compatible for multiplication"
	.align 8
.LC7:
	.string	"The result of matrix multiplication is:"
.LC8:
	.string	"%d\t"
	.text
	.globl	main
	.type	main, @function
main:
.LFB2:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$1232, %rsp
	leaq	-1216(%rbp), %rdx
	movl	$0, %eax
	movl	$50, %ecx
	movq	%rdx, %rdi
	rep stosq
	movl	$.LC0, %edi
	movl	$0, %eax
	call	printf
	leaq	-1224(%rbp), %rdx
	leaq	-1220(%rbp), %rax
	movq	%rax, %rsi
	movl	$.LC1, %edi
	movl	$0, %eax
	call	__isoc99_scanf
	movl	$.LC2, %edi
	call	puts
	movl	$0, -4(%rbp)
	jmp	.L2
.L5:
	movl	$0, -8(%rbp)
	jmp	.L3
.L4:
	leaq	-416(%rbp), %rcx
	movl	-8(%rbp), %eax
	movslq	%eax, %rsi
	movl	-4(%rbp), %eax
	movslq	%eax, %rdx
	movq	%rdx, %rax
	salq	$2, %rax
	addq	%rdx, %rax
	addq	%rax, %rax
	addq	%rsi, %rax
	salq	$2, %rax
	addq	%rcx, %rax
	movq	%rax, %rsi
	movl	$.LC3, %edi
	movl	$0, %eax
	call	__isoc99_scanf
	addl	$1, -8(%rbp)
.L3:
	movl	-1224(%rbp), %eax
	cmpl	%eax, -8(%rbp)
	jl	.L4
	addl	$1, -4(%rbp)
.L2:
	movl	-1220(%rbp), %eax
	cmpl	%eax, -4(%rbp)
	jl	.L5
	movl	$.LC4, %edi
	movl	$0, %eax
	call	printf
	leaq	-1232(%rbp), %rdx
	leaq	-1228(%rbp), %rax
	movq	%rax, %rsi
	movl	$.LC1, %edi
	movl	$0, %eax
	call	__isoc99_scanf
	movl	$.LC5, %edi
	call	puts
	movl	$0, -4(%rbp)
	jmp	.L6
.L9:
	movl	$0, -8(%rbp)
	jmp	.L7
.L8:
	leaq	-816(%rbp), %rcx
	movl	-8(%rbp), %eax
	movslq	%eax, %rsi
	movl	-4(%rbp), %eax
	movslq	%eax, %rdx
	movq	%rdx, %rax
	salq	$2, %rax
	addq	%rdx, %rax
	addq	%rax, %rax
	addq	%rsi, %rax
	salq	$2, %rax
	addq	%rcx, %rax
	movq	%rax, %rsi
	movl	$.LC3, %edi
	movl	$0, %eax
	call	__isoc99_scanf
	addl	$1, -8(%rbp)
.L7:
	movl	-1232(%rbp), %eax
	cmpl	%eax, -8(%rbp)
	jl	.L8
	addl	$1, -4(%rbp)
.L6:
	movl	-1228(%rbp), %eax
	cmpl	%eax, -4(%rbp)
	jl	.L9
	movl	-1224(%rbp), %edx
	movl	-1228(%rbp), %eax
	cmpl	%eax, %edx
	je	.L10
	movl	$.LC6, %edi
	call	puts
	movl	$1, %edi
	call	exit
.L10:
	movl	$0, -4(%rbp)
	jmp	.L11
.L16:
	movl	$0, -8(%rbp)
	jmp	.L12
.L15:
	movl	$0, -12(%rbp)
	jmp	.L13
.L14:
	movl	-8(%rbp), %eax
	movslq	%eax, %rcx
	movl	-4(%rbp), %eax
	movslq	%eax, %rdx
	movq	%rdx, %rax
	salq	$2, %rax
	addq	%rdx, %rax
	addq	%rax, %rax
	addq	%rcx, %rax
	movl	-1216(%rbp,%rax,4), %ecx
	movl	-12(%rbp), %eax
	movslq	%eax, %rsi
	movl	-4(%rbp), %eax
	movslq	%eax, %rdx
	movq	%rdx, %rax
	salq	$2, %rax
	addq	%rdx, %rax
	addq	%rax, %rax
	addq	%rsi, %rax
	movl	-416(%rbp,%rax,4), %esi
	movl	-8(%rbp), %eax
	movslq	%eax, %rdi
	movl	-12(%rbp), %eax
	movslq	%eax, %rdx
	movq	%rdx, %rax
	salq	$2, %rax
	addq	%rdx, %rax
	addq	%rax, %rax
	addq	%rdi, %rax
	movl	-816(%rbp,%rax,4), %eax
	imull	%esi, %eax
	addl	%eax, %ecx
	movl	-8(%rbp), %eax
	movslq	%eax, %rsi
	movl	-4(%rbp), %eax
	movslq	%eax, %rdx
	movq	%rdx, %rax
	salq	$2, %rax
	addq	%rdx, %rax
	addq	%rax, %rax
	addq	%rsi, %rax
	movl	%ecx, -1216(%rbp,%rax,4)
	addl	$1, -12(%rbp)
.L13:
	movl	-1224(%rbp), %eax
	cmpl	%eax, -12(%rbp)
	jl	.L14
	addl	$1, -8(%rbp)
.L12:
	movl	-1232(%rbp), %eax
	cmpl	%eax, -8(%rbp)
	jl	.L15
	addl	$1, -4(%rbp)
.L11:
	movl	-1220(%rbp), %eax
	cmpl	%eax, -4(%rbp)
	jl	.L16
	movl	$.LC7, %edi
	call	puts
	movl	$0, -4(%rbp)
	jmp	.L17
.L20:
	movl	$0, -8(%rbp)
	jmp	.L18
.L19:
	movl	-8(%rbp), %eax
	movslq	%eax, %rcx
	movl	-4(%rbp), %eax
	movslq	%eax, %rdx
	movq	%rdx, %rax
	salq	$2, %rax
	addq	%rdx, %rax
	addq	%rax, %rax
	addq	%rcx, %rax
	movl	-1216(%rbp,%rax,4), %eax
	movl	%eax, %esi
	movl	$.LC8, %edi
	movl	$0, %eax
	call	printf
	addl	$1, -8(%rbp)
.L18:
	movl	-1232(%rbp), %eax
	cmpl	%eax, -8(%rbp)
	jl	.L19
	movl	$10, %edi
	call	putchar
	addl	$1, -4(%rbp)
.L17:
	movl	-1220(%rbp), %eax
	cmpl	%eax, -4(%rbp)
	jl	.L20
	movl	$0, %edi
	call	exit
	.cfi_endproc
.LFE2:
	.size	main, .-main
	.ident	"GCC: (Ubuntu 5.4.0-6ubuntu1~16.04.12) 5.4.0 20160609"
	.section	.note.GNU-stack,"",@progbits
