#include <stdio.h>
#include <stdlib.h>
#include <math.h>

int main()
{
	float elements[20],sum=0.0,mean,var,sd;
	int num,i;
	printf("Enter the number of elements(max.20)\t");
	scanf("%d",&num);
	printf("Enter the elements:\n");
	for(i=0;i<num;i++)	
	{
		scanf("%f",&elements[i]);
	}
	for(i=0;i<num;i++)
		sum=sum+elements[i];
	mean=sum/num;
	sum=0.0;
	for(i=0;i<num;i++)
		sum=sum+(elements[i]-mean)*(elements[i]-mean);
	var=sum/num;
	sd=sqrt(var);
	printf("Arithmetic mean is %f\n",mean);
	printf("Variance is %f\n",var);
	printf("Standard Deviation is %f\n",sd);
	exit(0);
}

//SEGMENTATION FAULT with tool-chain
