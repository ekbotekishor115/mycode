#include <stdio.h>
#include <stdlib.h>

int main()
{
	int matrix[10][10];
	int order , sum=0 , i , j;
	printf("Enter the order of square matrix(mqx.10)");
	scanf("%d",&order);
	printf("Enter the elements of matrix:\n");
	for(i=0 ; i<order ; i++)
	{
		for(j=0 ; j<order ; j++)
			scanf("%d",&matrix[i][j]);
	}
	for(i=0 ; i<order ; i++)
		sum=sum+matrix[i][i];
	printf("Sum of elements of principal diagonal is %d\n",sum);
	exit(0);
}
