#include <stdio.h>
#include <stdlib.h>

int main()
{
	float matrix[3][3],aug_matrix[3][6];
	float identity[3][3] = {1,0,0,0,1,0,0,0,1};
	float c,r,sub,det;
	int i,j,order=3,k,row,col;
	printf("Enter the elements of 3 by 3 matrix:\n");
	for(i=0; i<order; i++)
	{
		for(j=0; j<order; j++)
			scanf("%d",&matrix[i][j]);
	}

	det = matrix[0][0]*(matrix[1][1]*matrix[2][2] - matrix[2][1]*matrix[1][2]) - matrix[0][1]*(matrix[1][0]*matrix[2][2] - matrix[2][0]*matrix[1][2]) + matrix[0][2]*(matrix[1][0]*matrix[2][1] - matrix[2][0]*matrix[1][1]);

	if(det != 0)
	{
		for(i=0; i<order; i++)
			for(j=0; j<order; j++)
			{
				aug_matrix[i][j] = matrix[i][j];
				aug_matrix[i][j+3] = identity[i][j];
			}
		
		for(i=0; i<order; i++)
			for(j=0; j<order; j++)
			{
				if(i==j)
				{
					c = aug_matrix[i][i];
					for(k=0; k<6; k++)
						aug_matrix[i][k]=aug_matrix[i][k]/c;
					
					for(row=0; row<order; row++)
					{
						sub=aug_matrix[row][j];
						for(col=0; col<6; col++)
							if(row!=i)
							{
								aug_matrix[row][col] = sub * aug_matrix[i][col];
							}
					}
				}
			}
		printf("Inverse of the matrix is:\n");
		for(i=0; i<order; i++)
		{
			for(j=0; j<order; j++)
				printf("%5.2f\t",aug_matrix[i][j+3]);
			printf("\n");
		}
	}
	else
		printf("Inverse does not exist\n");
	exit(0);
}
