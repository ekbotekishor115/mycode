	.file	"thirteen1.c"
	.section	.rodata
	.align 8
.LC0:
	.string	"The result of sizeof operator is %d\n"
	.text
	.globl	main
	.type	main, @function
main:
.LFB2:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$32, %rsp
	movl	$10, -32(%rbp)
	movl	$15, -28(%rbp)
	movl	$20, -24(%rbp)
	movl	$25, -20(%rbp)
	movl	$30, -16(%rbp)
	movl	$20, %esi
	movl	$.LC0, %edi
	movl	$0, %eax
	call	printf
	movl	$0, %edi
	call	exit
	.cfi_endproc
.LFE2:
	.size	main, .-main
	.ident	"GCC: (Ubuntu 5.4.0-6ubuntu1~16.04.12) 5.4.0 20160609"
	.section	.note.GNU-stack,"",@progbits
