#include <stdio.h>
#include <stdlib.h>

int main()
{
	int mat1[10][10] ,mat2[10][10], resultant[10][10] ={0};
	int m1, n1,m2,n2,i,j,k;
	printf("Enter the order of matrix-1(10 by 10)\t");
	scanf("%d %d",&m1,&n1);
	printf("Enter the elements of matrix-1:\n");
	for(i=0 ; i<m1 ;i++)
	{
		for(j=0 ; j<n1 ; j++)
			scanf("%d",&mat1[i][j]);
	}
	printf("Enter the order of matrix-2(10 by 10)\t");
	scanf("%d %d",&m2, &n2);
	printf("Enter the elements of matrix-2:\n");
	for(i=0 ; i<m2 ; i++)
	{
		for(j=0 ; j<n2 ; j++)
			scanf("%d",&mat2[i][j]);
	}
	if(n1 != m2)
	{
		printf("Matrices are not compatible for multiplication\n");
		exit(1);
	}
	else
	{
		for(i=0 ; i<m1 ; i++)
			for(j=0 ; j<n2 ; j++)
				for(k=0 ; k<n1 ;k++)
					resultant[i][j] = resultant[i][j] + mat1[i][k] * mat2[k][j];
	}
	printf("The result of matrix multiplication is:\n");
	for(i=0 ; i<m1 ;i++)
	{
		for(j=0 ; j<n2 ;j++)
			printf("%d\t",resultant[i][j]);
		printf("\n");
	}
	exit(0);
}
