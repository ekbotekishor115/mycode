	.file	"twentyeight1.c"
	.section	.rodata
	.align 8
.LC0:
	.string	"Enter the number of elements in A(max.20)\t"
.LC1:
	.string	"%d"
	.align 8
.LC2:
	.string	"Enter the elements in sorted order:"
	.align 8
.LC3:
	.string	"Enter the number of elements in B(max.20)\t"
.LC4:
	.string	"After merging, elements are:"
	.text
	.globl	main
	.type	main, @function
main:
.LFB2:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$352, %rsp
	movl	$.LC0, %edi
	movl	$0, %eax
	call	printf
	leaq	-340(%rbp), %rax
	movq	%rax, %rsi
	movl	$.LC1, %edi
	movl	$0, %eax
	call	__isoc99_scanf
	movl	$.LC2, %edi
	call	puts
	movl	$0, -4(%rbp)
	jmp	.L2
.L3:
	leaq	-96(%rbp), %rax
	movl	-4(%rbp), %edx
	movslq	%edx, %rdx
	salq	$2, %rdx
	addq	%rdx, %rax
	movq	%rax, %rsi
	movl	$.LC1, %edi
	movl	$0, %eax
	call	__isoc99_scanf
	addl	$1, -4(%rbp)
.L2:
	movl	-340(%rbp), %eax
	cmpl	%eax, -4(%rbp)
	jl	.L3
	movl	$.LC3, %edi
	movl	$0, %eax
	call	printf
	leaq	-344(%rbp), %rax
	movq	%rax, %rsi
	movl	$.LC1, %edi
	movl	$0, %eax
	call	__isoc99_scanf
	movl	$.LC2, %edi
	call	puts
	movl	$0, -4(%rbp)
	jmp	.L4
.L5:
	leaq	-176(%rbp), %rax
	movl	-4(%rbp), %edx
	movslq	%edx, %rdx
	salq	$2, %rdx
	addq	%rdx, %rax
	movq	%rax, %rsi
	movl	$.LC1, %edi
	movl	$0, %eax
	call	__isoc99_scanf
	addl	$1, -4(%rbp)
.L4:
	movl	-344(%rbp), %eax
	cmpl	%eax, -4(%rbp)
	jl	.L5
	movl	$0, -4(%rbp)
	movl	$0, -8(%rbp)
	movl	$0, -16(%rbp)
	jmp	.L6
.L9:
	movl	-4(%rbp), %eax
	cltq
	movl	-96(%rbp,%rax,4), %edx
	movl	-8(%rbp), %eax
	cltq
	movl	-176(%rbp,%rax,4), %eax
	cmpl	%eax, %edx
	jg	.L7
	movl	-4(%rbp), %eax
	cltq
	movl	-96(%rbp,%rax,4), %edx
	movl	-16(%rbp), %eax
	cltq
	movl	%edx, -336(%rbp,%rax,4)
	addl	$1, -4(%rbp)
	jmp	.L8
.L7:
	movl	-8(%rbp), %eax
	cltq
	movl	-176(%rbp,%rax,4), %edx
	movl	-16(%rbp), %eax
	cltq
	movl	%edx, -336(%rbp,%rax,4)
	addl	$1, -8(%rbp)
.L8:
	addl	$1, -16(%rbp)
.L6:
	movl	-340(%rbp), %eax
	cmpl	%eax, -4(%rbp)
	jl	.L9
	movl	-344(%rbp), %eax
	cmpl	%eax, -8(%rbp)
	jl	.L9
	movl	-340(%rbp), %eax
	cmpl	%eax, -4(%rbp)
	jne	.L10
	movl	-4(%rbp), %eax
	movl	%eax, -12(%rbp)
	jmp	.L11
.L12:
	movl	-16(%rbp), %eax
	leal	1(%rax), %edx
	movl	%edx, -16(%rbp)
	movl	-12(%rbp), %edx
	movslq	%edx, %rdx
	movl	-176(%rbp,%rdx,4), %edx
	cltq
	movl	%edx, -336(%rbp,%rax,4)
	addl	$1, -12(%rbp)
.L11:
	movl	-344(%rbp), %eax
	cmpl	%eax, -12(%rbp)
	jl	.L12
	jmp	.L13
.L10:
	movl	-344(%rbp), %eax
	cmpl	%eax, -8(%rbp)
	jne	.L13
	movl	-4(%rbp), %eax
	movl	%eax, -12(%rbp)
	jmp	.L14
.L15:
	movl	-16(%rbp), %eax
	leal	1(%rax), %edx
	movl	%edx, -16(%rbp)
	movl	-12(%rbp), %edx
	movslq	%edx, %rdx
	movl	-96(%rbp,%rdx,4), %edx
	cltq
	movl	%edx, -336(%rbp,%rax,4)
	addl	$1, -12(%rbp)
.L14:
	movl	-340(%rbp), %eax
	cmpl	%eax, -12(%rbp)
	jl	.L15
.L13:
	movl	$.LC4, %edi
	call	puts
	movl	$0, -4(%rbp)
	jmp	.L16
.L17:
	movl	-4(%rbp), %eax
	cltq
	movl	-336(%rbp,%rax,4), %eax
	movl	%eax, %esi
	movl	$.LC1, %edi
	movl	$0, %eax
	call	printf
	addl	$1, -4(%rbp)
.L16:
	movl	-340(%rbp), %edx
	movl	-344(%rbp), %eax
	addl	%edx, %eax
	cmpl	-4(%rbp), %eax
	jg	.L17
	movl	$0, %edi
	call	exit
	.cfi_endproc
.LFE2:
	.size	main, .-main
	.ident	"GCC: (Ubuntu 5.4.0-6ubuntu1~16.04.12) 5.4.0 20160609"
	.section	.note.GNU-stack,"",@progbits
