#include <stdio.h>
#include <stdlib.h>

int main()
{
	int a[10][10],olc,ilc,rows,cols;
	printf("Enter the number of rows(<10):\t");
	scanf("%d",&rows);
	printf("Enter the number of columns(<10):\t");
	scanf("%d",&cols);
	printf("Enter the elements:\n");
	for(olc=0;olc<rows;olc++)
	{
		for(ilc=0;ilc<cols;ilc++)
		{
			scanf("%d",&a[olc][ilc]);
		}
	}
	printf("The entered elements were:\n");
	for(olc=0;olc<rows;olc++)
	{
		for(ilc=0;ilc<cols;ilc++)
		{
			printf("%d",a[olc][ilc]);
		}
		printf("\n");
	}
	exit(0);
}
