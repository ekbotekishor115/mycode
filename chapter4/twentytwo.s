	.file	"twentytwo1.c"
	.section	.rodata
	.align 8
.LC0:
	.string	"Enter the number of elements in the set (max.20)\t"
.LC1:
	.string	"%d"
.LC2:
	.string	"Enter the elements:"
	.align 8
.LC3:
	.string	"Maximum number in the set is%d\n"
	.align 8
.LC4:
	.string	"Minimum number in the set is%d\n"
	.text
	.globl	main
	.type	main, @function
main:
.LFB2:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$112, %rsp
	movl	$.LC0, %edi
	movl	$0, %eax
	call	printf
	leaq	-100(%rbp), %rax
	movq	%rax, %rsi
	movl	$.LC1, %edi
	movl	$0, %eax
	call	__isoc99_scanf
	movl	$.LC2, %edi
	call	puts
	movl	$0, -4(%rbp)
	jmp	.L2
.L3:
	leaq	-96(%rbp), %rax
	movl	-4(%rbp), %edx
	movslq	%edx, %rdx
	salq	$2, %rdx
	addq	%rdx, %rax
	movq	%rax, %rsi
	movl	$.LC1, %edi
	movl	$0, %eax
	call	__isoc99_scanf
	addl	$1, -4(%rbp)
.L2:
	movl	-100(%rbp), %eax
	cmpl	%eax, -4(%rbp)
	jl	.L3
	movl	-96(%rbp), %eax
	movl	%eax, -12(%rbp)
	movl	-12(%rbp), %eax
	movl	%eax, -8(%rbp)
	movl	$1, -4(%rbp)
	jmp	.L4
.L7:
	movl	-4(%rbp), %eax
	cltq
	movl	-96(%rbp,%rax,4), %eax
	cmpl	-8(%rbp), %eax
	jle	.L5
	movl	-4(%rbp), %eax
	cltq
	movl	-96(%rbp,%rax,4), %eax
	movl	%eax, -8(%rbp)
	jmp	.L6
.L5:
	movl	-4(%rbp), %eax
	cltq
	movl	-96(%rbp,%rax,4), %eax
	cmpl	-12(%rbp), %eax
	jge	.L6
	movl	-4(%rbp), %eax
	cltq
	movl	-96(%rbp,%rax,4), %eax
	movl	%eax, -12(%rbp)
.L6:
	addl	$1, -4(%rbp)
.L4:
	movl	-100(%rbp), %eax
	cmpl	%eax, -4(%rbp)
	jl	.L7
	movl	-8(%rbp), %eax
	movl	%eax, %esi
	movl	$.LC3, %edi
	movl	$0, %eax
	call	printf
	movl	-12(%rbp), %eax
	movl	%eax, %esi
	movl	$.LC4, %edi
	movl	$0, %eax
	call	printf
	movl	$0, %edi
	call	exit
	.cfi_endproc
.LFE2:
	.size	main, .-main
	.ident	"GCC: (Ubuntu 5.4.0-6ubuntu1~16.04.12) 5.4.0 20160609"
	.section	.note.GNU-stack,"",@progbits
