	.file	"two1.c"
	.section	.rodata
	.align 8
.LC3:
	.string	"Elements of array are initialized with"
.LC4:
	.string	"arr1:%d%d%d\n"
.LC5:
	.string	"arr2:%f%f%f\n"
	.text
	.globl	main
	.type	main, @function
main:
.LFB2:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$32, %rsp
	movl	$2, -16(%rbp)
	movl	$4, -12(%rbp)
	movl	$6, -8(%rbp)
	movss	.LC0(%rip), %xmm0
	movss	%xmm0, -32(%rbp)
	movss	.LC1(%rip), %xmm0
	movss	%xmm0, -28(%rbp)
	movss	.LC2(%rip), %xmm0
	movss	%xmm0, -24(%rbp)
	movl	$.LC3, %edi
	call	puts
	movl	-8(%rbp), %ecx
	movl	-12(%rbp), %edx
	movl	-16(%rbp), %eax
	movl	%eax, %esi
	movl	$.LC4, %edi
	movl	$0, %eax
	call	printf
	movss	-24(%rbp), %xmm0
	cvtss2sd	%xmm0, %xmm2
	movss	-28(%rbp), %xmm0
	cvtss2sd	%xmm0, %xmm1
	movss	-32(%rbp), %xmm0
	cvtss2sd	%xmm0, %xmm0
	movl	$.LC5, %edi
	movl	$3, %eax
	call	printf
	movl	$0, %edi
	call	exit
	.cfi_endproc
.LFE2:
	.size	main, .-main
	.section	.rodata
	.align 4
.LC0:
	.long	1115815936
	.align 4
.LC1:
	.long	1115947008
	.align 4
.LC2:
	.long	1116078080
	.ident	"GCC: (Ubuntu 5.4.0-6ubuntu1~16.04.12) 5.4.0 20160609"
	.section	.note.GNU-stack,"",@progbits
