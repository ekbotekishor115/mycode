	.file	"thirtysix1.c"
	.section	.rodata
	.align 8
.LC2:
	.string	"Enter the elements of 3 by 3 matrix:"
.LC3:
	.string	"%d"
.LC4:
	.string	"Inverse of the matrix is:"
.LC5:
	.string	"%5.2f\t"
.LC6:
	.string	"Inverse does not exist"
	.text
	.globl	main
	.type	main, @function
main:
.LFB2:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$208, %rsp
	movss	.LC0(%rip), %xmm0
	movss	%xmm0, -208(%rbp)
	pxor	%xmm0, %xmm0
	movss	%xmm0, -204(%rbp)
	pxor	%xmm0, %xmm0
	movss	%xmm0, -200(%rbp)
	pxor	%xmm0, %xmm0
	movss	%xmm0, -196(%rbp)
	movss	.LC0(%rip), %xmm0
	movss	%xmm0, -192(%rbp)
	pxor	%xmm0, %xmm0
	movss	%xmm0, -188(%rbp)
	pxor	%xmm0, %xmm0
	movss	%xmm0, -184(%rbp)
	pxor	%xmm0, %xmm0
	movss	%xmm0, -180(%rbp)
	movss	.LC0(%rip), %xmm0
	movss	%xmm0, -176(%rbp)
	movl	$3, -24(%rbp)
	movl	$.LC2, %edi
	call	puts
	movl	$0, -4(%rbp)
	jmp	.L2
.L5:
	movl	$0, -8(%rbp)
	jmp	.L3
.L4:
	leaq	-80(%rbp), %rcx
	movl	-8(%rbp), %eax
	movslq	%eax, %rsi
	movl	-4(%rbp), %eax
	movslq	%eax, %rdx
	movq	%rdx, %rax
	addq	%rax, %rax
	addq	%rdx, %rax
	addq	%rsi, %rax
	salq	$2, %rax
	addq	%rcx, %rax
	movq	%rax, %rsi
	movl	$.LC3, %edi
	movl	$0, %eax
	call	__isoc99_scanf
	addl	$1, -8(%rbp)
.L3:
	movl	-8(%rbp), %eax
	cmpl	-24(%rbp), %eax
	jl	.L4
	addl	$1, -4(%rbp)
.L2:
	movl	-4(%rbp), %eax
	cmpl	-24(%rbp), %eax
	jl	.L5
	movss	-80(%rbp), %xmm2
	movss	-64(%rbp), %xmm1
	movss	-48(%rbp), %xmm0
	mulss	%xmm1, %xmm0
	movss	-52(%rbp), %xmm3
	movss	-60(%rbp), %xmm1
	mulss	%xmm3, %xmm1
	subss	%xmm1, %xmm0
	mulss	%xmm2, %xmm0
	movss	-76(%rbp), %xmm3
	movss	-68(%rbp), %xmm2
	movss	-48(%rbp), %xmm1
	mulss	%xmm2, %xmm1
	movss	-56(%rbp), %xmm4
	movss	-60(%rbp), %xmm2
	mulss	%xmm4, %xmm2
	subss	%xmm2, %xmm1
	mulss	%xmm3, %xmm1
	subss	%xmm1, %xmm0
	movaps	%xmm0, %xmm1
	movss	-72(%rbp), %xmm3
	movss	-68(%rbp), %xmm2
	movss	-52(%rbp), %xmm0
	mulss	%xmm2, %xmm0
	movss	-56(%rbp), %xmm4
	movss	-64(%rbp), %xmm2
	mulss	%xmm4, %xmm2
	subss	%xmm2, %xmm0
	mulss	%xmm3, %xmm0
	addss	%xmm1, %xmm0
	movss	%xmm0, -28(%rbp)
	pxor	%xmm0, %xmm0
	ucomiss	-28(%rbp), %xmm0
	jp	.L29
	pxor	%xmm0, %xmm0
	ucomiss	-28(%rbp), %xmm0
	je	.L30
.L29:
	movl	$0, -4(%rbp)
	jmp	.L8
.L11:
	movl	$0, -8(%rbp)
	jmp	.L9
.L10:
	movl	-8(%rbp), %eax
	movslq	%eax, %rcx
	movl	-4(%rbp), %eax
	movslq	%eax, %rdx
	movq	%rdx, %rax
	addq	%rax, %rax
	addq	%rdx, %rax
	addq	%rcx, %rax
	movss	-80(%rbp,%rax,4), %xmm0
	movl	-8(%rbp), %eax
	movslq	%eax, %rcx
	movl	-4(%rbp), %eax
	movslq	%eax, %rdx
	movq	%rdx, %rax
	addq	%rax, %rax
	addq	%rdx, %rax
	addq	%rax, %rax
	addq	%rcx, %rax
	movss	%xmm0, -160(%rbp,%rax,4)
	movl	-8(%rbp), %eax
	leal	3(%rax), %esi
	movl	-8(%rbp), %eax
	movslq	%eax, %rcx
	movl	-4(%rbp), %eax
	movslq	%eax, %rdx
	movq	%rdx, %rax
	addq	%rax, %rax
	addq	%rdx, %rax
	addq	%rcx, %rax
	movss	-208(%rbp,%rax,4), %xmm0
	movslq	%esi, %rcx
	movl	-4(%rbp), %eax
	movslq	%eax, %rdx
	movq	%rdx, %rax
	addq	%rax, %rax
	addq	%rdx, %rax
	addq	%rax, %rax
	addq	%rcx, %rax
	movss	%xmm0, -160(%rbp,%rax,4)
	addl	$1, -8(%rbp)
.L9:
	movl	-8(%rbp), %eax
	cmpl	-24(%rbp), %eax
	jl	.L10
	addl	$1, -4(%rbp)
.L8:
	movl	-4(%rbp), %eax
	cmpl	-24(%rbp), %eax
	jl	.L11
	movl	$0, -4(%rbp)
	jmp	.L12
.L23:
	movl	$0, -8(%rbp)
	jmp	.L13
.L22:
	movl	-4(%rbp), %eax
	cmpl	-8(%rbp), %eax
	jne	.L14
	movl	-4(%rbp), %eax
	cltq
	movq	%rax, %rdx
	leaq	0(,%rdx,4), %rax
	movq	%rax, %rdx
	leaq	0(,%rdx,8), %rax
	subq	%rdx, %rax
	addq	%rbp, %rax
	subq	$160, %rax
	movss	(%rax), %xmm0
	movss	%xmm0, -32(%rbp)
	movl	$0, -12(%rbp)
	jmp	.L15
.L16:
	movl	-12(%rbp), %eax
	movslq	%eax, %rcx
	movl	-4(%rbp), %eax
	movslq	%eax, %rdx
	movq	%rdx, %rax
	addq	%rax, %rax
	addq	%rdx, %rax
	addq	%rax, %rax
	addq	%rcx, %rax
	movss	-160(%rbp,%rax,4), %xmm0
	divss	-32(%rbp), %xmm0
	movl	-12(%rbp), %eax
	movslq	%eax, %rcx
	movl	-4(%rbp), %eax
	movslq	%eax, %rdx
	movq	%rdx, %rax
	addq	%rax, %rax
	addq	%rdx, %rax
	addq	%rax, %rax
	addq	%rcx, %rax
	movss	%xmm0, -160(%rbp,%rax,4)
	addl	$1, -12(%rbp)
.L15:
	cmpl	$5, -12(%rbp)
	jle	.L16
	movl	$0, -16(%rbp)
	jmp	.L17
.L21:
	movl	-8(%rbp), %eax
	movslq	%eax, %rcx
	movl	-16(%rbp), %eax
	movslq	%eax, %rdx
	movq	%rdx, %rax
	addq	%rax, %rax
	addq	%rdx, %rax
	addq	%rax, %rax
	addq	%rcx, %rax
	movss	-160(%rbp,%rax,4), %xmm0
	movss	%xmm0, -36(%rbp)
	movl	$0, -20(%rbp)
	jmp	.L18
.L20:
	movl	-16(%rbp), %eax
	cmpl	-4(%rbp), %eax
	je	.L19
	movl	-20(%rbp), %eax
	movslq	%eax, %rcx
	movl	-4(%rbp), %eax
	movslq	%eax, %rdx
	movq	%rdx, %rax
	addq	%rax, %rax
	addq	%rdx, %rax
	addq	%rax, %rax
	addq	%rcx, %rax
	movss	-160(%rbp,%rax,4), %xmm0
	mulss	-36(%rbp), %xmm0
	movl	-20(%rbp), %eax
	movslq	%eax, %rcx
	movl	-16(%rbp), %eax
	movslq	%eax, %rdx
	movq	%rdx, %rax
	addq	%rax, %rax
	addq	%rdx, %rax
	addq	%rax, %rax
	addq	%rcx, %rax
	movss	%xmm0, -160(%rbp,%rax,4)
.L19:
	addl	$1, -20(%rbp)
.L18:
	cmpl	$5, -20(%rbp)
	jle	.L20
	addl	$1, -16(%rbp)
.L17:
	movl	-16(%rbp), %eax
	cmpl	-24(%rbp), %eax
	jl	.L21
.L14:
	addl	$1, -8(%rbp)
.L13:
	movl	-8(%rbp), %eax
	cmpl	-24(%rbp), %eax
	jl	.L22
	addl	$1, -4(%rbp)
.L12:
	movl	-4(%rbp), %eax
	cmpl	-24(%rbp), %eax
	jl	.L23
	movl	$.LC4, %edi
	call	puts
	movl	$0, -4(%rbp)
	jmp	.L24
.L27:
	movl	$0, -8(%rbp)
	jmp	.L25
.L26:
	movl	-8(%rbp), %eax
	addl	$3, %eax
	movslq	%eax, %rcx
	movl	-4(%rbp), %eax
	movslq	%eax, %rdx
	movq	%rdx, %rax
	addq	%rax, %rax
	addq	%rdx, %rax
	addq	%rax, %rax
	addq	%rcx, %rax
	movss	-160(%rbp,%rax,4), %xmm0
	cvtss2sd	%xmm0, %xmm0
	movl	$.LC5, %edi
	movl	$1, %eax
	call	printf
	addl	$1, -8(%rbp)
.L25:
	movl	-8(%rbp), %eax
	cmpl	-24(%rbp), %eax
	jl	.L26
	movl	$10, %edi
	call	putchar
	addl	$1, -4(%rbp)
.L24:
	movl	-4(%rbp), %eax
	cmpl	-24(%rbp), %eax
	jl	.L27
	jmp	.L28
.L30:
	movl	$.LC6, %edi
	call	puts
.L28:
	movl	$0, %edi
	call	exit
	.cfi_endproc
.LFE2:
	.size	main, .-main
	.section	.rodata
	.align 4
.LC0:
	.long	1065353216
	.ident	"GCC: (Ubuntu 5.4.0-6ubuntu1~16.04.12) 5.4.0 20160609"
	.section	.note.GNU-stack,"",@progbits
