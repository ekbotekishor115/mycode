#include <stdio.h>
#include <stdlib.h>

int main()
{
	int arr[2][2]={{2,1},{3,1}};
	int(*ptr)[2]=arr;
	printf("Address of row one is%p\n",arr[0]);
	printf("Address of row 2 is%p\n",ptr+1);
	printf("First element of row 1 is %d\n",arr[0][0]);
	printf("First element of row 2 is %d\n",ptr[1][0]);
	exit(0);
}
