#include <stdio.h> 
#include <stdlib.h>

int main()
{
	int i;
	int a[3], b[3] = {10,20,30};
	printf("Assigning an array to an array:\n");
	for(i=0;i<=sizeof(b[3]);i++)
	{
		a[i] = b[i];
	}
	printf("Elements of array a are:\n");
	printf("%d %d %d\n",a[0],a[1],a[2]);
	exit(0);
}
