#include <stdio.h>
#include <stdlib.h>

int main()
{
	char *cptr;
	int *iptr;
	float *fptr;
	printf("Pointer to character takes %d bytes\n",sizeof(cptr));
	printf("Pointer to integer takes %d bytes\n",sizeof(iptr));
	printf("Pointer to float takes %d bytes\n",sizeof(fptr));
	exit(0);
}
