#include <stdio.h>
#include <stdlib.h>

int main()
{
	int matrix[10][10],ut_matrix[10][10],unequal=0;
	int i, j, order;
	printf("Enter the order of the square matrix(max. 10 by 10)");
	scanf("%d",&order);
	printf("Enter the elements of matrix:\n");
	for (i=0 ;i<order ;i++)
	{
		for (j=0 ;j<order ;j++)
			scanf("%d",&matrix[i][j]);
	}
	
	for (i=0; i<order; i++)
		for(j=0; j<order; j++)
			if(i<=j)
				ut_matrix[i][j] = matrix[i][j];
			else
				ut_matrix[i][j] = 0;
	printf("Upper triangular matrixis:\n");
	for (i=0; i<order; i++)
	{
			for (j=0; j<order; j++)
			printf("%d\t",ut_matrix[i][j]);
		printf("\n");
	}
	exit(0);

}
