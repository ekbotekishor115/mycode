#include <stdio.h>
#include <stdlib.h>

int main()
{
	int matrix[10][10],mat_transp[10][10];
	int m,n,i,j;
	printf("Enter the order of the matrix(max.10 by 10):\t");
	scanf("%d %d",&m,&n);
	printf("Enter the elements of matrix:\n");
	for(i=0 ; i<m ; i++)
	{
		for(j=0 ; j<n ; j++)
			scanf("%d",&matrix[i][j]);
	}
	printf("Elements of input matrix are:\n");
	for(i=0 ; i<m ; i++)
        {
                for(j=0 ; j<n ; j++)
                       printf("%d\t",matrix[i][j]);
		printf("\n");
        }

	for(i=0 ; i<n ; i++)
	{
		for(j=0 ; j<m ; j++)
			mat_transp[i][j] = matrix[j][i];
	}
	printf("Transpose of the matrix is:\n");
	for(i=0 ; i<n ; i++)
	{
		for(j=0 ; j<m ; j++)
			printf("%d\t",mat_transp[i][j]);
		printf("\n");
	}
	exit(0);
}
