#include <stdio.h>
#include <stdlib.h>

int main()
{
	int A[20],B[20],C[40];
	int i,j,l,h,m,n;
	printf("Enter the number of elements in A(max.20)\t");
	scanf("%d",&m);
	printf("Enter the elements in sorted order:\n");
	for(i=0 ; i<m ; i++)
		scanf("%d",&A[i]);
	printf("Enter the number of elements in B(max.20)\t");
	scanf("%d",&n);
	printf("Enter the elements in sorted order:\n");
	for(i=0 ; i<n ;i++)
		scanf("%d",&B[i]);
	i=0,j=0,h=0;
	while(i<m || j<n)
	{
		if(A[i]<=B[j])
		{
			C[h] = A[i];
			i++;
		}
		else
		{
			C[h] = B[j];
			j++;
		}
		h++;
	}
	if(i==m)
		for(l=i ; l<n ; l++)
			C[h++] = B[l];
	else if(j==n)
		for(l=i ; l<m ; l++)
			C[h++] = A[l];
	printf("After merging, elements are:\n");
	for(i=0 ; i<m+n ; i++)
		printf("%d",C[i]);
	exit(0);
}
