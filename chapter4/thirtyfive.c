#include <stdio.h>
#include <stdlib.h>

int main()
{
	int matrix[10][10], notzero=0;
	int i,j,order;
	printf("Enter order of the square matrix(max. 10 by 10)");
	scanf("%d",&order);
	printf("Enter the elements of the matrix:\n");
	for(i=0; i<order; i++)
	{
		for (j=0; j<order; j++)
			scanf("%d",&matrix[i][j]);
	}
	for (i=0; i<order; i++)
	{
		for(j=0; j<order; j++)
			if(i>=j)
				if(matrix[i][j] != 0)
				{
					notzero = 1;
					break;
				}
		if(notzero==1)
			break;
	}
	if(notzero == 1)
		printf("The given matrix is not strictly upper triangular\n");
	else
		printf("The given matrix is strictly upper triangle\n");
	exit(0);
}
