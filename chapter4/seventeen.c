#include <stdio.h>
#include <stdlib.h>

int main()
{
	int a[2][2]={2,1,3,4};
	printf("No subscript used:\n");
	printf("%p\n",a);
	printf("One subscript used:\n");
	printf("%p %p\n",a[0],a[1]);
	printf("Two subscripts used:\n");
	printf("%d %d\n",a[0][0],a[0][1]);
	printf("%d %d\n",a[1][0],a[1][1]);
	exit(0);
}
