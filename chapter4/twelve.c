#include <stdio.h>
#include <stdlib.h>

int main()
{
	int arr[3] = {10,20,30};
	printf("First element of array is at %p\n",arr);
	printf("Second element of array is at %p\n",arr+1);
	printf("Third element of array is at %p\n",arr+2);
	exit(0);
}
