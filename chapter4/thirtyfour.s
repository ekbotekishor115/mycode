	.file	"thirtyfour1.c"
	.section	.rodata
	.align 8
.LC0:
	.string	"Enter the order of the square matrix(max. 10 by 10)"
.LC1:
	.string	"%d"
.LC2:
	.string	"Enter the elements of matrix:"
.LC3:
	.string	"Upper triangular matrixis:"
.LC4:
	.string	"%d\t"
	.text
	.globl	main
	.type	main, @function
main:
.LFB2:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$832, %rsp
	movl	$0, -12(%rbp)
	movl	$.LC0, %edi
	movl	$0, %eax
	call	printf
	leaq	-820(%rbp), %rax
	movq	%rax, %rsi
	movl	$.LC1, %edi
	movl	$0, %eax
	call	__isoc99_scanf
	movl	$.LC2, %edi
	call	puts
	movl	$0, -4(%rbp)
	jmp	.L2
.L5:
	movl	$0, -8(%rbp)
	jmp	.L3
.L4:
	leaq	-416(%rbp), %rcx
	movl	-8(%rbp), %eax
	movslq	%eax, %rsi
	movl	-4(%rbp), %eax
	movslq	%eax, %rdx
	movq	%rdx, %rax
	salq	$2, %rax
	addq	%rdx, %rax
	addq	%rax, %rax
	addq	%rsi, %rax
	salq	$2, %rax
	addq	%rcx, %rax
	movq	%rax, %rsi
	movl	$.LC1, %edi
	movl	$0, %eax
	call	__isoc99_scanf
	addl	$1, -8(%rbp)
.L3:
	movl	-820(%rbp), %eax
	cmpl	%eax, -8(%rbp)
	jl	.L4
	addl	$1, -4(%rbp)
.L2:
	movl	-820(%rbp), %eax
	cmpl	%eax, -4(%rbp)
	jl	.L5
	movl	$0, -4(%rbp)
	jmp	.L6
.L11:
	movl	$0, -8(%rbp)
	jmp	.L7
.L10:
	movl	-4(%rbp), %eax
	cmpl	-8(%rbp), %eax
	jg	.L8
	movl	-8(%rbp), %eax
	movslq	%eax, %rcx
	movl	-4(%rbp), %eax
	movslq	%eax, %rdx
	movq	%rdx, %rax
	salq	$2, %rax
	addq	%rdx, %rax
	addq	%rax, %rax
	addq	%rcx, %rax
	movl	-416(%rbp,%rax,4), %ecx
	movl	-8(%rbp), %eax
	movslq	%eax, %rsi
	movl	-4(%rbp), %eax
	movslq	%eax, %rdx
	movq	%rdx, %rax
	salq	$2, %rax
	addq	%rdx, %rax
	addq	%rax, %rax
	addq	%rsi, %rax
	movl	%ecx, -816(%rbp,%rax,4)
	jmp	.L9
.L8:
	movl	-8(%rbp), %eax
	movslq	%eax, %rcx
	movl	-4(%rbp), %eax
	movslq	%eax, %rdx
	movq	%rdx, %rax
	salq	$2, %rax
	addq	%rdx, %rax
	addq	%rax, %rax
	addq	%rcx, %rax
	movl	$0, -816(%rbp,%rax,4)
.L9:
	addl	$1, -8(%rbp)
.L7:
	movl	-820(%rbp), %eax
	cmpl	%eax, -8(%rbp)
	jl	.L10
	addl	$1, -4(%rbp)
.L6:
	movl	-820(%rbp), %eax
	cmpl	%eax, -4(%rbp)
	jl	.L11
	movl	$.LC3, %edi
	call	puts
	movl	$0, -4(%rbp)
	jmp	.L12
.L15:
	movl	$0, -8(%rbp)
	jmp	.L13
.L14:
	movl	-8(%rbp), %eax
	movslq	%eax, %rcx
	movl	-4(%rbp), %eax
	movslq	%eax, %rdx
	movq	%rdx, %rax
	salq	$2, %rax
	addq	%rdx, %rax
	addq	%rax, %rax
	addq	%rcx, %rax
	movl	-816(%rbp,%rax,4), %eax
	movl	%eax, %esi
	movl	$.LC4, %edi
	movl	$0, %eax
	call	printf
	addl	$1, -8(%rbp)
.L13:
	movl	-820(%rbp), %eax
	cmpl	%eax, -8(%rbp)
	jl	.L14
	movl	$10, %edi
	call	putchar
	addl	$1, -4(%rbp)
.L12:
	movl	-820(%rbp), %eax
	cmpl	%eax, -4(%rbp)
	jl	.L15
	movl	$0, %edi
	call	exit
	.cfi_endproc
.LFE2:
	.size	main, .-main
	.ident	"GCC: (Ubuntu 5.4.0-6ubuntu1~16.04.12) 5.4.0 20160609"
	.section	.note.GNU-stack,"",@progbits
