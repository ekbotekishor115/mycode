	.file	"thirtythree1.c"
	.section	.rodata
	.align 8
.LC0:
	.string	"Enter the order of the square matrix(max 10 by 10)\t"
.LC1:
	.string	"%d"
	.align 8
.LC2:
	.string	"Enter the elements of the matrix:"
.LC3:
	.string	"The matrix is symmetric"
.LC4:
	.string	"The matrix is not symmetric"
	.text
	.globl	main
	.type	main, @function
main:
.LFB2:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$832, %rsp
	movl	$0, -4(%rbp)
	movl	$.LC0, %edi
	movl	$0, %eax
	call	printf
	leaq	-820(%rbp), %rax
	movq	%rax, %rsi
	movl	$.LC1, %edi
	movl	$0, %eax
	call	__isoc99_scanf
	movl	$.LC2, %edi
	call	puts
	movl	$0, -8(%rbp)
	jmp	.L2
.L5:
	movl	$0, -12(%rbp)
	jmp	.L3
.L4:
	leaq	-416(%rbp), %rcx
	movl	-12(%rbp), %eax
	movslq	%eax, %rsi
	movl	-8(%rbp), %eax
	movslq	%eax, %rdx
	movq	%rdx, %rax
	salq	$2, %rax
	addq	%rdx, %rax
	addq	%rax, %rax
	addq	%rsi, %rax
	salq	$2, %rax
	addq	%rcx, %rax
	movq	%rax, %rsi
	movl	$.LC1, %edi
	movl	$0, %eax
	call	__isoc99_scanf
	addl	$1, -12(%rbp)
.L3:
	movl	-820(%rbp), %eax
	cmpl	%eax, -12(%rbp)
	jl	.L4
	addl	$1, -8(%rbp)
.L2:
	movl	-820(%rbp), %eax
	cmpl	%eax, -8(%rbp)
	jl	.L5
	movl	$0, -8(%rbp)
	jmp	.L6
.L9:
	movl	$0, -12(%rbp)
	jmp	.L7
.L8:
	movl	-8(%rbp), %eax
	movslq	%eax, %rcx
	movl	-12(%rbp), %eax
	movslq	%eax, %rdx
	movq	%rdx, %rax
	salq	$2, %rax
	addq	%rdx, %rax
	addq	%rax, %rax
	addq	%rcx, %rax
	movl	-416(%rbp,%rax,4), %ecx
	movl	-12(%rbp), %eax
	movslq	%eax, %rsi
	movl	-8(%rbp), %eax
	movslq	%eax, %rdx
	movq	%rdx, %rax
	salq	$2, %rax
	addq	%rdx, %rax
	addq	%rax, %rax
	addq	%rsi, %rax
	movl	%ecx, -816(%rbp,%rax,4)
	addl	$1, -12(%rbp)
.L7:
	movl	-820(%rbp), %eax
	cmpl	%eax, -12(%rbp)
	jl	.L8
	addl	$1, -8(%rbp)
.L6:
	movl	-820(%rbp), %eax
	cmpl	%eax, -8(%rbp)
	jl	.L9
	movl	$0, -8(%rbp)
	jmp	.L10
.L17:
	movl	$0, -12(%rbp)
	jmp	.L11
.L14:
	movl	-12(%rbp), %eax
	movslq	%eax, %rcx
	movl	-8(%rbp), %eax
	movslq	%eax, %rdx
	movq	%rdx, %rax
	salq	$2, %rax
	addq	%rdx, %rax
	addq	%rax, %rax
	addq	%rcx, %rax
	movl	-816(%rbp,%rax,4), %ecx
	movl	-12(%rbp), %eax
	movslq	%eax, %rsi
	movl	-8(%rbp), %eax
	movslq	%eax, %rdx
	movq	%rdx, %rax
	salq	$2, %rax
	addq	%rdx, %rax
	addq	%rax, %rax
	addq	%rsi, %rax
	movl	-416(%rbp,%rax,4), %eax
	cmpl	%eax, %ecx
	je	.L12
	movl	$1, -4(%rbp)
	jmp	.L13
.L12:
	addl	$1, -12(%rbp)
.L11:
	movl	-820(%rbp), %eax
	cmpl	%eax, -12(%rbp)
	jl	.L14
.L13:
	cmpl	$1, -4(%rbp)
	je	.L20
	addl	$1, -8(%rbp)
.L10:
	movl	-820(%rbp), %eax
	cmpl	%eax, -8(%rbp)
	jl	.L17
	jmp	.L16
.L20:
	nop
.L16:
	cmpl	$0, -4(%rbp)
	jne	.L18
	movl	$.LC3, %edi
	call	puts
	jmp	.L19
.L18:
	movl	$.LC4, %edi
	call	puts
.L19:
	movl	$0, %edi
	call	exit
	.cfi_endproc
.LFE2:
	.size	main, .-main
	.ident	"GCC: (Ubuntu 5.4.0-6ubuntu1~16.04.12) 5.4.0 20160609"
	.section	.note.GNU-stack,"",@progbits
