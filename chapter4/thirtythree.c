#include <stdio.h>
#include <stdlib.h>

int main()
{
	int matrix[10][10], trans_mat[10][10],unequal = 0;
	int i,j,order;
	printf("Enter the order of the square matrix(max 10 by 10)\t");
	scanf("%d",&order);
	printf("Enter the elements of the matrix:\n");
	for (i=0 ; i<order ; i++)
	{
		for (j=0 ; j<order ; j++)
			scanf("%d",&matrix[i][j]);
	}
	for(i=0 ; i<order ; i++)
	{
		for(j=0 ; j<order ; j++)
			trans_mat[i][j] = matrix[j][i];
	}
	for(i=0 ; i<order ; i++)
	{
		for(j=0 ; j<order ; j++)
		//	unequal=0;
			if(trans_mat[i][j] != matrix[i][j])
			{
				unequal = 1;
				break;
			}
		if(unequal == 1)
			break;
	}
	if(unequal == 0)
		printf("The matrix is symmetric\n");
	else
		printf("The matrix is not symmetric\n");
	exit(0);
}
