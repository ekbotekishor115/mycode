	.file	"thirtytwo1.c"
	.section	.rodata
	.align 8
.LC0:
	.string	"Enter the order of the matrix(max.10 by 10):\t"
.LC1:
	.string	"%d %d"
.LC2:
	.string	"Enter the elements of matrix:"
.LC3:
	.string	"%d"
.LC4:
	.string	"Elements of input matrix are:"
.LC5:
	.string	"%d\t"
.LC6:
	.string	"Transpose of the matrix is:"
	.text
	.globl	main
	.type	main, @function
main:
.LFB2:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$832, %rsp
	movl	$.LC0, %edi
	movl	$0, %eax
	call	printf
	leaq	-824(%rbp), %rdx
	leaq	-820(%rbp), %rax
	movq	%rax, %rsi
	movl	$.LC1, %edi
	movl	$0, %eax
	call	__isoc99_scanf
	movl	$.LC2, %edi
	call	puts
	movl	$0, -4(%rbp)
	jmp	.L2
.L5:
	movl	$0, -8(%rbp)
	jmp	.L3
.L4:
	leaq	-416(%rbp), %rcx
	movl	-8(%rbp), %eax
	movslq	%eax, %rsi
	movl	-4(%rbp), %eax
	movslq	%eax, %rdx
	movq	%rdx, %rax
	salq	$2, %rax
	addq	%rdx, %rax
	addq	%rax, %rax
	addq	%rsi, %rax
	salq	$2, %rax
	addq	%rcx, %rax
	movq	%rax, %rsi
	movl	$.LC3, %edi
	movl	$0, %eax
	call	__isoc99_scanf
	addl	$1, -8(%rbp)
.L3:
	movl	-824(%rbp), %eax
	cmpl	%eax, -8(%rbp)
	jl	.L4
	addl	$1, -4(%rbp)
.L2:
	movl	-820(%rbp), %eax
	cmpl	%eax, -4(%rbp)
	jl	.L5
	movl	$.LC4, %edi
	call	puts
	movl	$0, -4(%rbp)
	jmp	.L6
.L9:
	movl	$0, -8(%rbp)
	jmp	.L7
.L8:
	movl	-8(%rbp), %eax
	movslq	%eax, %rcx
	movl	-4(%rbp), %eax
	movslq	%eax, %rdx
	movq	%rdx, %rax
	salq	$2, %rax
	addq	%rdx, %rax
	addq	%rax, %rax
	addq	%rcx, %rax
	movl	-416(%rbp,%rax,4), %eax
	movl	%eax, %esi
	movl	$.LC5, %edi
	movl	$0, %eax
	call	printf
	addl	$1, -8(%rbp)
.L7:
	movl	-824(%rbp), %eax
	cmpl	%eax, -8(%rbp)
	jl	.L8
	movl	$10, %edi
	call	putchar
	addl	$1, -4(%rbp)
.L6:
	movl	-820(%rbp), %eax
	cmpl	%eax, -4(%rbp)
	jl	.L9
	movl	$0, -4(%rbp)
	jmp	.L10
.L13:
	movl	$0, -8(%rbp)
	jmp	.L11
.L12:
	movl	-4(%rbp), %eax
	movslq	%eax, %rcx
	movl	-8(%rbp), %eax
	movslq	%eax, %rdx
	movq	%rdx, %rax
	salq	$2, %rax
	addq	%rdx, %rax
	addq	%rax, %rax
	addq	%rcx, %rax
	movl	-416(%rbp,%rax,4), %ecx
	movl	-8(%rbp), %eax
	movslq	%eax, %rsi
	movl	-4(%rbp), %eax
	movslq	%eax, %rdx
	movq	%rdx, %rax
	salq	$2, %rax
	addq	%rdx, %rax
	addq	%rax, %rax
	addq	%rsi, %rax
	movl	%ecx, -816(%rbp,%rax,4)
	addl	$1, -8(%rbp)
.L11:
	movl	-820(%rbp), %eax
	cmpl	%eax, -8(%rbp)
	jl	.L12
	addl	$1, -4(%rbp)
.L10:
	movl	-824(%rbp), %eax
	cmpl	%eax, -4(%rbp)
	jl	.L13
	movl	$.LC6, %edi
	call	puts
	movl	$0, -4(%rbp)
	jmp	.L14
.L17:
	movl	$0, -8(%rbp)
	jmp	.L15
.L16:
	movl	-8(%rbp), %eax
	movslq	%eax, %rcx
	movl	-4(%rbp), %eax
	movslq	%eax, %rdx
	movq	%rdx, %rax
	salq	$2, %rax
	addq	%rdx, %rax
	addq	%rax, %rax
	addq	%rcx, %rax
	movl	-816(%rbp,%rax,4), %eax
	movl	%eax, %esi
	movl	$.LC5, %edi
	movl	$0, %eax
	call	printf
	addl	$1, -8(%rbp)
.L15:
	movl	-820(%rbp), %eax
	cmpl	%eax, -8(%rbp)
	jl	.L16
	movl	$10, %edi
	call	putchar
	addl	$1, -4(%rbp)
.L14:
	movl	-824(%rbp), %eax
	cmpl	%eax, -4(%rbp)
	jl	.L17
	movl	$0, %edi
	call	exit
	.cfi_endproc
.LFE2:
	.size	main, .-main
	.ident	"GCC: (Ubuntu 5.4.0-6ubuntu1~16.04.12) 5.4.0 20160609"
	.section	.note.GNU-stack,"",@progbits
