#include <stdio.h>
#include <stdlib.h>

int main()
{
	int val = 10;
	float *ptr =(float *) &val;
	printf("Value of variable is %d\n",val);
	printf("Pointer holds %p\n",ptr);
	exit(0);
}
