#include <stdio.h>
#include <stdlib.h>

int main()
{
	int arr1[] = {2.3, 4.5, 6.9};
	float arr2[] = {'A','B','C'};
	printf("Elements of array are initialized with\n");
	printf("arr1:%d%d%d\n",arr1[0],arr1[1],arr1[2]);
	printf("arr2:%f%f%f\n",arr2[0],arr2[1],arr2[2]);
	exit(0);
}
 //SEGMENTATION FAULT with tool-chain
