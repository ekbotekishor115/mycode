	.file	"thirtyfive1.c"
	.section	.rodata
	.align 8
.LC0:
	.string	"Enter order of the square matrix(max. 10 by 10)"
.LC1:
	.string	"%d"
	.align 8
.LC2:
	.string	"Enter the elements of the matrix:"
	.align 8
.LC3:
	.string	"The given matrix is not strictly upper triangular"
	.align 8
.LC4:
	.string	"The given matrix is strictly upper triangle"
	.text
	.globl	main
	.type	main, @function
main:
.LFB2:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$432, %rsp
	movl	$0, -4(%rbp)
	movl	$.LC0, %edi
	movl	$0, %eax
	call	printf
	leaq	-420(%rbp), %rax
	movq	%rax, %rsi
	movl	$.LC1, %edi
	movl	$0, %eax
	call	__isoc99_scanf
	movl	$.LC2, %edi
	call	puts
	movl	$0, -8(%rbp)
	jmp	.L2
.L5:
	movl	$0, -12(%rbp)
	jmp	.L3
.L4:
	leaq	-416(%rbp), %rcx
	movl	-12(%rbp), %eax
	movslq	%eax, %rsi
	movl	-8(%rbp), %eax
	movslq	%eax, %rdx
	movq	%rdx, %rax
	salq	$2, %rax
	addq	%rdx, %rax
	addq	%rax, %rax
	addq	%rsi, %rax
	salq	$2, %rax
	addq	%rcx, %rax
	movq	%rax, %rsi
	movl	$.LC1, %edi
	movl	$0, %eax
	call	__isoc99_scanf
	addl	$1, -12(%rbp)
.L3:
	movl	-420(%rbp), %eax
	cmpl	%eax, -12(%rbp)
	jl	.L4
	addl	$1, -8(%rbp)
.L2:
	movl	-420(%rbp), %eax
	cmpl	%eax, -8(%rbp)
	jl	.L5
	movl	$0, -8(%rbp)
	jmp	.L6
.L13:
	movl	$0, -12(%rbp)
	jmp	.L7
.L10:
	movl	-8(%rbp), %eax
	cmpl	-12(%rbp), %eax
	jl	.L8
	movl	-12(%rbp), %eax
	movslq	%eax, %rcx
	movl	-8(%rbp), %eax
	movslq	%eax, %rdx
	movq	%rdx, %rax
	salq	$2, %rax
	addq	%rdx, %rax
	addq	%rax, %rax
	addq	%rcx, %rax
	movl	-416(%rbp,%rax,4), %eax
	testl	%eax, %eax
	je	.L8
	movl	$1, -4(%rbp)
	jmp	.L9
.L8:
	addl	$1, -12(%rbp)
.L7:
	movl	-420(%rbp), %eax
	cmpl	%eax, -12(%rbp)
	jl	.L10
.L9:
	cmpl	$1, -4(%rbp)
	je	.L16
	addl	$1, -8(%rbp)
.L6:
	movl	-420(%rbp), %eax
	cmpl	%eax, -8(%rbp)
	jl	.L13
	jmp	.L12
.L16:
	nop
.L12:
	cmpl	$1, -4(%rbp)
	jne	.L14
	movl	$.LC3, %edi
	call	puts
	jmp	.L15
.L14:
	movl	$.LC4, %edi
	call	puts
.L15:
	movl	$0, %edi
	call	exit
	.cfi_endproc
.LFE2:
	.size	main, .-main
	.ident	"GCC: (Ubuntu 5.4.0-6ubuntu1~16.04.12) 5.4.0 20160609"
	.section	.note.GNU-stack,"",@progbits
