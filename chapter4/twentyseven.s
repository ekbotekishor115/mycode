	.file	"twentyseven1.c"
	.section	.rodata
	.align 8
.LC0:
	.string	"Enter the number of elements(max.20)\t"
.LC1:
	.string	"%d"
.LC2:
	.string	"Enter the elements:"
.LC3:
	.string	"After sorting, elements are:"
.LC4:
	.string	"%d\n"
	.text
	.globl	main
	.type	main, @function
main:
.LFB2:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$112, %rsp
	movl	$.LC0, %edi
	movl	$0, %eax
	call	printf
	leaq	-100(%rbp), %rax
	movq	%rax, %rsi
	movl	$.LC1, %edi
	movl	$0, %eax
	call	__isoc99_scanf
	movl	$.LC2, %edi
	call	puts
	movl	$0, -4(%rbp)
	jmp	.L2
.L3:
	leaq	-96(%rbp), %rax
	movl	-4(%rbp), %edx
	movslq	%edx, %rdx
	salq	$2, %rdx
	addq	%rdx, %rax
	movq	%rax, %rsi
	movl	$.LC1, %edi
	movl	$0, %eax
	call	__isoc99_scanf
	addl	$1, -4(%rbp)
.L2:
	movl	-100(%rbp), %eax
	cmpl	%eax, -4(%rbp)
	jl	.L3
	movl	$0, -4(%rbp)
	jmp	.L4
.L8:
	movl	$0, -8(%rbp)
	jmp	.L5
.L7:
	movl	-8(%rbp), %eax
	cltq
	movl	-96(%rbp,%rax,4), %edx
	movl	-8(%rbp), %eax
	addl	$1, %eax
	cltq
	movl	-96(%rbp,%rax,4), %eax
	cmpl	%eax, %edx
	jle	.L6
	movl	-8(%rbp), %eax
	cltq
	movl	-96(%rbp,%rax,4), %eax
	movl	%eax, -12(%rbp)
	movl	-8(%rbp), %eax
	addl	$1, %eax
	cltq
	movl	-96(%rbp,%rax,4), %edx
	movl	-8(%rbp), %eax
	cltq
	movl	%edx, -96(%rbp,%rax,4)
	movl	-8(%rbp), %eax
	addl	$1, %eax
	cltq
	movl	-12(%rbp), %edx
	movl	%edx, -96(%rbp,%rax,4)
.L6:
	addl	$1, -8(%rbp)
.L5:
	movl	-100(%rbp), %eax
	subl	$1, %eax
	subl	-4(%rbp), %eax
	cmpl	-8(%rbp), %eax
	jg	.L7
	addl	$1, -4(%rbp)
.L4:
	movl	-100(%rbp), %eax
	subl	$1, %eax
	cmpl	-4(%rbp), %eax
	jg	.L8
	movl	$.LC3, %edi
	call	puts
	movl	$0, -4(%rbp)
	jmp	.L9
.L10:
	movl	-4(%rbp), %eax
	cltq
	movl	-96(%rbp,%rax,4), %eax
	movl	%eax, %esi
	movl	$.LC4, %edi
	movl	$0, %eax
	call	printf
	addl	$1, -4(%rbp)
.L9:
	movl	-100(%rbp), %eax
	cmpl	%eax, -4(%rbp)
	jl	.L10
	movl	$0, %edi
	call	exit
	.cfi_endproc
.LFE2:
	.size	main, .-main
	.ident	"GCC: (Ubuntu 5.4.0-6ubuntu1~16.04.12) 5.4.0 20160609"
	.section	.note.GNU-stack,"",@progbits
