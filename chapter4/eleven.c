#include <stdio.h>
#include <stdlib.h>

int main()
{
	int a=10;
	int *iptr = &a;
	void *vptr = iptr;
	printf("int* is Implicitly converted to void*\n");
	exit(0);
}
