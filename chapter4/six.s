	.file	"six1.c"
	.section	.rodata
.LC0:
	.string	"%d %d %d\n"
.LC1:
	.string	"Arrays are equal"
.LC2:
	.string	"Arrays are not equal"
	.text
	.globl	main
	.type	main, @function
main:
.LFB2:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$32, %rsp
	movl	$10, -16(%rbp)
	movl	$20, -12(%rbp)
	movl	$30, -8(%rbp)
	movl	$10, -32(%rbp)
	movl	$20, -28(%rbp)
	movl	$30, -24(%rbp)
	movl	-8(%rbp), %ecx
	movl	-12(%rbp), %edx
	movl	-16(%rbp), %eax
	movl	%eax, %esi
	movl	$.LC0, %edi
	movl	$0, %eax
	call	printf
	movl	-16(%rbp), %edx
	movl	-32(%rbp), %eax
	cmpl	%eax, %edx
	jne	.L2
	movl	-12(%rbp), %edx
	movl	-28(%rbp), %eax
	cmpl	%eax, %edx
	jne	.L2
	movl	-8(%rbp), %edx
	movl	-24(%rbp), %eax
	cmpl	%eax, %edx
	jne	.L2
	movl	$.LC1, %edi
	call	puts
	jmp	.L3
.L2:
	movl	$.LC2, %edi
	call	puts
.L3:
	movl	$0, %edi
	call	exit
	.cfi_endproc
.LFE2:
	.size	main, .-main
	.ident	"GCC: (Ubuntu 5.4.0-6ubuntu1~16.04.12) 5.4.0 20160609"
	.section	.note.GNU-stack,"",@progbits
