//Bubble sort
#include <stdio.h>
#include <stdlib.h>

int main()
{
	int list[20],num,min,tmp,i,j;
	printf("Enter the number of elements(max.20)\t");
	scanf("%d",&num);
	printf("Enter the elements:\n");
	for(i=0 ; i<num ; i++)
		scanf("%d",&list[i]);
	for(i=0 ; i<num-1 ; i++)	
		for(j=0 ; j<num-1-i ;j++)
			if(list[j] > list[j+1])
			{
				tmp = list[j];
				list[j] = list[j+1];
				list[j+1] = tmp;
			}
	printf("After sorting, elements are:\n");
	for(i=0 ; i<num ; i++)
		printf("%d\n",list[i]);
	exit(0);
}
