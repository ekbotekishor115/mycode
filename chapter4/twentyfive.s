	.file	"twentyfive1.c"
	.section	.rodata
	.align 8
.LC0:
	.string	"Enter the number of elements(max.20)\t"
.LC1:
	.string	"%d"
.LC2:
	.string	"Enter the elements:"
.LC3:
	.string	"elements in list %d\n"
.LC4:
	.string	"%d is current\n"
.LC5:
	.string	"After sorting, elements are:"
.LC6:
	.string	"%d\n"
	.text
	.globl	main
	.type	main, @function
main:
.LFB2:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$112, %rsp
	movl	$.LC0, %edi
	movl	$0, %eax
	call	printf
	leaq	-100(%rbp), %rax
	movq	%rax, %rsi
	movl	$.LC1, %edi
	movl	$0, %eax
	call	__isoc99_scanf
	movl	$.LC2, %edi
	call	puts
	movl	$0, -4(%rbp)
	jmp	.L2
.L3:
	leaq	-96(%rbp), %rax
	movl	-4(%rbp), %edx
	movslq	%edx, %rdx
	salq	$2, %rdx
	addq	%rdx, %rax
	movq	%rax, %rsi
	movl	$.LC1, %edi
	movl	$0, %eax
	call	__isoc99_scanf
	addl	$1, -4(%rbp)
.L2:
	movl	-100(%rbp), %eax
	cmpl	%eax, -4(%rbp)
	jl	.L3
	movl	-4(%rbp), %eax
	cltq
	movl	-96(%rbp,%rax,4), %eax
	movl	%eax, %esi
	movl	$.LC3, %edi
	movl	$0, %eax
	call	printf
	movl	$1, -4(%rbp)
	jmp	.L4
.L9:
	movl	-4(%rbp), %eax
	cltq
	movl	-96(%rbp,%rax,4), %edx
	movl	-4(%rbp), %eax
	subl	$1, %eax
	cltq
	movl	-96(%rbp,%rax,4), %eax
	cmpl	%eax, %edx
	jge	.L5
	movl	-4(%rbp), %eax
	cltq
	movl	-96(%rbp,%rax,4), %eax
	movl	%eax, -12(%rbp)
	movl	-12(%rbp), %eax
	movl	%eax, %esi
	movl	$.LC4, %edi
	movl	$0, %eax
	call	printf
	movl	-4(%rbp), %eax
	subl	$1, %eax
	movl	%eax, -8(%rbp)
	jmp	.L6
.L8:
	movl	-8(%rbp), %eax
	leal	1(%rax), %ecx
	movl	-8(%rbp), %eax
	cltq
	movl	-96(%rbp,%rax,4), %edx
	movslq	%ecx, %rax
	movl	%edx, -96(%rbp,%rax,4)
	cmpl	$0, -8(%rbp)
	je	.L7
	movl	-8(%rbp), %eax
	subl	$1, %eax
	cltq
	movl	-96(%rbp,%rax,4), %eax
	cmpl	-12(%rbp), %eax
	jle	.L7
	subl	$1, -8(%rbp)
.L6:
	cmpl	$0, -8(%rbp)
	jns	.L8
.L7:
	movl	-8(%rbp), %eax
	cltq
	movl	-12(%rbp), %edx
	movl	%edx, -96(%rbp,%rax,4)
.L5:
	addl	$1, -4(%rbp)
.L4:
	movl	-100(%rbp), %eax
	cmpl	%eax, -4(%rbp)
	jl	.L9
	movl	$.LC5, %edi
	call	puts
	movl	$0, -4(%rbp)
	jmp	.L10
.L11:
	movl	-4(%rbp), %eax
	cltq
	movl	-96(%rbp,%rax,4), %eax
	movl	%eax, %esi
	movl	$.LC6, %edi
	movl	$0, %eax
	call	printf
	addl	$1, -4(%rbp)
.L10:
	movl	-100(%rbp), %eax
	cmpl	%eax, -4(%rbp)
	jl	.L11
	movl	$0, %edi
	call	exit
	.cfi_endproc
.LFE2:
	.size	main, .-main
	.ident	"GCC: (Ubuntu 5.4.0-6ubuntu1~16.04.12) 5.4.0 20160609"
	.section	.note.GNU-stack,"",@progbits
