#include <stdio.h>
#include <stdlib.h>

int main()
{
	int list[20],num,current,i,j;
	printf("Enter the number of elements(max.20)\t");
	scanf("%d",&num);
	printf("Enter the elements:\n");
	for (i=0 ; i<num ; i++)
		scanf("%d",&list[i]);
		printf("elements in list %d\n",list[i]);
	for(i=1 ; i<num ; i++)
		if(list[i] < list[i-1])
		{
			current = list[i];
			printf("%d is current\n",current);
			for(j=i-1 ; j>=0 ; j--)
			{
				list[j+1] = list[j];
				if(j==0 || list[j-1]<=current)
					break;
			}
			list[j] = current;
		}
	printf("After sorting, elements are:\n");
	for(i=0 ; i<num; i++)
		printf("%d\n", list[i]);
	exit(0);
}
