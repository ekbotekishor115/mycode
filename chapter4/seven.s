	.file	"seven1.c"
	.section	.rodata
	.align 8
.LC0:
	.string	"Pointer to character takes %d bytes\n"
	.align 8
.LC1:
	.string	"Pointer to integer takes %d bytes\n"
	.align 8
.LC2:
	.string	"Pointer to float takes %d bytes\n"
	.text
	.globl	main
	.type	main, @function
main:
.LFB2:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	movl	$8, %esi
	movl	$.LC0, %edi
	movl	$0, %eax
	call	printf
	movl	$8, %esi
	movl	$.LC1, %edi
	movl	$0, %eax
	call	printf
	movl	$8, %esi
	movl	$.LC2, %edi
	movl	$0, %eax
	call	printf
	movl	$0, %edi
	call	exit
	.cfi_endproc
.LFE2:
	.size	main, .-main
	.ident	"GCC: (Ubuntu 5.4.0-6ubuntu1~16.04.12) 5.4.0 20160609"
	.section	.note.GNU-stack,"",@progbits
