#include <stdio.h>
#include <stdlib.h>

int main()
{
	int list[20],num,min,tmp,i,j;
	printf("Enter the number of elements(max.20)\t");
	scanf("%d",&num);
	printf("Enter the elements:\n");
	for(i=0 ; i<num ; i++)
		scanf("%d",&list[i]);
	for(i=0 ; i<num ; i++)
	{
		min=i;
		for(j=i+1 ; j<num ; j++)
			if(list[j] < list[min])
				min=j;
		{
			tmp = list[min];
			list[min] = list[i];
			list[i] = tmp;
		}
	}
	printf("After sorting, elements are:\n");
	for(i=0 ; i<num ; i++)
		printf("%d\n",list[i]);
	exit(0);
}
