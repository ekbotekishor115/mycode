	.file	"twentythree1.c"
	.section	.rodata
	.align 8
.LC1:
	.string	"Enter the number of elements(max.20)\t"
.LC2:
	.string	"%d"
.LC3:
	.string	"Enter the elements:"
.LC4:
	.string	"%f"
.LC5:
	.string	"Arithmetic mean is %f\n"
.LC6:
	.string	"Variance is %f\n"
.LC7:
	.string	"Standard Deviation is %f\n"
	.text
	.globl	main
	.type	main, @function
main:
.LFB2:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	addq	$-128, %rsp
	pxor	%xmm0, %xmm0
	movss	%xmm0, -4(%rbp)
	movl	$.LC1, %edi
	movl	$0, %eax
	call	printf
	leaq	-116(%rbp), %rax
	movq	%rax, %rsi
	movl	$.LC2, %edi
	movl	$0, %eax
	call	__isoc99_scanf
	movl	$.LC3, %edi
	call	puts
	movl	$0, -8(%rbp)
	jmp	.L2
.L3:
	leaq	-112(%rbp), %rax
	movl	-8(%rbp), %edx
	movslq	%edx, %rdx
	salq	$2, %rdx
	addq	%rdx, %rax
	movq	%rax, %rsi
	movl	$.LC4, %edi
	movl	$0, %eax
	call	__isoc99_scanf
	addl	$1, -8(%rbp)
.L2:
	movl	-116(%rbp), %eax
	cmpl	%eax, -8(%rbp)
	jl	.L3
	movl	$0, -8(%rbp)
	jmp	.L4
.L5:
	movl	-8(%rbp), %eax
	cltq
	movss	-112(%rbp,%rax,4), %xmm0
	movss	-4(%rbp), %xmm1
	addss	%xmm1, %xmm0
	movss	%xmm0, -4(%rbp)
	addl	$1, -8(%rbp)
.L4:
	movl	-116(%rbp), %eax
	cmpl	%eax, -8(%rbp)
	jl	.L5
	movl	-116(%rbp), %eax
	pxor	%xmm0, %xmm0
	cvtsi2ss	%eax, %xmm0
	movss	-4(%rbp), %xmm1
	divss	%xmm0, %xmm1
	movaps	%xmm1, %xmm0
	movss	%xmm0, -12(%rbp)
	pxor	%xmm0, %xmm0
	movss	%xmm0, -4(%rbp)
	movl	$0, -8(%rbp)
	jmp	.L6
.L7:
	movl	-8(%rbp), %eax
	cltq
	movss	-112(%rbp,%rax,4), %xmm0
	movaps	%xmm0, %xmm1
	subss	-12(%rbp), %xmm1
	movl	-8(%rbp), %eax
	cltq
	movss	-112(%rbp,%rax,4), %xmm0
	subss	-12(%rbp), %xmm0
	mulss	%xmm1, %xmm0
	movss	-4(%rbp), %xmm1
	addss	%xmm1, %xmm0
	movss	%xmm0, -4(%rbp)
	addl	$1, -8(%rbp)
.L6:
	movl	-116(%rbp), %eax
	cmpl	%eax, -8(%rbp)
	jl	.L7
	movl	-116(%rbp), %eax
	pxor	%xmm0, %xmm0
	cvtsi2ss	%eax, %xmm0
	movss	-4(%rbp), %xmm1
	divss	%xmm0, %xmm1
	movaps	%xmm1, %xmm0
	movss	%xmm0, -16(%rbp)
	cvtss2sd	-16(%rbp), %xmm0
	call	sqrt
	cvtsd2ss	%xmm0, %xmm2
	movss	%xmm2, -20(%rbp)
	cvtss2sd	-12(%rbp), %xmm0
	movl	$.LC5, %edi
	movl	$1, %eax
	call	printf
	cvtss2sd	-16(%rbp), %xmm0
	movl	$.LC6, %edi
	movl	$1, %eax
	call	printf
	cvtss2sd	-20(%rbp), %xmm0
	movl	$.LC7, %edi
	movl	$1, %eax
	call	printf
	movl	$0, %edi
	call	exit
	.cfi_endproc
.LFE2:
	.size	main, .-main
	.ident	"GCC: (Ubuntu 5.4.0-6ubuntu1~16.04.12) 5.4.0 20160609"
	.section	.note.GNU-stack,"",@progbits
