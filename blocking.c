#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <unistd.h>

void sig_handler(int sig)
{
	printf("In sig catcher\n");
}

int main()
{
	sigset_t blockSet,prevMask;
	/*Initialize a signal set to contain SIGNET*/
	sigemptyset(&blockSet);
	sigaddset(&blockSet, SIGINT);
	/*Block SIGINT, save previous signal mask*/
	if(sigprocmask(SIG_BLOCK, &blockSet, &prevMask) == -1)
		perror("sigprocmask1");
	signal(SIGINT, sig_handler);
	while(1)
	{
		usleep(500);
	}
	/*... Code that should not be interrupted by SIGINT ...*/
	/*Restore previous signal mask, unblocking SIGINT*/
	if(sigprocmask(SIG_SETMASK, &prevMask, NULL) == -1)
		perror("sigprocmask2");
	return 0;
}
	
