#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <stdbool.h>
#include <unistd.h>
#include <sys/types.h>

bool is_run = true;
int r;
void sig_handler(int signum)
{
	is_run = false;
	printf("signal catched %d\n",signum);
}
int main()
{
	signal(SIGINT,sig_handler);
	while(is_run)
	{
		usleep(5000000);
//		sig_handler;
		r = raise(SIGINT);
	}
	return 0;
}
