#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/mman.h>


static void usageError(const char *progName)
{
	fprintf(stderr,"Usag: %s [-cx] name size [octal-perms]\n",progName);
	fprintf(stderr,"	-c	Create shared memory (O_CREAT)\n");
	fprintf(stderr,"	-x	Create exclusively (O_EXCL)\n");
	exit(EXIT_FAILURE);
}

int main(int argc, char **argv)
{
	int flags, opt, fd;
	mode_t perms;
	size_t size;
	void *addr;

	flags = O_RDWR;
	while ((opt = getopt(argc,argv,"cx")) != -1)
	{
		switch (opt)
		{
			case 'c':
				flags |= O_CREAT;
				break;
			case 'x':
				flags |= O_EXCL;
				break;
			default:
				usageError(argv[0]);
		}
	}

	if (optind + 1 >= argc)
	{
		usageError(argv[0]);
	}

	size = atol(argv[optind + 1]);
	perms = (S_IRUSR | S_IWUSR) ;

	printf("%s\n",argv[optind]);
	fd =shm_open(argv[optind],flags,perms);
	if (fd == -1)
	{
		perror("shm_open");
		exit(EXIT_FAILURE);
	}

	if (ftruncate(fd, size) == -1)
	{
		perror("ftruncate");
		exit(EXIT_FAILURE);
	}

	addr = mmap(NULL, size, PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0);
	if (addr == MAP_FAILED)
	{
		perror("mmap");
		exit(EXIT_FAILURE);
	}

	exit(EXIT_SUCCESS);
}
