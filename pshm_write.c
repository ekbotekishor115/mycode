#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/mman.h>

int main(int argc,char **argv)
{
	int fd;
	size_t len;
	char *addr;

	if (argc != 3 || strcmp(argv[1],"--help") == 0)
	{
		printf("%s shm-name string\n",argv[0]);
		exit(EXIT_FAILURE);
	}

	fd = shm_open(argv[1],O_RDWR,0);
	if (fd == -1)
	{
		perror("shm_open");
		exit(EXIT_FAILURE);
	}

	len = strlen(argv[2]);
	if (ftruncate(fd,len) == -1)
	{
		perror("ftruncate");
		exit(EXIT_FAILURE);
	}

	printf("Resized to %ld bytes\n",(long) len);

	addr = mmap(NULL, len, PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0);
	if (addr == MAP_FAILED)
	{
		perror("mmap");
		exit(EXIT_FAILURE);
	}

	if (close(fd) == -1)
	{
		perror("close");
		exit(EXIT_FAILURE);
	}

	printf("copying %ld bytes\n",(long) len);
	memcpy(addr, argv[2], len);
	exit(EXIT_SUCCESS);
}
