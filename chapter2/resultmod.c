#include <stdio.h>
#include <stdlib.h>

int main()
{
	printf("Sign of the result of the modulus operator:\n");
	printf("4%%3=%d, -4%%3=%d\n",4%3,-4%3);
	printf("4%%-3=%d, -4%%-3=%d\n",4%-3,-4%-3);
	exit(0);
}
