#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <stdbool.h>

//void getAsmExecutable(char *);
void hyphen_s(char *, char *);
void hyphen_c(char *, char *);
void no_option(char *,char *);

int main(int argc, char **argv)
{
	int ret_opt;
	char *opt_asm, *opt_obj, *opt_exe;
	bool opt_S = false, opt_c = false, opt_o = false;
	if (argc == 2)	
	{
		//call noOption()
		no_option(argv[1],NULL);
		exit(EXIT_FAILURE);
	}
	else 
	{
		while ((ret_opt = getopt(argc,argv,"Sco:")) != -1)
		{
			printf("%d, %s\n",ret_opt,optarg);
			switch (ret_opt)
			{
				case 'S':
					printf("%s\n",argv[optind]);
					opt_S = true;
					break;
					// .S file will be created
				case 'c':
					opt_c = true;
					break;
					// .o file is created and .S file is deleted
				case 'o':
					opt_o = true;
					opt_exe = malloc(strlen(optarg) + 1);
					strcpy(opt_exe,optarg);
					break;
					// creates customized executable name
				default :
					// creates output as 'a.out'
					break;
			}
		}
	}
	char *input_file = malloc(strlen(argv[optind]) + 1);
	strcpy(input_file, argv[optind]);
	if (opt_S == true )
	{
		printf("For .S file generation\n");
//		printf("argv[optind] = %s\n",argv[optind]);
		if (opt_o == true)
		{
			//pass -o as an option with -S
			printf("input file name = %s\n",input_file);
			printf("option = %s\n",opt_exe);
			hyphen_s(input_file,opt_exe);
		}
		else
		{
			//pass only -S as an option
//			printf("option = %s\n",opt_exe);
			hyphen_s(input_file,NULL);
		}
//		getAsmExecutable(argv[optind]);
		printf("completed!!\n");
	}
	else if (opt_c == true)
	{
		if (opt_o == true)
		{
			//pass -o as an option with -c
			hyphen_c(input_file,opt_exe);
		}
		else
		{
			//pass -c as an option
			printf("passing only -c\n");
			hyphen_c(input_file, NULL);
		}
	}
	else
	{
		if (opt_o == true)
		{
			//create customize exe file
			no_option(input_file,opt_exe);
		}
	}
	
	printf("end of file\n");
}

void getAsmExecutable(char *cmdln_arg)
{
	pid_t ret_fork;
	int exec;
	printf("cmd_line arg = %s\n",(char *)cmdln_arg);	
	switch (ret_fork = fork())
	{
		case -1:
			perror("fork()");
		case 0:
			exec = execlp("/usr/bin/gcc","gcc","-S",(char *)cmdln_arg,NULL);
			if (exec == -1)
			{
				perror("execlp");
			}
			_exit(EXIT_SUCCESS);
		default:
			printf("pid=%ld\n",(long)getpid());
			exit(EXIT_SUCCESS);
	}
}
