#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <libgen.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <time.h>
#include <stdbool.h>

bool ulink = false;

void my_system(char *);

void hyphen_s(char *, char *);
void write_logs(bool,char *,char *,char *);
char *get_timestamp(bool ,char *,char *);

void hyphen_s(char *file,char *option)
{
	char command[500] = {0};
	printf("In hyphen_S class...\n");
	char target_file[100] = {0};

	if (option == NULL)
	{
		char *base_file_name = basename(file);
		strcpy(target_file,base_file_name);
		target_file[strlen(target_file) - 1] = 's';
	}
	else
	{
		strcpy(target_file,option);
	}

	strcat(command,"gcc -S -o ");
	strcat(command,target_file);
	strcat(command," ");
	strcat(command,file);
	my_system(command);
	write_logs(ulink = false,target_file,file," file Created\n");
}

// gcc -S -o file.S file.c

void hyphen_c(char *file,char *option)
{
//create .S file
//create .o file
//delete .S file
// if option is -o , create customized executable
	char target_file[100] = {0};
	
	if (option == NULL)	
	{
		//take base name of file
		char *base_file_name = basename(file);
		strcpy(target_file,base_file_name);
		target_file[strlen(target_file) - 1] = 'o';
	}
	else
	{
		strcpy(target_file,option);
		
	}
	printf("Entering into -c\n");
	char gcc_s[200] = {0};
	strcat(gcc_s,"gcc -S -o ");
	printf("gcc cmd preparation = %s\n",gcc_s);
	printf("input file = %s\n",file);

	char *base_name;

	char *as_file = malloc(strlen(file));
	strcpy(as_file,file);
	base_name = basename(as_file);
	printf("baseName of file is %s\n",base_name);
	
	int len = strlen(base_name);
	base_name[len - 1] = 's';
	printf("converted baseName is %s\n",base_name);

	strcat(gcc_s,base_name);
	strcat(gcc_s," ");
	strcat(gcc_s,file);
	my_system(gcc_s);
	write_logs(ulink = false,base_name,file," FILE CREATED.\n");

	//create command for .o file using target_file
	char command[500] = "as -o ";
	strcat(command,target_file);
	strcat(command," ");
	strcat(command,base_name);	
	printf("command = %s\n",command);

	my_system(command);
	write_logs(ulink = false,target_file,file," FILE CREATED.\n");
	char del_file[50] = {0};
	strcpy(del_file,base_name);
	unlink(base_name);
	write_logs(ulink = true,del_file,file," FILE DELETED.\n");
}

void no_option(char *file,char *option)
{
	char target_file[100] = {0};

	if (option == NULL)
	{
		char *creat_target = malloc(strlen(file));
		strcpy(creat_target,file);
		char *base_file_name = basename(creat_target);
		
//		for (int i = 1; i < 3; i++)
//		{
			base_file_name[strlen(base_file_name) - 2] = '\0';
//		}
//		base_file_name[strlen(base_file_name) - 2] = ' ';
		strcpy(target_file,base_file_name);
//		target_file[strlen(target_file) - 1] = 
		printf("target file = %s\n",target_file);
	}
	else
	{
		strcpy(target_file,option);
	}
	
	char gcc_s[500] = {0};
	strcat(gcc_s,"gcc -S -o ");
	char *asm_file = malloc(strlen(file));
	strcpy(asm_file,file);
	printf("src_file = %s\n",asm_file);	
	char *base_name_asm;
	base_name_asm = basename(asm_file);
	base_name_asm[strlen(base_name_asm) - 1] = 's';
	strcat(gcc_s,base_name_asm);
	strcat(gcc_s," ");
	strcat(gcc_s,file);
	printf("command = %s\n",gcc_s);
	my_system(gcc_s);
	write_logs(ulink =false,base_name_asm,file," FILE CREATED.\n");

	char cmd_asm[200] = {0};
	strcat(cmd_asm,"as -o ");
	char * obj_file = malloc(strlen(file));
	strcpy(obj_file,file);

	char *base_name_obj;
	base_name_obj = basename(obj_file);
	base_name_obj[strlen(base_name_obj) - 1] = 'o';
	strcat(cmd_asm,base_name_obj);
	strcat(cmd_asm," ");
	strcat(cmd_asm,base_name_asm);
	printf("command for .o file = %s\n",cmd_asm);
	my_system(cmd_asm);
	write_logs(ulink = false,base_name_obj,file," FILE CREATED.\n");

	char cmd_executable[300] = {0};
	strcat(cmd_executable,"ld ");
	printf("cmd_executable = %s\n",cmd_executable);	
/*	if (option != NULL)
	{*/
	strcat(cmd_executable,"-o ");
//		char *exec_file = malloc(strlen(file));
//		strcpy(exec_file,file);
//		char *base_name_exec = basename(exec_file);
	strcat(cmd_executable,target_file);
	strcat(cmd_executable," -lc -dynamic-linker /lib64/ld-linux-x86-64.so.2 ");
	strcat(cmd_executable,base_name_obj);
	strcat(cmd_executable," -e main");
		
/*		
	}
	else
	{
		strcat(cmd_executable," -lc -dynamic-linker /lib64/ld-linux-x86-64.so.2 ");
		strcat(cmd_executable,base_name_obj);
		strcat(cmd_executable," -e main");
	}
*/
	printf("ld command = %s\n",cmd_executable);
		
	my_system(cmd_executable);
	write_logs(ulink = false,target_file,file," EXECUTABLE FILE CREATED.\n");
	char del_file1[50] = {0};
	strcpy(del_file1,base_name_asm);
	unlink(base_name_asm);
	write_logs(ulink = true,del_file1,file," FILE DELETED.\n");
	char del_file2[50] = {0};
	strcpy(del_file2,base_name_obj);
	unlink(base_name_obj);
	write_logs(ulink = true,del_file2,file," FILE DELETED.\n");
}

void write_logs(bool ulink,char *path,char *file,char *log)
{
	char time_stmp[200] = {0};

	char *tm_stmp = get_timestamp(ulink,file,path);

	tm_stmp[strlen(tm_stmp) - 1] = ' ';

	strcat(time_stmp,tm_stmp);
	strcat(time_stmp," ");
	strcat(time_stmp,path);
	strcat(time_stmp,log);
	
	int log_fd = open("build.log",O_CREAT | O_RDWR ,S_IRUSR | S_IWUSR | S_IRGRP | S_IWGRP | S_IROTH | S_IWOTH);
	if (log_fd < 0)
	{
		perror("open Log file");
		exit(EXIT_FAILURE);
	}

	off_t offset = lseek(log_fd,0,SEEK_END);
	if (offset < 0)
	{
		perror("lseek");
		exit(EXIT_FAILURE);
	}

	ssize_t wr_log = write(log_fd,time_stmp,strlen(time_stmp));
	if (wr_log < 0)
	{
		perror("Log write Failed");
		close(log_fd);
		exit(EXIT_FAILURE);
	}

//	close(log_fd);
	
}

char *get_timestamp(bool ulink,char *file,char *path)
{
	struct stat st, st_src;
        struct tm *gmt;
	time_t currtime ;
        char *time_stmp;

	printf("path for timestamp = %s\n",path);

	if (ulink == true)
        {
//              perror("stat");
//              exit(EXIT_FAILURE);
//              gmt = localtime(&st.st_ctime);
                currtime = time(NULL);
                if (currtime == -1)
                {
                        perror("time");
                        exit(EXIT_FAILURE);
                }
		
		gmt = localtime(&currtime);
		if (gmt == NULL)
		{
			perror("localtime");
			exit(EXIT_FAILURE);
		}

		time_stmp = asctime(gmt);
		if (time_stmp == NULL)
		{
			perror("asctime");
			exit(EXIT_FAILURE);
		}
        }
        else
        {
		int ret = stat(path,&st);
		if (ret < 0)
		{
			perror("stat");
			exit(EXIT_FAILURE);
		}

		//if modification time of source file is greater than modification time of target file
		//then rebuild the source file
		int src_ret = stat(file,&st_src);
		if (src_ret < 0)
		{
			perror("stat");
			exit(EXIT_FAILURE);
		}
		
		if (st_src.st_mtime > st.st_mtime)
		{
			if (path[strlen(path) - 1] == 's')
			{
				hyphen_s(file,path);
			}
			else if (path[strlen(path) - 1] == 'o')
			{
				hyphen_c(file,path);
			}
			else
			{
				no_option(file,path);
			}
		}


                gmt = gmtime(&st.st_ctime);
                if (gmt == NULL)
                {
                        perror("gmtime");
                        exit(EXIT_FAILURE);
                }

		time_stmp = asctime(gmt);
		if (time_stmp == NULL)
		{
			perror("asctime");
			exit(EXIT_FAILURE);
		}
        }

//      printf(" %d %d:%d\n",gmt->tm_mday,gmt->tm_hour,gmt->tm_min);

//        sprintf(time_stmp,"%d %d:%d:%d",gmt->tm_mday,gmt->tm_hour,gmt->tm_min,gmt->tm_sec);

/*      strcat(time_stmp,gmt->tm_mday);
        strcat(time_stmp," ");
        strcat(time_stmp,gmt->tm_hour);
        strcat(time_stmp,":");
        strcat(time_stmp,gmt->tm_min);
        strcat(time_stmp,":");
        strcat(time_stmp,gmt->tm_sec);
        printf("%s\n",time_stmp);
*/
  //      strcat(time_stmp,log);
        printf("%s\n",time_stmp);
	return time_stmp;	
}
