#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <unistd.h>
#include <errno.h>
#include <sys/types.h>
#include <time.h>

#define SYNC_SIG SIGUSR1

static void handler(int sig)
{
	
}

int main(int argc,char**argv)
{
	pid_t childPid;
	sigset_t blockMask, origMask, emptyMask;
	struct sigaction sa;

	setbuf (stdout,NULL);

	sigemptyset(&blockMask);
	sigaddset(&blockMask,SYNC_SIG);
	if(sigprocmask(SIG_BLOCK,&blockMask,&origMask) == -1)
		perror("sigprocmask");
	
	sigemptyset(&sa.sa_mask);
	sa.sa_flags = SA_RESTART;
	sa.sa_handler = handler;
	if(sigaction(SYNC_SIG,&sa,NULL) == -1)
		perror("sigaction");

	switch(childPid = fork())
	{
		case -1 :
			perror("fork");
		case 0 :
			printf("[%s %ld] Child started - doing some work\n",curTime("%T"),(long)getpid());
			sleep(2);
			printf("[%s %ld] Child about to signal parent\n",curTime("%T"),(long)getpid());
			if(kill(getpid(),SYNC_SIG) == -1)
				perror("kill");
			_exit(EXIT_SUCCESS);
		default:
			printf("[%s %ld] Parent about to wait for signal\n",curTime("%T"),(long)getpid());
			sigemptyset(&emptyMask);
			if(sigsuspend(&emptyMask) == -1 && errno != EINTR)
				perror("sigsuspend");
			printf("[%s %ld] Parent got signal\n",curTime("%T"),(long)getpid());
			if(sigprocmask(SIG_SETMASK, &origMask,NULL) == -1)
				perror("sigprocmask");
			exit(EXIT_SUCCESS);
	}
}
