#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>

static void *thread_fun(void *arg);
int main()
{
	pthread_t t1,t2;
	char buffer[100] = {0};
//	char buffer[50];
	void * res;
	int s1,s2;
	s1 = pthread_create(&t1,NULL,thread_fun,"Hello World!");
	s2 = pthread_create(&t2,NULL,thread_fun,"Hello Universe!!");

	if (s1 != 0)
	{
		//
	}
	if (s2 != 0)
	{
		//
	}
//	sprintf(buffer,"Message from main()\n");
	ssize_t n = write(STDOUT_FILENO, "Message from main()\n", strlen("Message from main()\n"));

	s1 = pthread_join(t2,&res);
	s1 = pthread_join(t1,&res);
	sprintf(buffer,"thread1 ret val = %ld\n",(long)res);
	ssize_t m = write(STDOUT_FILENO,buffer,strlen(buffer));

	memset(buffer,'0',strlen(buffer));
	s1 = pthread_join(t2,&res);
	sprintf(buffer,"thread2 ret val = %ld\n",(long)res);
	ssize_t o= write(STDOUT_FILENO,buffer,strlen(buffer));
	memset(buffer,'0',strlen(buffer));

	exit(EXIT_SUCCESS);
}

static void *thread_fun(void *arg)
{
	char *s = (char*)arg;
	char buffer[100] = {0};
	
	sprintf(buffer,"Printing from thread:- %s\n",s);
	ssize_t pp = write(STDOUT_FILENO,buffer,strlen(buffer));
	memset(buffer,'0',strlen(buffer));
	return (void*)strlen(s);
}
