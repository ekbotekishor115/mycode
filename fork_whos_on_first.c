#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <string.h>

int main(int argc, char**argv)
{
	int numChildren,j;
	pid_t childPid;
	if(argc > 1 && strcmp(argv[1], "--help") == 0)
		printf("%s [num-children]\n",argv[0]);

	numChildren =atoi(argv[1]);

	setbuf(stdout,NULL);

	for(j = 0; j < numChildren; j++)
	{
		switch(childPid = fork())
		{
			case -1:
				perror("fork");
			case 0:
				printf("%d child\n",j);
				_exit(EXIT_SUCCESS);
			default:
				printf("%d parent\n",j);
				wait(NULL);
				break;
		}
	}
	exit(EXIT_SUCCESS);
}
