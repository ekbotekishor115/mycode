#include <stdio.h>

void two(void)
{
	printf("Stack frames created in order:\n");
	printf(" 1)Main method\n 2)One method\n 3)Two method\n");
}

void one(void)
{
	two();
}

int main()
{
	int a = 5;
        int *b = &a;
        printf("Process Address is %ld:\n",(long int *)b);

	one();
}
