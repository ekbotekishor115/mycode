#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <stdbool.h>
#include <unistd.h>

bool is_run = true;
void sig_handler(int signum)
{
	is_run = false;
	printf("signal catch %d\n",signum);
}

int main()
{
	signal(SIGRTMAX,sig_handler);
	while(is_run)
	{
		usleep(500);
	}
	printf("is_run is false now\n");
	return 0;
}
