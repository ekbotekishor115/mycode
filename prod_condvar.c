#include <time.h>
#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <unistd.h>

static pthread_mutex_t mtx = PTHREAD_MUTEX_INITIALIZER;
static pthread_cond_t cond = PTHREAD_COND_INITIALIZER;

static int avail = 0;

static void *producer(void *arg)
{
	int cnt = atoi((char *)arg);
	int s,j;

	for (j = 0; j < cnt; j++)
	{
		sleep(1);
		s = pthread_mutex_lock(&mtx);
		if(s != 0)
			printf("pthread_mutex_lock %d\n",s);
		avail++;

		s = pthread_mutex_unlock(&mtx);
		if(s != 0)
			printf("pthread_mutex_unlock %d\n",s);
		s = pthread_cond_signal(&cond);
		if(s != 0)
			printf("pthread_cond_signal %d\n",s);
	}

	return NULL;
}

int main(int argc,char **argv)
{
	pthread_t tid;
	int s,j;
	int totRequired;

	int numConsumed;
	bool done;
	time_t t;

	t = time(NULL);

	totRequired = 0;
	for(j = 1; j < argc; j++)
	{
		totRequired += atoi(argv[j]);

		s = pthread_create(&tid, NULL,producer,argv[j]);
		if(s != 0)
			printf("pthead_create %d\n",s);
	}

	numConsumed = 0;
	done = false;

	for(;;)
	{
		s = pthread_mutex_lock(&mtx);
		if(s != 0)
			printf("pthread_mutex_lock %d\n",s);
		while(avail == 0)
		{
			s = pthread_cond_wait(&cond,&mtx);
			if(s != 0)
				printf("pthread_cond_wait %d\n",s);
		}
	
		while(avail > 0)
		{
			numConsumed++;
			avail--;
			printf("T=%ld: nonConsumed=%d\n", (long) (time(NULL)-t),numConsumed);
			done = numConsumed >= totRequired;
		}
		
		s = pthread_mutex_unlock(&mtx);
		if(s != 0)
			printf("pthread_mutex_unlock %d\n",s);
		if(done)
			break;
	}
	exit(EXIT_SUCCESS);
}
