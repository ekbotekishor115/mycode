#include <stdio.h>
#include <fcntl.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

int main(int argc,char*argv[])
{
	char f[100];
	char buf[BUFSIZ];
	
	if(argc != 2)
	{
		printf("%s,wrong argv\n",argv[0]);
		exit(EXIT_FAILURE);
	}
	strcpy(f,argv[1]);
//	strcpy(s,argv[2]);

	printf("first argument is %s\n",f);

	//opening the source file for reading
	
	int fd = open(f,O_RDWR);
	if(fd < 0)
	{
		perror("Open system call");
		exit(EXIT_FAILURE);
	}
	
	//reading the content in source file
	ssize_t count = read(fd,buf,BUFSIZ);
	if(count < 0)
	{
		perror("Read system call");
		exit(EXIT_FAILURE);
	}

	printf("count=%ld\n",count);
	printf("buf data = %s\n",buf);	


	lseek(fd, 0, SEEK_SET);

	 //writing into the source file
        ssize_t wr_count = write(fd,"40000",5);
        if(wr_count < 0)
        {
                perror("Write failed");
               // close(cr_fd);
                exit(EXIT_FAILURE);
        }


	//closing the source file
	int cl_fd = close(fd);
	if(cl_fd < 0)
	{
		perror("File not closed");
	}
	
	//creating destination file
//	int cr_fd = creat(s,S_IRUSR | S_IWUSR | S_IRGRP | S_IWGRP | S_IROTH | S_IWOTH);
//	if(cr_fd < 0)
//	{
//		perror("Create file failed");
//		exit(EXIT_FAILURE);
//	}

	//writing into the destination file
//	ssize_t wr_count = write(cr_fd,"40000",5);
//	if(wr_count < 0)
//	{
//		perror("Write failed");
//		close(cr_fd);
//		exit(EXIT_FAILURE);
//	}

}
