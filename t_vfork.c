#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <unistd.h>

int main(int argc, char **argv)
{
	int istack = 222;

	switch(vfork())
	{
		case -1:
			perror("vfork");
		case 0:
			sleep(3);
		
			write(STDOUT_FILENO,"Child Executing\n",16);
			istack *= 3;
			_exit(EXIT_SUCCESS);
		default:
			write(STDOUT_FILENO, "Parent Executing\n",17);
			printf("istack = %d\n",istack);
			exit(EXIT_SUCCESS);
	}
}
