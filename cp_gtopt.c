#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <string.h>
#include <unistd.h>

int main(int argc, char*argv[])
{
//	char f[10], s[10];
	char buf[BUFSIZ];
	char *stri;
	char *stro;
	int opt;

	while((opt = getopt(argc, argv, "i:o:")) != -1)
	{
		switch (opt)
		{
			case 'i':
				stri = malloc(strlen(optarg) * sizeof(char) + 1);
				strcpy(stri,optarg );
				break;
			case 'o':
				stro = malloc(strlen(optarg) * sizeof(char) + 1);
				strcpy(stro, optarg);
				break;
			default:
				fprintf(stderr, "Usage: %s [-i f] [-o s] name\n", argv[0]);
				exit(EXIT_FAILURE);
		}
	}
	
	printf("stri = %s; stro = %s; optind = %d; argc=%d; \n",stri ,stro, optind,argc);
	if(optind != argc)
	{
	
		fprintf(stderr,"Expected argument after options\n");
		exit(EXIT_FAILURE);
	}
	//printf("name argument = %s\n", argv[optind]);

	int fd = open(stri, O_RDONLY);
	if(fd < 0)
	{
		perror("Open system call");
		exit(EXIT_FAILURE);
	}

	ssize_t count = read (fd,buf,BUFSIZ);
	if (count < 0)
	{
		perror("Read system call");
		exit(EXIT_FAILURE);
	}

	int cl_fd = close(fd);
	if(cl_fd < 0)
	{
		perror("File closed");
	}

	int cr_fd = creat(stro, S_IRUSR | S_IWUSR | S_IRGRP | S_IWGRP | S_IROTH | S_IWOTH);
	if(cr_fd < 0)
	{
		perror("Create file failed");
		exit(EXIT_FAILURE);
	}
	
	ssize_t wr_count = write(cr_fd, buf,count);
	if(wr_count < 0)
	{
		perror("Write failed");
		close(cr_fd);
		exit(EXIT_FAILURE);
	}
	free(stri);
	free(stro);
	exit(EXIT_FAILURE);
}
