#include <stdio.h>
#include <stdlib.h>
#include <sys/mount.h>

int main()
{
	char* data;
	const char* s= "/dev/sda3";
	const char* d= "/mnt";
	unsigned long mount_flag= 0;
	mount_flag= MS_SYNCHRONOUS;// MS_RDONLY;//MS_SYNCHRONOUS
	int ret= mount(s,d,"ext4",mount_flag,data);
	if(ret < 0)
		perror("mount");
	return 0;
}
