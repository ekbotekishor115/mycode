#include <stdio.h>
#include <stdlib.h>

struct Date
{
	int day;
	int month;
	int year;
};

struct Student
{
	int st_roll_number;
	float st_marks;
	float st_attendance;
	struct Date st_date_of_birth;
};

/*defination of instance variable*/
struct Student s1={10,87,65,98,43,{20,4,1988}};

/*defination of instance ofstruct Date*/
struct Date d1={11,3,2011};

/* Folowing expressions can be used on RHS to read and on LHS to write
 * s1.st_roll_number
 * s1.st_marks
 * s1.st_attendance
 * s1.st_date_of_birth.day
 * s1.st_date_of_birth.month
 * s1.st_date_of_birth.year
 */

/* General syntax of pointer variable: T* p;
 * T = struct Date/struct Student
 * int* p = &n;
 *
 * p -> pointer to int -> int*
 * *p -> int
 * */

struct Student *p_student;//defination of pointer variable
struct Date *p_date;//defination of pointer to struct Date

int main(void)
{
	// Put address of instance variable in pointer variable
	p_student = &s1;

	/* typeof(p_student) = struct Student*
	 * typeof(*p_student) = struct Student
	 */
}
