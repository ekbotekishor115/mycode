#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <libgen.h>
#include <unistd.h>
#include <sys/wait.h>


#define CMD_SIZE 200

void handler(int signum)
{
	printf("Signal Received: %d\n",signum);
}

int main(int argc, char **argv)
{
	char cmd[CMD_SIZE];
	pid_t childPid;

	setbuf(stdout,NULL);

	printf("Parent PID = %ld\n", (long) getpid());

	switch (childPid = fork())
	{
		case -1:
			perror("fork");

		case 0:
			signal(SIGINT,handler);
			printf("Child (PID=%ld) exiting\n", (long) getpid());
			_exit(EXIT_SUCCESS);

		default:
			sleep(3);
			snprintf(cmd,CMD_SIZE, "ps | grep %s",basename(argv[0]));
			cmd[CMD_SIZE - 1] = '\n';
			system(cmd);
			
			if (kill(childPid, SIGINT) == -1)
				perror("kill");
			sleep(3);
			printf("After sending SIGKILL to zombie (PIS = %ld):\n",(long) childPid);
			system(cmd);
			exit(EXIT_SUCCESS);
	}
}
