#include <stdio.h>
#include <stdlib.h>

struct Date
{
	int day;
	int month;
	int year;
};

struct Date d1 = {21,1,2021};
struct Date d2;

int main(void)
{
	int dd, mm, yy;
	
	dd = d1.day;
	mm = d1.month;
	yy = d1.year;

	printf("dd: %d mm: %d yy: %d\n",dd,mm,yy);
	
	d1.day = 12;
	d1.month = 2;
	d1.year = 2014;

	dd = d1.day;
	mm = d1.month;
	yy = d1.year;
	printf("dd: %d mm: %d yy: %d\n",dd,mm,yy);
	
	printf("Enter the Day value for d2:");
	scanf("%d",&d2.day);
	
	printf("Enter the month value for d2:");
	scanf("%d",&d2.month);
	
	printf("Enter the year value of d2:");
	scanf("%d",&d2.year);

	printf("dd: %d mm: %d yy: %d\n",d2.day,d2.month,d2.year);
	return (0);
}
