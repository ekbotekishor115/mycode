#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>

int main()
{
	uid_t r_uid,e_uid;
	gid_t r_gid,e_gid;
	int err = setuid(0);
	if (err < 0)
		perror("setuid");
	int err1 = seteuid(0);
	if (err1 < 0)
		perror("seteuid");
	int err2 = setgid(0);
        if (err2 < 0)
                perror("setgid");
        int err3 = setegid(0);
        if (err3 < 0)
                perror("setegid");
	int err4 = setreuid(getuid(),0);
        if (err4 < 0)
                perror("setreuid");
        int err5 = setregid(getgid(),0);
        if (err5 < 0)
                perror("setregid");
	
	r_uid = getuid();
	e_uid = geteuid();
	r_gid = getgid();
	e_gid = getegid();

	printf("calling process id info\n1) real uid=%d\n2) effective uid=%d\n3) real gid=%d\n4) effective gid=%d\n",r_uid,e_uid,r_gid,e_gid);
	exit(EXIT_SUCCESS);

}
