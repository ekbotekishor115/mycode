#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <fcntl.h>
#include <unistd.h>

int main(int argc, char*argv[])
{
	char f[BUFSIZ],s[BUFSIZ];
	char buf[BUFSIZ];

	if (argc != 3)
	{
		printf("%s,wrong argv\n",argv[0]);
		exit(EXIT_FAILURE);
	}
	
	strcpy(f,argv[1]);
	strcpy(s,argv[2]);

	printf("first argument is %s\n second argument is %s\n",f,s);

	int fd = open(f,O_RDONLY);
	if (fd < 0)
	{
		perror("Open system call");
		exit(EXIT_FAILURE);
	}

	ssize_t count;

	int cr_fd = creat(s, S_IRUSR | S_IWUSR | S_IRGRP | S_IWGRP | S_IROTH | S_IWOTH);
	if (cr_fd < 0)
	{
		perror("Create file failure");
		exit(EXIT_FAILURE);
	}

	while((count = read(fd,buf,BUFSIZ)) > 0) 
	{
		ssize_t wr_count = write(cr_fd,buf,count);
		if (wr_count < 0)
		{
			perror("Write failed");
		}
	}

	int cl_fd = close(fd);
	if(cl_fd < 0)
	{
		perror("File not closed");
	}
	
	close(cr_fd);
	exit(EXIT_SUCCESS);
}
