#include <stdio.h>
#include <stdlib.h>
#include <signal.h>

int main(int argc, char **argv)
{
	if(argc != 2)
	{
		printf("%s<pid>\n",argv[0]);
		exit(EXIT_FAILURE);
	}
	kill(atoi(argv[1]),SIGINT);
	return 0;
}
