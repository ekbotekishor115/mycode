#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdbool.h>
#include "mbr.h"

#define BOOTSTRAP_SIZ 446 //TODO --change name to proper
#define MBR_SIZ 512 //TODO

enum OS_TYPE{WINDOWS = 1,LINUX};
void createBootDevice(char* dev_file,enum OS_TYPE os_type);


void createBootDevice(char* dev_file,enum OS_TYPE os_type)
{
	int fd;
	char buf[MBR_SIZ];
	

	fd = open(dev_file,O_RDWR);
	if(fd < 0)
	{
		perror("open Dev");
		exit(EXIT_FAILURE);
	}
	
	ssize_t rd = read(fd,buf,MBR_SIZ);
	if(rd < 0)
	{
		perror("read_win");
		exit(EXIT_FAILURE);
	}

	memset(&buf[446],0,1);
	
	if(os_type == WINDOWS)
	{
//		memcpy(buf,win_bootStrp,BOOTSTRAP_SIZ);
	}
	else if (os_type == LINUX)
	{
//		memcpy(buf,lnx_bootStrp,BOOTSTRAP_SIZ);
	}

	buf[446] = 0x80;

//	for(int i =0; i < BUFDEV; i++)
//		printf("%hhx ",buf_win[i]);
	
	off_t t = lseek(fd,0,SEEK_SET);
	ssize_t wr = write(fd,buf,MBR_SIZ);
	if(wr < 0)
	{
		perror("write failed");
		exit(EXIT_FAILURE);
	}
	
	int cl = close(fd);
	if(cl < 0)
	{
		perror("file not closed");
	}
}

/*void createDeviceLinux()
{
	int fd_lnx;
	char buf_lnx[BUFDEV];

	fd_lnx = open(argv[2],O_RDWR);
	if (fd_lnx < 0)
	{
		perror("open Dev");
		exit(EXIT_FAILURE);
	}

	ssize_t rd_lnx = read(fd_lnx,buf_lnx,BUFDEV);
	if (rd_lnx < 0)
	{
		perror("read lnx");
		exit(EXIT_FAILURE);
	}
	buf_lnx[446] = 0x80;

	printf("%hhx ",buf_lnx[446]);

	off_t t = lseek(fd_lnx,0,SEEK_SET);
	ssize_t wr_lnx = write(fd_lnx,buf_lnx,BUFDEV);
	if (wr_lnx < 0)
	{
		perror("write failed");
		exit(EXIT_FAILURE);
	}

	int cl_lnx = close(fd_lnx);
	if (cl_lnx < 0)
		perror("file not closed");
}*/

int main(int argc,char**argv)
{
	int opt;
	char* dev_file = NULL;
	enum OS_TYPE os_type = -1;
//	if (argc == 2)
//	{
	while((opt = getopt(argc,argv,"f:lw")) != -1)
	{
		switch(opt)
		{
			case 'w':
				os_type = WINDOWS;
				break;

			case 'l':
				os_type = LINUX;
				break;
			case 'f':
				dev_file = (char*) malloc(strlen(optarg) + 1);
				//TODO  --ERROR check
				if (dev_file < 0)
				{
					perror("malloc");
				}
				memcpy(dev_file, optarg,strlen(optarg));
				break;
			default:
				fprintf(stderr,"Expected arguments after options\n");
				exit(EXIT_FAILURE);
		}
	}
	
	if (os_type == WINDOWS && dev_file != NULL)
	{
		//TODO-- call windows func
		createBootDevice(dev_file,os_type);
	}
	else if (os_type == LINUX && dev_file != NULL)
	{
		//TODO -- call linux func
		createBootDevice(dev_file,os_type);
	}
	else
	{
		//TODO-- exit -> provide <-w> or <-l>
		exit(EXIT_FAILURE);
	}
	
	exit(EXIT_SUCCESS);				
}
