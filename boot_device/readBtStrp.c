#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdint.h>
#include <unistd.h>

#define BUFSTRP 446

int main(int argc,char**argv)
{
	int opn;
	char buf[BUFSTRP];

	if (argc == 2)
	{
		opn = open(argv[1],O_RDONLY);
		if (opn < 0)
		{
			perror("open");
			exit(EXIT_FAILURE);
		}
	}

	ssize_t red = read(opn,buf,BUFSTRP);
	if(red < 0)
	{
		perror("read");	
	}
	

	printf("uint8_t win_bootStrp = \n\t{");
	for (int i=0 ; i < BUFSTRP ; i++)
	{
		printf("0x%02hhx , ",buf[i]);
	}
	printf("\b\b}\n");
}
