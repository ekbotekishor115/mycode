#define _GNU_SOURCE
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/wait.h>
#include <fcntl.h>
#include <sched.h>
#include <string.h>
#include <errno.h>

#ifndef CHILD_SIG
#define CHILD_SIG SIGUSR1

#endif

static int childFunc(void *arg)
{
	if(close(*((int *)arg)) == -1)
		perror("close");
	
	return 0;
}

int main(int argc,char**argv)
{
	const int STACK_SIZE = 65536;
	char* stack;
	char *stackTop;
	int s,fd, flags;

	fd = open("/dev/null", O_RDWR);
	if(fd == -1)
		perror("open");

	flags = (argc > 1) ? CLONE_FILES : 0;

	stack = malloc(STACK_SIZE);
	if(stack ==NULL)
		perror("malloc");
	stackTop = stack + STACK_SIZE;

	if(CHILD_SIG != 0 && CHILD_SIG != SIGCHLD)
		if(signal(CHILD_SIG, SIG_IGN) == SIG_ERR)
			perror("signal");

	if(clone(childFunc,stackTop,flags | CHILD_SIG,(void *) &fd) == -1)
		perror("clone");
	
	if(waitpid(-1, NULL, (CHILD_SIG != SIGCHLD) ? __WCLONE : 0) == -1)
		perror("waitpid");
	printf("Child has terminated\n");

	s = write(fd, "x", 1);
	if(s == -1 && errno == EBADF)
		printf("file descripter %d has been closed\n", fd);
	else if(s == -1)
		printf("write() on file descriptor %d failed ""unexpectedly(%s)\n", fd, strerror(errno));
	else 
		printf("write() on file descripter %d succeeded\n", fd);

	exit(EXIT_SUCCESS);
}
