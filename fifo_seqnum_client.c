#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include "fifo_seqnum.h"

static char clientFifo[CLIENT_FIFO_NAME_LEN];

static void removeFifo()
{
	unlink(clientFifo);
}

int main(int argc,char **argv)
{
	int serverFd,clientFd;
	struct request req;
	struct response resp;

	if (argc > 1 && strcmp(argv[1],"--help") == 0)
	{
		printf("%s [seq-len...]\n",argv[0]);
		exit(EXIT_FAILURE);
	}

	umask(0);
	snprintf(clientFifo,CLIENT_FIFO_NAME_LEN,CLIENT_FIFO_TEMPLATE,(long) getpid());

	if (mkfifo(clientFifo,S_IRUSR | S_IWUSR | S_IWGRP) == -1 && errno != EEXIST)
	{
		printf("mkfifo %d\n",clientFd);
		perror("mkfifo");
	}
	
	if (atexit(removeFifo) != 0)
		perror("atexit");

	req.pid = getpid();
	req.seqLen = (argc > 1) ? atoi(argv[1]) : 1;

	serverFd = open(SERVER_FIFO,O_WRONLY);
	if (serverFd == -1)
	{
		printf("open %s\n",SERVER_FIFO);
		perror("open");
	}

	if (write(serverFd,&req,sizeof(struct request)) != sizeof(struct request))
	{
		printf("Can't write to server\n");
		exit(EXIT_FAILURE);
	}
		
	clientFd = open(clientFifo,O_RDONLY);
	if (clientFd == -1)
	{
		printf("open %s\n",clientFifo);
		printf("clientFd = %d\n",clientFd);
		perror("open");
	}
	
	if (read(clientFd,&resp,sizeof(struct response)) != sizeof(struct response))
	{
		printf("Can't read response from server\n");
		exit(EXIT_FAILURE);
	}

	printf("%d\n",resp.seqNum);
	exit(EXIT_SUCCESS);
}
