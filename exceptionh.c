#include <stdio.h>
#include <stdlib.h>
#include <setjmp.h>

static jmp_buf env;

static void shortcut2(void);
static void shortcut1(int argc);

int main(int argc, char**argv)
{
	switch(setjmp(env))
	{
		case 0:
			printf("Callint shortcut1()\n");
			shortcut1(argc);
			break;
		case 1:
			printf("start failed in first way..shortcut1()\n");
			break;
		case 2:
			printf("start failed in shortcut2()\n");
			break;
	}
	exit(EXIT_SUCCESS);
}
static void shortcut1(int argc)
{
	if(argc == 1)
		longjmp(env,1);
	shortcut2();
}

static void shortcut2(void)
{
	longjmp(env,2);
}		
