#define _BSD_SOURSE
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>

int main(int argc, char **argv)
{
	if (argc > 2 || (argc > 1 && strcmp(argv[1], "--help") == 0))
		printf(" [file]\n");

	if (acct(argv[1]) == -1)
		perror("acct");

	printf("Process accounting %s\n",(argv[1] == NULL) ? "disabled" : "enabled");
	exit(EXIT_SUCCESS);
}
