#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <sys/sem.h>
#include "semun.h"

int main(int argc,char **argv)
{
	struct semid_ds ds;
	union semun arg;
	int j,semid;

	if (argc < 3 || strcmp(argv[1],"--help") == 0)
	{
		printf("%s semid val...\n",argv[0]);
		exit(EXIT_FAILURE);
	}

	semid = atoi(argv[1]);

	arg.buf = &ds;

	if (semctl(semid,0,IPC_STAT,arg) == -1)
	{
		perror("semctl");
		exit(EXIT_FAILURE);
	}

	if (ds.sem_nsems != argc - 2)
	{
		printf("Set contains %ld semaphores, but %d values were supplied\n",(long) ds.sem_nsems,argc - 2);
	}

	arg.array = calloc(ds.sem_nsems,sizeof(arg.array[0]));
	if (arg.array == NULL)
	{
		perror("calloc");
		exit(EXIT_FAILURE);
	}

	for (j = 2; j < argc; j++)
	{
		arg.array[j - 2] = atoi(argv[j]);
	}
		
	if (semctl(semid,0,SETALL,arg) == -1)
	{
		perror("semctl-SETALL");
		exit(EXIT_FAILURE);
	}
	
	printf("Semaphore values changed (PID=%ld)\n",(long) getpid());
	
	exit(EXIT_SUCCESS);
}
