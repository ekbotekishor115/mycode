#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/mman.h>
#include <sys/stat.h>

int main(int argc,char **argv)
{
	int fd;
	char *addr;
	struct stat sb;

	if (argc != 2 || strcmp(argv[1],"--help") == 0)
	{
		printf("%s shm-name\n",argv[0]);
		exit(EXIT_FAILURE);
	}

	fd = shm_open(argv[1],O_RDONLY,0);
	if (fd == -1)
	{
		perror("shm_open");
		exit(EXIT_FAILURE);
	}

	if (fstat(fd, &sb) == -1)
	{
		perror("fstat");
		exit(EXIT_FAILURE);
	}

	addr = mmap(NULL, sb.st_size, PROT_READ,MAP_SHARED, fd, 0);
	if (addr == MAP_FAILED)
	{
		perror("mmap");
		exit(EXIT_FAILURE);
	}

	if (close(fd) == -1)
	{
		perror("close");
		exit(EXIT_FAILURE);
	}

	write(STDOUT_FILENO,addr,sb.st_size);
	printf("\n");
	exit(EXIT_SUCCESS);
}
