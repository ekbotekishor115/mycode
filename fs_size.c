#include <stdio.h>
#include <stdlib.h>
#include <sys/statvfs.h>

int main()
{
	struct statvfs buf;
	long int a,b,c=0;
	int fs_size=0;
	int ret = statvfs("/home/kishor/Desktop/kishor",&buf);
	if(ret < 0)
	{
		perror("statvfs");
		exit(EXIT_FAILURE);
	}
	a = buf.f_bsize;
	b = buf.f_bfree;
	c = a*b;
	fs_size = c / (1024*1024*1024);
	printf("File System size in GB is %d\n",fs_size);	
	exit(EXIT_SUCCESS);
}
