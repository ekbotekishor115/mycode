#include <stdio.h>
#include <stdlib.h>

//Declaration of data type Date -> No memory allocated
struct Date
{
	int day;
	int month;
	int year;
};

//Data defination statement : Memory is allocated
struct Date d1;

//Declaration of data type Student : No memory allocated
struct Student
{
	int st_roll_number;
	float st_marks;
	float st_attendance;
	//Data declaration statements : No memory allocated
	struct Date at_date_of_birth;
};

//Data defination statement : Memory is allocated
struct Student s1;

int main()
{
	printf("sizeof(d1):%lu\n",sizeof(d1));
	printf("sizeof(s1):%lu\n",sizeof(s1));
	return (0);
}
