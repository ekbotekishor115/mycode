#include <stdio.h>
#include <stdlib.h>
#include <sys/statvfs.h>

int main()
{
	struct statvfs buf;
	int ret = statvfs("/home/kishor/Desktop/kishor",&buf);
	if(ret < 0)
	{
		perror("statvfs");
		exit(EXIT_FAILURE);
	}
	printf("buf.f_bsize=%ld\nbuf.f_frsize=%ld\nbuf.f_blocks=%ld\nbuf.f_bfree=%ld\nbuf.f_bavail=%ld\nbuf.f_files=%ld\nbuf.f_ffree=%ld\nbuf.f_favail=%ld\nbuf.f_fsid=%ld\nbuf.f_flag=%ld\nbuf.f_namemax=%ld\n",buf.f_bsize,buf.f_frsize,buf.f_blocks,buf.f_bfree,buf.f_bavail,buf.f_files,buf.f_ffree,buf.f_favail,buf.f_fsid,buf.f_flag,buf.f_namemax);
	exit(EXIT_SUCCESS);
}

