#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/wait.h>

int myfunc(int arg)
{
	return arg;
}

int main(int argc,char **argv)
{	
	if (argc != 2)
	{
		printf("UsageErr!!, %s <Return Status>\n",argv[1]);
		exit(EXIT_FAILURE);
	}
	int ret_val = atoi(argv[1]);
	pid_t child_pid;
	int status = 0;

	child_pid = fork();
	if(child_pid == -1)
		perror("fork");
	if(child_pid == 0)
		exit(myfunc(ret_val));
	if(wait(&status) == -1)
		perror("wait");

	printf("In parent return status of child : %d status : %d\n",status,status >> 8);
	return 0;
}
